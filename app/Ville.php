<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ville extends Model
{
    protected $table = 'villes';

    protected $fillable = [
        'libelle',
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    } 
}
