<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
	protected $table = 'devices';

    protected $fillable = [
        'uuid', 'model', 'manufacturer', 'serial', 'nom'
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    } 
}
