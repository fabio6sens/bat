<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rules extends Model
{
    use SoftDeletes;
	
	protected $table = 'roles';
	
	protected $fillable = [
        'role',
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function users()
    {
        return $this->hasMany('App\User');
    } 
}
