<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\Ville;

use Validator;

class MobileController extends Controller
{
	public function initDevice(Request $request)
	{
		try {
			$device = Device::where('uuid', $request->uuid)->firstOrfail();
		} catch (\Exception $e) {
			$count = Device::all()->count();
			$device = new Device;
			$device->nom = 'Appareil '.($count+1);
			$device->uuid = $request->uuid;
			$device->serial = $request->serial;
			$device->manufacturer = $request->manufacturer;
			$device->model = $request->model;
			$device->save();
		}

		$return = [
			'villes' => Ville::all()->toArray(),
			'device' => $device->toArray(),
		];

		return response()->json($return);
	}

    public function saveUser(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email',
            'telephone' => 'required|unique:users,telephone'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 'ERROR',
                'message' => 'Ce client existe déjà.' 
            ]);
        }

        $code = \App\User::code();
        while(\App\User::where('code_acces', $code)->count() > 0){
            $code = \App\User::code();
        }

        try {
            $user = new \App\User;
            $user->name = $request->name;
            $user->genre = $request->genre;
            $user->ville_id = $request->ville_id;
            $user->telephone = '+225'.$request->telephone;
            $user->moy_contact = $request->moy_contact;
            $user->marque_pref = $request->marque_pref;
            $user->marque_alt = $request->marque_alt;
            $user->datenaiss = $request->date;
            $user->echantillonage_pqt = $request->echantillonage_pqt;
            $user->pqt_acht = $request->pqt_acht;
            $user->email = $request->email;
            $user->device_id = $request->device_id;
            $user->role_id = 4;
            $user->code_acces = $code;
            $user->password = '';
            $user->save();

            $return = [
                'code' => 'OK',
                'message' => 'Client Ajouté avec succès. Son code d\'accès est le suivant : '.$user->code_acces
            ];
        } catch (\Exception $e) {
            $return = [
                'code' => 'ERROR',
                'message' => 'Erreur lors de l\'ajout. Veuillez réessayer SVP!'
                // 'message' => $e->getMessage()
            ];
        }

        return response()->json($return);
    }

    public function saveU($request)
    {	
        $validator = Validator::make($request, [
            'email' => 'required|email|unique:users,email',
            'telephone' => 'required|unique:users,telephone'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 'ERROR',
                'message' => 'Ce client existe déjà.' 
            ]);
        }

    	$code = \App\User::code();
    	while(\App\User::where('code_acces', $code)->count() > 0){
    		$code = \App\User::code();
    	}

    	try {
    		$user = new \App\User;
    		$user->name = $request['name'];
    		$user->genre = $request['genre'];
    		$user->ville_id = $request['ville_id'];
    		$user->telephone = '+225'.$request['telephone'];
    		$user->moy_contact = $request['moy_contact'];
    		$user->marque_pref = $request['marque_pref'];
            $user->marque_alt = $request['marque_alt'];
    		$user->datenaiss = $request['date'];
    		$user->echantillonage_pqt = $request['echantillonage_pqt'];
    		$user->pqt_acht = $request['pqt_acht'];
    		$user->email = $request['email'];
    		$user->device_id = $request['device_id'];
    		$user->role_id = 4;
    		$user->code_acces = $code;
    		$user->password = '';
    		$user->save();

    		$return = [
    			'code' => 'OK',
    			'message' => 'Client Ajouté avec succès. Son code d\'accès est le suivant : '.$user->code_acces
    		];
    	} catch (\Exception $e) {
    		$return = [
    			'code' => 'ERROR',
    			// 'message' => 'Erreur lors de l\'ajout. Veuillez réessayer SVP!'
    			'message' => $e->getMessage()
    		];
    	}

    	return response()->json($return);
    }

    public function saveUsers(Request $request)
    {
        $back = [];
        foreach ($request->users as $k => $v) {
            $back[] = $this->saveU($v);
        }

        return response()->json($back);
    }

    public function getUsers($device, $page = 1)
    {
        $currentPage = $page;
        \Illuminate\Pagination\Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        $users = \App\User::where([['role_id', 4], ['device_id', $device]])
                            ->orderBy('created_at', 'desc')
                            ->paginate(5);
        $u = [];
        foreach ($users as $user) {
            $median = $user->toArray();
            $median['ville'] = is_null($user->ville) ? [] : $user->ville->toArray();
            $u[] = $median;
        }

        return response()->json($u);
    }

    public function deleteUser($id)
    {
        try {
            $user = \App\User::find($id);
            $user->delete();

            return response()->json([
                'code' => 'OK',
                'message' => 'Suppression effectuée avec succès'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 'ERROR',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function villes()
    {
    	$code = \App\User::code(15);
    	echo $code;
    }
}
