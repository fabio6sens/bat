<?php

namespace App\Http\Controllers\Frontend;

use App\Image;
use App\News;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    public function gallerie()
    {
        $allimg = Image::all();
        return view('admin.gallery.index',compact('allimg'));
    }

    public function store(Request $request){
        $requete = $request->except(['_token']);
        //dd($requete);
        if($request->hasFile('file')){
            try{
                $img = new Image();
                //dd($img);
                //$actus->albums_id=$requete['idalbum'] ;

                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                //$folderName = '../public_html/piges/panneau';
                $folderName = 'frontgalerie/';
                $picture = str_random(8).'.'. $extension;
                $file->move($folderName,$picture);

                $img->imgs = $picture;
                $img->save();
            }catch (\Exception $e){
                dd($e->getMessage());
            }
            return redirect('gallery');
        }else{
            return redirect()->route('gallery');
        }
    }

    public function delImg($id){
        if($id){
            $image = Image::find($id);
            if (!empty($image->imgs)) {
                $folderName ='frontgalerie/';
                unlink($folderName.'/'.$image->imgs);
                $image->delete();
                return redirect('gallery');
            }
        }
    }


    public function video()
    {
        $allvideo = Video::all();
        return view('admin.video.index',compact('allvideo'));
    }

    public function storeVideo(Request $request){
        $requete = $request->except(['_token']);
        //dd($requete);
        try{
            $video = new Video();
            $video->title = $requete['libelle'];
            $video->url = $requete['url'];
            $video->save();
        }catch (\Exception $e){
            dd($e->getMessage());
        }
        return redirect('video');

    }

    public function delvideo($id){
        //dd($id);
        if($id){
            $video = Video::find($id);
            $video->delete();
            return redirect('video');
        }
    }

    public function news()
    {
        $allactus = News::orderBy('id','desc')->get();
        return view('admin/news/index',compact('allactus'));
    }

    public function addnews(){
        return view('admin/news/add');
    }

    public function newsstore(Request $request)
    {
        $reponse = $request->except(['_token']);
        //dd($reponse);

        $valider = Validator::make($request->all(),[
            'titre' =>'required',
            'contenu' =>'required',
        ]);
        if($valider->fails()){
            return redirect()->route('addnews')->withErrors($valider->errors());
        }else{
            if($request->hasFile('file')){

                try{
                    $actus = new News();
                    $actus->slug= str_slug($reponse['titre']);
                    $actus->title = $reponse['titre'];
                    $actus->des = $reponse['contenu'];

                    $file = $request->file('file');
                    $extension = $file->getClientOriginalExtension() ?: 'png';
                    //$folderName = '../public_html/piges/panneau';
                    $folderName = 'frontarticles/';
                    $picture = str_random(8).'.'. $extension;
                    $file->move($folderName,$picture);
                    $actus->imgactu = $picture;
                    $actus->save();
                }catch (\Exception $e){
                    dd($e->getMessage());
                }
                return redirect()->route('new')->with('success','✔ Félicitation ! vous venez d\' ajoute une actualite');

            }else{
                return redirect()->route('new')->with('error','Veuillez inserez la Photo de l\'actualité');
            }
        }
    }

    public function newsedit($id)
    {
        //dd($id);
        $itemactu = News::find($id);
        return view('admin/news/edit',compact('itemactu'));
    }

    public function newsshow($id)
    {
        //dd($id);
        $item = News::find($id);
        return view('admin/news/show',compact('item'));
    }

    public function newsupdate(Request $request){
        //dd($id);
        $reponse = $request->except(['_token']);
        //dd($reponse);
        $valider = Validator::make($request->all(),[
            'titre' =>'required',
            'contenu' =>'required',
            'etat' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('new')->withErrors($valider->errors());
        }else{

            $actus = News::find($reponse['idactu']);

            if($request->hasFile('file')){
                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = 'frontarticles/';
                $picture = str_random(8).'.'. $extension;

                if (!empty($actus->imgactu)) {
                    unlink($folderName.$actus->imgactu);
                }
                $actus->imgactu = $picture;
                $file->move($folderName,$picture);
            }

            $actus->title = $reponse['titre'];
            $actus->des = $reponse['contenu'];
            $actus->status = $reponse['etat'];
            $actus->save();

            return redirect()->route('new')->with('success','✔ Félicitation ! vous venez de modifier une actualite');

            //dd($reponse);
        }
    }

    public function destroy(Request $request,$id){
        dd($id);
        $new = News::find($id);
        dd($new);
        $new->delete();
        return redirect()->route('new')->with('success','vous venez de supprimer une actualite');
    }
    
    public function importExcel(Request $request){
        $excel = $request->except(['_token']);

        $valider = Validator::make($request->all(),[
            'file' =>'required|mimes:xls,xlsx',
        ]);

        if($valider->fails()){
            return redirect('users/collecte')->withErrors($valider->errors());
        }else{
            $results = Excel::selectSheetsByIndex(0)->load($excel['file'])->toArray();
            $datadate = null;
            //dd($results);
            //dd($results[0]['datenaiss']->format('Y-m-d'));
            if(!empty($results)){
                try{
                    foreach($results as $value=>$key ){
                        $code = \App\User::code();
                        while(\App\User::where('code_acces', $code)->count() > 0){
                            $code = \App\User::code();
                        }
                        
                        if(!empty($key['datenaiss_old'])){
                            $dat = $key['dat'].$key['datenaiss_old'];
                            $date = new \DateTime($dat);
                            $datadate = $date->format('Y-m-d');
                        }
    
                        $user = new \App\User;
                        $user->name = $key['nom'];
                        $user->genre = $key['sexe'];
                        $user->ville_id = $key['lieu_dengagement'];
                        $user->telephone = $key['contact'];
                        //$user->moy_contact = $key['moyen_de_contact'];
                        $user->moy_contact = 'sms';
                        $user->marque_pref = $key['marque_preferee'];
                        $user->marque_alt = $key['marque_alternative'];
                        $user->datenaiss = $datadate;
                        $user->echantillonage_pqt = $key['echantillonage_au_paquet'];
                        $user->pqt_acht = 'commun';
                        $user->email = 'a@a.com';
                        //$user->pqt_acht = $key['paquet_achete'];
                        //$user->email = $key['email'];
                        $user->instagram = $key['instagram'];
                        $user->device_id = 1;
                        $user->role_id = 4;
                        $user->code_acces = $code;
                        $user->password = '';
                        $user->save();
    
                        //var_dump($key["prenoms_contact"]);
                    }
                    return redirect('users/import')->with('success', 'Contact Ajouté avec Succès');
                }catch(\Exception $e){
                    //dd($e->getMessage());
                    return redirect('users/import')->with('error', 'une erreur s\'est produite lors de l\'envoi du fichier');
                }
            }else{
                return redirect('users/import')->with('error', 'Le fichier ne contient aucun contact');
            }
        }
    }


}
