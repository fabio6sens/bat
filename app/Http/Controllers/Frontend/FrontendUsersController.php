<?php

namespace App\Http\Controllers\Frontend;

use App\Message;
use App\Like;
use App\News;
use App\User;
use App\Winner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class FrontendUsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('frontend/profil/index');
    }

    public function game(){
        //dd("de");
        return view('frontend/profil/game');
    }

    public function savewin(Request $request){
        $response=$request->all();
        //dd($response);
        try{
            $win = new Winner();
            $win->user_id= 2;
            $win->status= $response['status'];
            $win->save();
            return json_encode(array('type'=>1));
        }catch (\Exception $e){
            return json_encode(array('type'=>0));
            //dd($e->getMessage());
        }
    }

    public function update(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);
        $valider = Validator::make($request->all(),[
            'contact' =>'max:255|min:3|required',
            //'adresse' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('indexUser')->withErrors($valider->errors());
        }else{
            $user = User::where('id',Auth::user()->id)->firstOrFail();
            $user->telephone = $reponse['contact'];
            //$user->bio = $reponse['bio'];
            $user->save();
            return redirect()->route('indexUser')->with('success','✔ Félicitation ! modification a été modifié');
        }
    }

    public function editpass(Request $request){
        //dd($request->all());
        $reponse = $request->except(['_token']);
        $valider = Validator::make($request->all(),[
            'motpass' =>'max:255|min:6|required',
            'newpass' =>'max:255|min:6|required',
            'confirmpass' =>'max:255|min:6|required',
        ]);

        if($valider->fails()){
            return redirect()->route('indexUser')->withErrors($valider->errors());
        }else{
            $oldpass = $reponse['motpass'];
            $newpass = $reponse['newpass'];
            $newpassconfirm = $reponse['confirmpass'];
            $passUser = Auth::user()->password;
            if($newpass===$newpassconfirm)
            {
                if(Hash::check($oldpass,$passUser) ){

                    $user = User::find(Auth::user()->id);
                    $user->password = Hash::make($newpass);
                    $user->save();

                    return redirect()->route('indexUser')->with('success','Félicitation votre mot de passe a été mise a jour');
                }else{
                    return redirect()->route('indexUser')->with('error','Désolé ! votre mot de passe actuel est erronée');
                }
            }else{
                return redirect()->route('indexUser')->with('error','Les mots de passe sont différents');
            }

        }
    }

    public function editavatar(Request $request){
        //dd($request->file('fileUser'));
        $file=$request->file('fileUser');
        $fileSize=$file->getSize();
        //dd($fileSize);
        try{
            if($fileSize <= 3000263):
                $extension = $file->getClientOriginalExtension() ?: 'png';
                //$folderName = '../public_html/piges/panneau';
                $folderName = 'userAvatar/';
                $picture = str_random(8).'.'. $extension;

                if (!empty(Auth::user()->img)) {
                    unlink($folderName.Auth::user()->img);
                }
                $users= User::find(Auth::user()->id);
                $users->img = $picture;
                $users->save();

                $file->move($folderName,$picture);

                return redirect()->route('indexUser')->with('success','Félicitation ! votre avatar a été mise à jours');
            else:
                return redirect()->route('indexUser')->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 3Mb');
            endif;

        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }
    
    public function messadmin(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);
        $valider = Validator::make($request->all(),[
            'msg' =>'required',
        ]);
        $actu = News::where('id',$reponse['slug'])->first();
        //dd($actu);

        if($valider->fails()){
            return redirect()->route('showNews',$actu->slug)->withErrors($valider->errors());
        }else{
            //dd($actu);
           $messge = new Message();
           $messge->user_id = Auth::user()->id;
           $messge->new_id = $actu->id;
           $messge->msg = $reponse['msg'];
           $messge->save();

           return redirect()->route('showNews',$actu->slug)->with('success','✔ Félicitation ! votre message a bien été envoyé');
        }
    }
    
    
    public function likenew($id){
        //dd($id);
        $actu = News::where('id',$id)->first();
        $ollike = Like::where('new_id',$id)->count();

        $lik = new Like();
        $lik->user_id = Auth::user()->id ;
        $lik->new_id = $id ;
        $lik->nb =$ollike + 1;
        $lik->save();

        return redirect()->route('showNews',$actu->slug);
//        ->with('success','✔ Vous ! votre message a bien été envoyé');
    }

    public function likenonew($id){
        //dd($id);
        $actu = News::where('id',$id)->first();
        $ollike = Like::where('new_id',$id)->count();
        $ollikeUser = Like::where('new_id',$id)->where('user_id',Auth::user()->id)->delete();
        //$ollikeUser->delete();

        return redirect()->route('showNews',$actu->slug);
//        ->with('success','✔ Vous ! votre message a bien été envoyé');
    }
}
