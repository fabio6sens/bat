<?php

namespace App\Http\Controllers\Frontend;


use App\Image;
use App\News;
use App\Like;
use App\User;
use App\Video;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class FrontendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        return redirect()->route('admin.dashboard');
         //Auth::loginUsingId(8);
        $allactus = News::where('status','1')->orderBy('id','desc')->limit(3)->get();
        //dd($allactus[0]);
        $allactus->map(function($item){
            $tab= explode(' ', html_entity_decode(strip_tags($item->des)), 16); unset($tab[15]);
            $minides = implode(' ', $tab).'...';
            $item->minidesc = $minides;
        });

        $lastVideo = Video::orderBy('id','desc')->limit(1)->first();

        $galeries= Image::orderBy('id','desc')->limit(6)->get();

        return view('welcome',compact('allactus','lastVideo','galeries'));
    }

    public function indexGalerie(){
        $galeries= Image::orderBy('id','desc')->get();
        return view('frontend/galerie',compact('galeries'));
    }

    public function indexNews(){
        $allactus= News::where('status','1')->orderBy('id','desc')->get();
        $allactus->map(function($item){
            $tab= explode(' ', html_entity_decode(strip_tags($item->des)), 16); unset($tab[15]);
            $minides = implode(' ', $tab).'...';
            $item->minidesc = $minides;
        });
        return view('frontend/news',compact('allactus'));
    }

    public function showNews($slug){
        $itemactu= News::where('slug',$slug)->where('status','1')->first();
        //dd($itemactu);
        $likUser = Like::where('user_id',Auth::user()->id)->where('new_id',$itemactu->id)->count();
        $countliknew =Like::where('new_id',$itemactu->id)->count();
        return view('frontend/shownew',compact('itemactu','likUser','countliknew'));
    }

    public function indexVideo(){
        $videos= Video::orderBy('id','desc')->get();
        //dd($videos);
        return view('frontend/video',compact('videos'));
    }

    public function creatpass($token){
        $userExit = User::where('code_acces',$token)->first();
        if($userExit){
            return view('frontend/createdpass',compact('userExit'));
        }else{
            return redirect()->route('login')->with('error','Désolé ! ce lien ne semble plus valide');;
        }

    }

    public function savecreatpass(Request $request){
        $response=$request->all();
        //dd($response);
        $valider =  Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ]);
        $firstUser = User::where('id',$response['slug'])->first();

        if($valider->fails()){
            return redirect()->route('creatpass',$firstUser->code_acces)->withErrors($valider->errors());
        }else{
            //dd($valider->errors());
            $firstUser->password = Hash::make($response['password']);
            $firstUser->code_acces = '' ;
            $firstUser->save();
            return redirect()->route('login');
        }
    }

    // public function getgame($token , Request $request){

    // }

}
