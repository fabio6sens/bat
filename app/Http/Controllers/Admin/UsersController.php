<?php

namespace App\Http\Controllers\Admin;

use App\Collecte;
use App\Message;
use App\Roving;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


use App\Rules;
use App\User;
use App\Ville;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Response;

class UsersController extends Controller
{

    public function roles()
    {
        /*$all = DB::table('users')
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'), 'device_id')
            ->groupBy('date')
            ->groupBy('device_id')
            ->where('role_id',4)
            ->orderBy('device_id', 'ASC')
            ->get();
        $aray= $all->toJson();
        dd($aray);
        dd($all,'der');*/
    	$roles = Rules::orderBy('id','DESC')->get();

    	return view('admin.users.roles', compact('roles'));
    }

    public function postroles(Request $request)
    {
    	$roles = new Rules($request->all());
    	$check = Rules::where('role', $request->role)->get();
    	if(count($check) == 0){
    		$roles->save();
    		$request->session()->flash('success', 'Enregistrement réussi.');
    		return redirect('users/roles');
    	}
    	else{
    		$request->session()->flash('error', 'Enregistrement déjà existant.');
    		return redirect('users/roles');
    	}

    }
    
    public function editroles($id){
        
        $role = Rules::where('id', $id)->first();
        //dd($role);
        return view('admin.users.editrole', compact('role'));
    }
    
    public function updateroles(Request $request){
        $role = Rules::where('id', $request->slug)->first();
        $role->role = $request->role;
        $role->save();
        $request->session()->flash('success', 'Modification réussi.');
            return redirect('users/roles');
    }

    public function index()
    {
    	$users = User::where('role_id', '<>', '')->where('role_id', '<>', '4')->orderBy('id','DESC')->get();
    	//dd($users);

    	return view('admin.users.index', compact('users'));
    }

    public function create()
    {
    	$roles = Rules::where('role', '<>', 'Utilisateur')->orderBy('id','DESC')->get();

    	return view('admin.users.create', compact('roles'));
    }


    public function edit($id){
        $user = User::where('id',$id)->first();
        $roles = Rules::where('role', '<>', 'Utilisateur')->orderBy('id','DESC')->get();
        return view('admin.users.edit',compact('user','roles'));
    }

    public function update(Request $request){
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'role_id' => 'required|string|max:255',
            'telephone' => 'required|string|max:255',
            'datenaiss' => 'required|string|max:255',
            //'email' => 'required|string|email|max:255|unique:users',
            //'password' => 'required|string|min:6|confirmed'
        ]);

        if ($validator->fails()) {
            return redirect()->route('edituser',$request->slug)
                ->withErrors($validator)
                ->withInput();
        }
        else{

            $users = User::where('id',$request->slug)->first();
            $users->name = $request->name;
            $users->telephone = $request->telephone;
            $users->datenaiss = $request->datenaiss;
            $users->email = $request->email;
            $users->role_id = $request->role_id;
            $users->save();
            $request->session()->flash('success', 'Modification réussi.');
            return redirect('users/index');
        }
    }
    
     public function store(Request $request)
    {
       
	    $validator = Validator::make($request->all(), [
	    	'name' => 'required|string|max:255',
            'role_id' => 'required|string|max:255',
            'telephone' => 'required|string|max:255',
            'datenaiss' => 'required|string|max:255',
            //'email' => 'required|string|email|max:255|unique:users',
            //'password' => 'required|string|min:6|confirmed'
        ]);

        if ($validator->fails()) {
            return redirect('users/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        else{
            $newtelephone = '+'.$request->telephone;
            
	    	$users = new User();
	    	$users->name = $request->email;
	    	$users->role_id = $request->role_id;
	    	$users->telephone = $newtelephone;
	    	$users->email = $request->email;
	    	$users->datenaiss = $request->datenaiss;
	    	
	    	 //dd($request->all());
	    	$check = User::where('email', $request->email)->get();
	    	if(count($check) == 0){
	    	    $pass = User::code(6);
	    		$users->password = Hash::make($pass);
	    		$users->save();

                $emailme = $request->email;
                if($request->role_id== 1){
                    $typecompte = 'ADMINISTRATEUR';
                }else{
                    $typecompte = 'EDITEUR';
                }
                $datamail=array(
                    "contact"=>$users->telephone,
                    "datenaiss"=>$users->datenaiss,
                    "pass"=>$pass,
                    "nameUser"=>$users->name,
                    "typecompte"=>$typecompte
                );

                Mail::send('mail/inforuser',$datamail,function ($message) use($emailme){
                    $message->to($emailme);
                    $message->subject('Information de compte');
                });

	    		$request->session()->flash('success', 'Enregistrement réussi.');
	    		return redirect('users/index');
	    	}
	    	else{
	    		$request->session()->flash('error', 'Enregistrement déjà existant.');
	    		return redirect('users/index');
	    	}
    	}
    }


    public function collecte()
    {
        $user = new User;
        //dd($user);
        $user->setConnection('mysql2');
        $ville = new Ville;
        $ville->setConnection('mysql2');

        $slug = 'all';
        $countUsers = $user->where('role_id', '4')->orderBy('id', 'DESC')->with('ville')->count();
        $users = $user->where('role_id', '4')->orderBy('id', 'DESC')->with('ville')->paginate(200);
        $villes = $ville->orderBy('id', 'ASC')->get();
        //dd($villes);
        return view('admin.users.collecte', compact('villes', 'users', 'slug', 'countUsers'));
    }
    
    public function collecteshow($id)
    {
        $use = new User;
        $use->setConnection('mysql2');
        $user = $use->where('id', $id)->with('ville')->first();
        return view('admin.users.collecteshow', compact('user'));
    }

    public function excel($selectExport)
    {
        //dd($selectExport);
        $customerArray[] = array('NUM','NOM','GENRE','DATE DE NAISSANCE','CONTACT','MARQUE REGULIERE','PAQUET ACHETER','ECHANTILLONAGE','PREFERENCE','APPAREIL','EMAIL','INSTAGRAM','DATE DE MISE A JOUR');
        $i=1;
        foreach ($selectExport as $export){
            if($export->genre == 'H'){
                $export->genre = 'Homme';
            }
            else{
                $export->genre = 'Femme';
            }
            $customerArray[]= array(
                'NUM'=>$i,
                'NAME'=> $export->name,
                'SEXE'=> $export->genre,
                //'VILLE'=> $export->ville->libelle,
                'DATE DE NAISSANCE'=> date("d-m-Y",strtotime($export->datenaiss)) ,
                'CONTACT'=> $export->telephone,
                'MARQUE REGULIERE'=> $export->marque_pref,
                'PAQUET ACHETER'=> $export->marque_alt,
                'ECHANTILLONAGE'=> $export->echantillonage_pqt,
                'PREFERENCE'=> $export->pqt_acht,
                'APPAREIL'=> $export->device->nom,
                'EMAIL'=> $export->email,
                'INSTAGRAM'=> 'https://instagram.com/'.$export->instagram,
                'DATE D AJOUT'=> $export->created_at->format('d-m-Y')
            );
            $i++;
        }
        $date=new \DateTime('now');
        $date = $date->format('d-m-Y');

        Excel::create("BAT-".$date,function($excel) use($customerArray,$date){
            $excel->setTitle("BAT-".$date);
            $excel->sheet("BAT-".$date,function ($sheet) use ($customerArray){
                $sheet->fromArray($customerArray,null,'A1',false,false);
            });

        })->download('xlsx');
    }
    public function export(Request $request)
    {
        //dd($request->all());
        $lieu = $request->regular;
        $contact = $request->paquet;
        $marque = $request->preference;
        $submit = $request->btn;
        //dd($submit);

        $user = new User;
        $user->setConnection('mysql2');
        $ville = new Ville;
        $ville->setConnection('mysql2');
        $villes = $ville->orderBy('id', 'ASC')->get();
        //trie
        if($submit == 'trie'){
            if($lieu != '0'){
                //dd('d0');
               $slug = 'Marque reguilière';
               $countUsers  = $user->where('marque_pref', $lieu)->orderBy('id', 'DESC')->count();
               $users = $user->where('marque_pref', $lieu)->orderBy('id', 'DESC')->get();
               return view('admin.users.collecte', compact('villes', 'users', 'slug', 'countUsers'));
            }
            elseif($contact != '0') {
                $slug = 'Paquet Acheté';
                $countUsers  = $user->where('marque_alt', $contact)->orderBy('id', 'DESC')->count();
                $users = $user->where('marque_alt', $contact)->orderBy('id', 'DESC')->get();
                return view('admin.users.collecte', compact('villes', 'users', 'slug', 'countUsers'));
            }
            elseif($marque!='0') {
                $slug = 'Préférence';
                $countUsers  = $user->where('pqt_acht', $marque)->orderBy('id', 'DESC')->count();
                $users = $user->where('pqt_acht', $marque)->orderBy('id', 'DESC')->get();
                return view('admin.users.collecte', compact('villes', 'users', 'slug', 'countUsers'));
            }
            else{
                $slug = 'all';
                $countUsers  = $user->where('role_id', '4')->orderBy('id', 'DESC')->count();
                $users = $user->where('role_id', '4')->orderBy('id', 'DESC')->get();
                return view('admin.users.collecte', compact('villes', 'users', 'slug', 'countUsers'));
            }
        }
        //export excel
        else{
            if($lieu != '0'){
               $selectExport = $user->where('marque_pref', $lieu)->where('role_id', '4')->with('device')->orderBy('id', 'DESC')->get();
               $this->excel($selectExport);
            }

            elseif($contact!= '0') {
                $selectExport = $user->where('marque_alt' , $contact)->where('role_id', '4')->with('device')->orderBy('id', 'DESC')->get();
                //dd($selectExport);
                $this->excel($selectExport);
            }
            elseif($marque!= '0') {
                $selectExport = $user->where('pqt_acht', $marque)->where('role_id', '4')->with('device')->orderBy('id', 'DESC')->get();
                //dd($selectExport);
                $this->excel($selectExport);
            }
            else{
                $selectExport = $user->where('role_id', '4')->with('device')->orderBy('id', 'DESC')->get();
                $this->excel($selectExport);
            }
        }
    }

    public function exportphase($id){
        $user = new User;
        $user->setConnection('mysql2');
        if($id=='1'){
            $selectExport = $user->where('role_id', '4')->with('device')->orderBy('id', 'DESC')->take(65000)->get();
            $this->excel($selectExport);
        }else{
            $selectExport = $user->where('role_id', '4')->with('device')->orderBy('id', 'DESC')->skip(65000)->take(65000)->get();
            $this->excel($selectExport);
        }
    }

     public function exporturl(Request $request)
    {
        //export excel
        $selectExport = User::where('role_id', '4')->with('device')->orderBy('id', 'DESC')->get();
        $this->excel($selectExport);
    }

    public function destroy(Request $request)
    {
        $delete = $request->get('delete');
        $instance = User::find($delete);
        $instance->delete();
        $request->session()->flash('delete', 'Enregistrement supprimé.');

        return back();
    }

    public function destroyRule(Request $request)
    {
        $delete = $request->get('delete');
        $instance = Rules::find($delete);
        $instance->delete();
        $request->session()->flash('delete', 'Enregistrement supprimé.');

        return back();
    }
    
    
    public function indexprofil(){
        return view('admin.users.profil');
    }

    public function profilupdate(Request $request){
        $reponse = $request->except(['_token']);
        //dd($reponse);
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            'sexe' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->route('myprofil')->withErrors($valider->errors());
        }else{
            $user = User::where('id',Auth::user()->id)->firstOrFail();
            $user->name = $reponse['nom'];
            $user->genre = $reponse['sexe'];
            $user->save();
            return redirect()->route('myprofil')->with('success','✔ Félicitation ! modification a été modifié');
        }
    }

    public function profileditpass(Request $request){
        //dd($request->all());
        $reponse = $request->except(['_token']);
        $valider = Validator::make($request->all(),[
            'motpass' =>'max:255|min:6|required',
            'newpass' =>'max:255|min:6|required',
            'confirmpass' =>'max:255|min:6|required',
        ]);

        if($valider->fails()){
            return redirect()->route('myprofil')->withErrors($valider->errors());
        }else{
            $oldpass = $reponse['motpass'];
            $newpass = $reponse['newpass'];
            $newpassconfirm = $reponse['confirmpass'];
            $passUser = Auth::user()->password;
            if($newpass===$newpassconfirm)
            {
                if(Hash::check($oldpass,$passUser) ){

                    $user = User::find(Auth::user()->id);
                    $user->password = Hash::make($newpass);
                    $user->save();

                    return redirect()->route('myprofil')->with('success','Félicitation votre mot de passe a été mise a jour');
                }else{
                    return redirect()->route('myprofil')->with('error','Désolé ! votre mot de passe actuel est erronée');
                }
            }else{
                return redirect()->route('myprofil')->with('error','Les mots de passe sont différents');
            }

        }
    }

    public function profileditavatar(Request $request){
        //dd($request->file('fileUser'));
        $file=$request->file('fileUser');
        $fileSize=$file->getSize();
        //dd($fileSize);
        try{
            if($fileSize <= 3000263):
                $extension = $file->getClientOriginalExtension() ?: 'png';
                //$folderName = '../public_html/piges/panneau';
                $folderName ='userAvatar/';
                $picture = str_random(8).'.'. $extension;

                if (!empty(Auth::user()->img)) {
                    unlink($folderName.Auth::user()->img);
                }
                $users= User::find(Auth::user()->id);
                $users->img = $picture;
                $users->save();

                $file->move($folderName,$picture);

                return redirect()->route('myprofil')->with('success','Félicitation ! votre avatar a été mise à jours');
            else:
                return redirect()->route('myprofil')->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 3Mb');
            endif;

        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }
    
    
    public function indexmessage(){
        $allmsg = Message::with('userMessage')->with('newMessage')->orderBy('id','desc')->get();
        //dd($allmsg);
        return view('admin.users.message',compact('allmsg'));
    }

    public function indexmessageshow($id){
        $msg = Message::where('id',$id)->with('userMessage')->with('newMessage')->first();
        $msg->etat = '1';
        $msg->save();
        //dd($msg);
        return view('admin.users.messageshow',compact('msg'));
    }
    
    public function import(){
        return view('admin.users.import');
    }
    
    public function rapport(){
        $datas= null;
        //$mydevice = null;
        return view('admin.users.rapport', compact('datas'));
    }

    public function rapportshow(Request $request){

        $mydevice = $request->device;
        $date = $request->date;
        $date2 = $request->datefin;
        $btn = $request->btn;
        $dateD = $date .' 00:00:00';
        $dateF = $date2 .' 23:59:59';
        //dd($dateD,$dateF);
        $user = new User;
        $user->setConnection('mysql2');
        if($btn=='trie'){

            if($mydevice=='0'){
                $datas = $user->whereBetween('created_at',[$dateD, $dateF])
                    ->select(DB::raw('count(*) as views'), 'device_id')
                    ->groupBy('device_id')
                    ->where('device_id','!=',1)
                    ->where('device_id','!=',2)
                    ->orderBy('device_id', 'ASC')
                    ->get();
                //dd($datas);
                return view('admin.users.rapport', compact('btn','datas'));
            }else{
                $datas = $user->whereBetween('created_at',[$dateD, $dateF])
                    ->select(DB::raw('count(*) as views'), 'device_id')
                    ->groupBy('device_id')
                    ->where('device_id',$mydevice)
                    ->get();
                //dd($datas);
                return view('admin.users.rapport', compact('btn','datas'));
            }
        }else{
            if($mydevice=='0'){
                $datas = $user->whereBetween('created_at',[$dateD, $dateF])
                    ->select(DB::raw('count(*) as views'), 'device_id')
                    ->groupBy('device_id')
                    ->where('device_id','!=',1)
                    ->where('device_id','!=',2)
                    ->orderBy('device_id', 'ASC')
                    ->get();
            }else{
                $datas = $user->whereBetween('created_at',[$dateD, $dateF])
                    ->select(DB::raw('count(*) as views'), 'device_id')
                    ->groupBy('device_id')
                    ->where('device_id',$mydevice)
                    ->get();
            }
            //dd($datas);
            $this->excelRapport($datas);

            return view('admin.users.rapport', compact('btn','datas'));
        }
        //dd($request->all());

    }

    public function excelRapport($datas){
        $customerArray[] = array('APPAREIL','NOMBRE D ENREGISTREMENT');

        foreach ($datas as $export){
            $customerArray[]= array(
                'APPAREIL'=>$export->device_id,
                'NOMBRE D ENREGISTREMENT'=> $export->views
                //'DATE D AJOUT'=> $export->created_at->format('d-m-Y')
            );
        }
        $date=new \DateTime('now');
        $date = $date->format('d-m-Y');

        Excel::create("BAT-RAPPORT-".$date,function($excel) use($customerArray,$date){
            $excel->setTitle("BAT-RAPPORT-".$date);
            $excel->sheet("BAT-RAPPORT-".$date,function ($sheet) use ($customerArray){
                $sheet->fromArray($customerArray,null,'A1',false,false);
            });

        })->download('xlsx');
    }


    //NEW ROUTE
    public function collectePdt1()
    {
        $slug = 'all';
        $countUsers = Collecte::where('type_pdt', 'dunhill')->orderBy('id', 'DESC')->count();
        $users = Collecte::where('type_pdt', 'dunhill')->orderBy('id', 'DESC')->with('roving')->paginate(200);
        //$villes = Ville::orderBy('id', 'ASC')->get();
        //dd($users[0]->ville->libelle);
        return view('admin.users.collectepdt1', compact('users', 'slug', 'countUsers'));
    }

    public function collectePdt2()
    {
        $slug = 'all';
        $countUsers = Collecte::where('type_pdt', 'cravenarouge')->orderBy('id', 'DESC')->count();
        $users = Collecte::where('type_pdt', 'cravenarouge')->orderBy('id', 'DESC')->with('roving')->paginate(200);
        //$villes = Ville::orderBy('id', 'ASC')->get();
        //dd($users[0]->ville->libelle);
        return view('admin.users.collectepdt2', compact('users', 'slug', 'countUsers'));
    }

    public function excelPdt1($selectExport)
    {
        //dd($selectExport);
        $customerArray[] = array('NUM','NOM','SEXE','CONTACT','DATE DE NAISSANCE','MOYEN DE CONTACT','MOYEN DE CONTACT VALEUR','MARQUE REGULIERE','ECHANTILLONAGE','PAQUET ACHETER','VARIANTE ACHETER','LOT','ADRESSE QUARTIER','ADRESSE COMMUNE','ADRESSE VILLE','ADRESSE REGION','ROVING TEAM','DATE DE MISE A JOUR');
        $i=1;
        foreach ($selectExport as $export){

            $customerArray[]= array(
                'NUM'=>$i,
                'NOM'=> $export->nom,
                'SEXE'=> $export->genre,
                'CONTACT'=> $export->telephone,
                'DATE DE NAISSANCE'=> date("d-m-Y",strtotime($export->datenaiss)) ,
                'MOYEN DE CONTACT'=> $export->moy_contact,
                'MOYEN DE CONTACT VALEUR'=> $export->moy_contact_value,
                'MARQUE REGULIERE'=> $export->marque_reg,
                'ECHANTILLONAGE'=> $export->echantillonage_pqt,
                'PAQUET ACHETER'=> $export->pqt_acht,
                'VARIANTE ACHETER'=> $export->variante_acht,
                'LOT'=> $export->lot,
                'ADRESSE QUARTIER'=> $export->ads_quartier,
                'ADRESSE COMMUNE'=> $export->ads_commun,
                'ADRESSE VILLE'=> $export->ads_ville,
                'ADRESSE REGION'=> $export->ads_region,
                'ROVING TEAM ID'=> $export->roving->identifiant,
                'ROVING TEAM NOM'=> $export->roving->nom,
                'DATE D AJOUT'=> $export->created_at->format('d-m-Y')
            );
            $i++;
        }
        $date=new \DateTime('now');
        $date = $date->format('d-m-Y');

        Excel::create("BAT-".$date,function($excel) use($customerArray,$date){
            $excel->setTitle("BAT-".$date);
            $excel->sheet("BAT-".$date,function ($sheet) use ($customerArray){
                $sheet->fromArray($customerArray,null,'A1',false,false);
            });

        })->download('xlsx');
    }

    public function collectePdt1export(Request $request)
    {
        //dd($request->all());
        //$villes = Ville::orderBy('id', 'ASC')->get();
        //dd($request->all());
        //$lieu = Marque regulière | $contact = Variante Acheté | $marque = Moyen de contact

        $lieu = $request->regular;
        $contact = $request->paquet;
        $marque = $request->preference;
        $submit = $request->btn;
        //dd($submit);

        //trie
        if($submit == 'trie'){
            if($lieu != '0'){
                //dd('d0');
                $slug = 'Marque reguilière';
                $countUsers  = Collecte::where('type_pdt', 'dunhill')->where('marque_reg', $lieu)->orderBy('id', 'DESC')->count();
                $users = Collecte::where('type_pdt', 'dunhill')->where('marque_reg', $lieu)->orderBy('id', 'DESC')->get();
                return view('admin.users.collectepdt1', compact('users', 'slug', 'countUsers'));
            }
            elseif($contact != '0') {
                $slug = 'Variante Acheté';
                $countUsers  = Collecte::where('type_pdt', 'dunhill')->where('variante_acht', $contact)->orderBy('id', 'DESC')->count();
                $users = Collecte::where('type_pdt', 'dunhill')->where('variante_acht', $contact)->orderBy('id', 'DESC')->get();
                return view('admin.users.collectepdt1', compact('users', 'slug', 'countUsers'));
            }
            elseif($marque!='0') {
                $slug = 'Moyen de contact';
                $countUsers  = Collecte::where('type_pdt', 'dunhill')->where('moy_contact', $marque)->orderBy('id', 'DESC')->count();
                $users = Collecte::where('type_pdt', 'dunhill')->where('moy_contact', $marque)->orderBy('id', 'DESC')->get();
                return view('admin.users.collectepdt1', compact('users', 'slug', 'countUsers'));
            }
            else{
                $slug = 'all';
                $countUsers  = Collecte::where('type_pdt', 'dunhill')->orderBy('id', 'DESC')->count();
                $users = Collecte::where('type_pdt', 'dunhill')->orderBy('id', 'DESC')->get();
                return view('admin.users.collectepdt1', compact('users', 'slug', 'countUsers'));
            }
        }
        //export excel
        else{
            if($lieu != '0'){
                $selectExport = Collecte::where('type_pdt', 'dunhill')->where('marque_reg', $lieu)->orderBy('id', 'DESC')->get();
                $this->excelPdt1($selectExport);
            }

            elseif($contact!= '0') {
                $selectExport = Collecte::where('type_pdt', 'dunhill')->where('variante_acht', $contact)->orderBy('id', 'DESC')->get();
                //dd($selectExport);
                $this->excelPdt1($selectExport);
            }
            elseif($marque!= '0') {
                $selectExport = Collecte::where('type_pdt', 'dunhill')->where('moy_contact', $marque)->orderBy('id', 'DESC')->get();
                //dd($selectExport);
                $this->excelPdt1($selectExport);
            }
            else{
                $selectExport = Collecte::where('type_pdt', 'dunhill')->with('roving')->orderBy('id', 'DESC')->get();
                $this->excelPdt1($selectExport);
            }
        }
    }

    public function collectePdt2export(Request $request)
    {
        //dd($request->all());
        //$villes = Ville::orderBy('id', 'ASC')->get();
        //dd($request->all());
        //$lieu = Marque regulière | $contact = Variante Acheté | $marque = Moyen de contact

        $lieu = $request->regular;
        $contact = $request->paquet;
        $marque = $request->preference;
        $submit = $request->btn;
        //dd($submit);

        //trie
        if($submit == 'trie'){
            if($lieu != '0'){
                //dd('d0');
                $slug = 'Marque reguilière';
                $countUsers  = Collecte::where('type_pdt', 'cravenarouge')->where('marque_reg', $lieu)->orderBy('id', 'DESC')->count();
                $users = Collecte::where('type_pdt', 'cravenarouge')->where('marque_reg', $lieu)->orderBy('id', 'DESC')->get();
                return view('admin.users.collectepdt2', compact('users', 'slug', 'countUsers'));
            }
            elseif($contact != '0') {
                $slug = 'Variante Acheté';
                $countUsers  = Collecte::where('type_pdt', 'cravenarouge')->where('variante_acht', $contact)->orderBy('id', 'DESC')->count();
                $users = Collecte::where('type_pdt', 'cravenarouge')->where('variante_acht', $contact)->orderBy('id', 'DESC')->get();
                return view('admin.users.collectepdt2', compact('users', 'slug', 'countUsers'));
            }
            elseif($marque!='0') {
                $slug = 'Moyen de contact';
                $countUsers  = Collecte::where('type_pdt', 'cravenarouge')->where('moy_contact', $marque)->orderBy('id', 'DESC')->count();
                $users = Collecte::where('type_pdt', 'cravenarouge')->where('moy_contact', $marque)->orderBy('id', 'DESC')->get();
                return view('admin.users.collectepdt2', compact('users', 'slug', 'countUsers'));
            }
            else{
                $slug = 'all';
                $countUsers  = Collecte::where('type_pdt', 'cravenarouge')->orderBy('id', 'DESC')->count();
                $users = Collecte::where('type_pdt', 'cravenarouge')->orderBy('id', 'DESC')->get();
                return view('admin.users.collectepdt2', compact('users', 'slug', 'countUsers'));
            }
        }
        //export excel
        else{
            if($lieu != '0'){
                $selectExport = Collecte::where('type_pdt', 'cravenarouge')->where('marque_reg', $lieu)->orderBy('id', 'DESC')->get();
                $this->excelPdt1($selectExport);
            }

            elseif($contact!= '0') {
                $selectExport = Collecte::where('type_pdt', 'cravenarouge')->where('variante_acht', $contact)->orderBy('id', 'DESC')->get();
                //dd($selectExport);
                $this->excelPdt1($selectExport);
            }
            elseif($marque!= '0') {
                $selectExport = Collecte::where('type_pdt', 'cravenarouge')->where('moy_contact', $marque)->orderBy('id', 'DESC')->get();
                //dd($selectExport);
                $this->excelPdt1($selectExport);
            }
            else{
                $selectExport = Collecte::where('type_pdt', 'cravenarouge')->with('roving')->orderBy('id', 'DESC')->get();
                $this->excelPdt1($selectExport);
            }
        }
    }

    public function duncrashow($id)
    {
        $user = Collecte::where('id', $id)->with('roving')->first();
        return view('admin.users.collecteshow_new', compact('user'));
    }

    public function rapportDunhill(){
        $datas= null;
        $roving = Roving::get();
        return view('admin.users.rapport_dunhill', compact('datas','roving'));
    }

    public function rapportCraven(){
        $datas= null;
        $roving = Roving::get();
        return view('admin.users.rapport_craven', compact('datas','roving'));
    }

    public function excelRapport2($datas,$pdt){
        $customerArray[] = array('PRODUIT','ROVING TEAM','NOMBRE D ENREGISTREMENT');

        foreach ($datas as $export){
            $customerArray[]= array(
                'PRODUIT'=>$pdt,
                'ROVING TEAM'=>$export->device_id,
                'NOMBRE D ENREGISTREMENT'=> $export->views
                //'DATE D AJOUT'=> $export->created_at->format('d-m-Y')
            );
        }
        $date=new \DateTime('now');
        $date = $date->format('d-m-Y');

        Excel::create("BAT-RAPPORT-".$date,function($excel) use($customerArray,$date){
            $excel->setTitle("BAT-RAPPORT-".$date);
            $excel->sheet("BAT-RAPPORT-".$date,function ($sheet) use ($customerArray){
                $sheet->fromArray($customerArray,null,'A1',false,false);
            });

        })->download('xlsx');
    }

    public function rapportDunhillShow(Request $request){

        $mydevice = $request->device;
        $date = $request->date;
        $date2 = $request->datefin;
        $btn = $request->btn;
        $dateD = $date .' 00:00:00';
        $dateF = $date2 .' 23:59:59';
        //dd($dateD,$dateF);
        $roving = Roving::get();
        if($btn=='trie'){

            if($mydevice=='0'){
                $datas = Collecte::whereBetween('created_at',[$dateD, $dateF])
                    ->select(DB::raw('count(*) as views'), 'roving_id')
                    ->groupBy('roving_id')
                    ->where('type_pdt','dunhill')
                    ->orderBy('roving_id', 'ASC')
                    ->get();
                //dd($datas);
                return view('admin.users.rapport_dunhill', compact('btn','datas','roving'));
            }else{
                $datas = Collecte::whereBetween('created_at',[$dateD, $dateF])
                    ->select(DB::raw('count(*) as views'), 'roving_id')
                    ->groupBy('roving_id')
                    ->where('roving_id',$mydevice)
                    ->where('type_pdt','dunhill')
                    ->get();
                //dd($datas);
                return view('admin.users.rapport_dunhill', compact('btn','datas','roving'));
            }
        }else{
            if($mydevice=='0'){
                $datas = Collecte::whereBetween('created_at',[$dateD, $dateF])
                    ->select(DB::raw('count(*) as views'), 'roving_id')
                    ->groupBy('roving_id')
                    ->where('type_pdt','dunhill')
                    ->orderBy('roving_id', 'ASC')
                    ->get();
            }else{
                $datas = Collecte::whereBetween('created_at',[$dateD, $dateF])
                    ->select(DB::raw('count(*) as views'), 'roving_id')
                    ->groupBy('roving_id')
                    ->where('type_pdt','dunhill')
                    ->where('roving_id',$mydevice)
                    ->get();
            }
            $pdt = 'DUNHILL';
            //dd($datas);
            $this->excelRapport2($datas,$pdt);

            return view('admin.users.rapport_dunhill', compact('btn','datas','roving'));
        }
        //dd($request->all());

    }

    public function rapportCravenShow(Request $request){

        $mydevice = $request->device;
        $date = $request->date;
        $date2 = $request->datefin;
        $btn = $request->btn;
        $dateD = $date .' 00:00:00';
        $dateF = $date2 .' 23:59:59';
        //dd($dateD,$dateF);
        $roving = Roving::get();
        if($btn=='trie'){

            if($mydevice=='0'){
                $datas = Collecte::whereBetween('created_at',[$dateD, $dateF])
                    ->select(DB::raw('count(*) as views'), 'roving_id')
                    ->groupBy('roving_id')
                    ->where('type_pdt','cravenarouge')
                    ->orderBy('roving_id', 'ASC')
                    ->get();
                //dd($datas);
                return view('admin.users.rapport_craven', compact('btn','datas','roving'));
            }else{
                $datas = Collecte::whereBetween('created_at',[$dateD, $dateF])
                    ->select(DB::raw('count(*) as views'), 'roving_id')
                    ->groupBy('roving_id')
                    ->where('roving_id',$mydevice)
                    ->where('type_pdt','cravenarouge')
                    ->get();
                //dd($datas);
                return view('admin.users.rapport_craven', compact('btn','datas','roving'));
            }
        }else{
            if($mydevice=='0'){
                $datas = Collecte::whereBetween('created_at',[$dateD, $dateF])
                    ->select(DB::raw('count(*) as views'), 'roving_id')
                    ->groupBy('roving_id')
                    ->where('type_pdt','cravenarouge')
                    ->orderBy('roving_id', 'ASC')
                    ->get();
            }else{
                $datas = Collecte::whereBetween('created_at',[$dateD, $dateF])
                    ->select(DB::raw('count(*) as views'), 'roving_id')
                    ->groupBy('roving_id')
                    ->where('type_pdt','cravenarouge')
                    ->where('roving_id',$mydevice)
                    ->get();
            }
            $pdt = 'CRAVEN A ROUGE';
            //dd($datas);
            $this->excelRapport2($datas,$pdt);

            return view('admin.users.rapport_craven', compact('btn','datas','roving'));
        }
        //dd($request->all());

    }


    public function roving(){
        $rovings = Roving::get();
        return view('admin.users.rovingteam',compact('rovings'));
    }

    public function roving_create(){
        return view('admin.users.rovingteam_create');
    }

    public function roving_store(Request $request){
        //dd($request->all());

        Validator::make($request->all(),[
            'name' =>'required',
            'telephone' =>'required',
            'sexe' =>'required',
            'password' =>'required',
            'password_confirmation' =>'required',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
        ])->validate();

        if($request->password != $request->password_confirmation){
            $request->session()->flash('error', 'les Mot de passe ne conrespondants pas.');
            return redirect()->back();
        }

        //create
        $allcount = Roving::count();
        $rov = new Roving();
        $rov->identifiant= "BAT-".sprintf("%'.04d", $allcount+1);
        $rov->nom = $request->name ;
        $rov->contact = $request->telephone ;
        $rov->adresse = $request->adresse ;
        $rov->sexe = $request->sexe ;
        $rov->password = $request->password;
        //$rov->password = Hash::make($request->password);
        $rov->save();

        $request->session()->flash('success', 'Enregistrement réussi.');
        return redirect('users/rovingteam');

    }

    public function roving_edit($id){
        $roving = Roving::where('identifiant',$id)->first();
        return view('admin.users.rovingteam_edit',compact('roving'));
    }

    public function roving_delete($id){
        $roving = Roving::where('identifiant',$id)->first();
        $roving->delete();
        session()->flash('success', 'Suppression réussi.');
        return redirect('users/rovingteam');
    }

    public function roving_update(Request $request){
        //dd($request->all());

        Validator::make($request->all(),[
            'name' =>'required',
            'telephone' =>'required',
            'sexe' =>'required',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
        ])->validate();

        if($request->password != $request->password_confirmation){
            $request->session()->flash('error', 'les Mot de passe ne conrespondants pas.');
            return redirect()->back();
        }

        //create
        $rov = Roving::where('identifiant',$request->id)->first();
        $rov->nom = $request->name ;
        $rov->contact = $request->telephone ;
        $rov->adresse = $request->adresse ;
        $rov->sexe = $request->sexe ;
        if($request->password != null){
            $rov->password = $request->password;
        }
        //$rov->password = Hash::make($request->password);
        $rov->save();

        $request->session()->flash('success', 'Modification réussi.');
        return redirect('users/rovingteam');

    }
}
