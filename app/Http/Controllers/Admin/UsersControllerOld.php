<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

use App\Rules;
use App\User;
use App\Ville;
use Session;
use Response;

class UsersController extends Controller
{

    public function roles()
    {
    	$roles = Rules::orderBy('id','DESC')->get();

    	return view('admin.users.roles', compact('roles'));
    }

    public function postroles(Request $request)
    {
    	$roles = new Rules($request->all());
    	$check = Rules::where('role', $request->role)->get();
    	if(count($check) == 0){
    		$roles->save();
    		$request->session()->flash('success', 'Enregistrement réussi.');
    		return redirect('users/roles');
    	}
    	else{
    		$request->session()->flash('error', 'Enregistrement déjà existant.');
    		return redirect('users/roles');
    	}

    }

    public function index()
    {
    	$users = User::where('role_id', '<>', '')->where('role_id', '<>', '4')->orderBy('id','DESC')->get();

    	return view('admin.users.index', compact('users'));
    }

    public function create()
    {
    	$roles = Rules::where('role', '<>', 'Utilisateur')->orderBy('id','DESC')->get();

    	return view('admin.users.create', compact('roles'));
    }

    public function store(Request $request)
    {

	    $validator = Validator::make($request->all(), [
	    	'name' => 'required|string|max:255',
            'role_id' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed'
        ]);

        if ($validator->fails()) {
            return redirect('users/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        else{

	    	$users = new User($request->all());
	    	$check = User::where('email', $request->email)->get();
	    	if(count($check) == 0){
	    		$users->password = Hash::make($request->password);
	    		$users->save();
	    		$request->session()->flash('success', 'Enregistrement réussi.');
	    		return redirect('users/index');
	    	}
	    	else{
	    		$request->session()->flash('error', 'Enregistrement déjà existant.');
	    		return redirect('users/index');
	    	}
    	}
    }


    public function collecte()
    {
        $users = User::where('role_id', '4')->get();
        $villes = Ville::orderBy('id', 'ASC')->get();
        
        return view('admin.users.collecte', compact('villes', 'users'));
    }

    public function export(Request $request)
    {
        // $headers = array(
        // "Content-type" => "text/csv",
        // "Content-Disposition" => "attachment; filename=file.csv",
        // "Pragma" => "no-cache",
        // "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        // "Expires" => "0"
        // );

        // $reviews = User::orderBy('id','DESC')->select('name')->get();
        
        // $columns = array('Nom');

        // $callback = function() use ($reviews, $columns)
        // {
        //     $file = fopen('php://output', 'w');
        //     fputcsv($file, $columns);

        //     foreach($reviews as $review) {
        //         dd($review);
        //         fputcsv($file, array($review->name));
        //     }
        //     fclose($file);
        // };
        //return Response::stream($callback, 200, $headers);
    }

    public function destroy(Request $request)
    {
        $delete = $request->get('delete');
        $instance = User::find($delete);
        $instance->delete();
        $request->session()->flash('delete', 'Enregistrement supprimé.');

        return back();
    }

    public function destroyRule(Request $request)
    {
        $delete = $request->get('delete');
        $instance = Rules::find($delete);
        $instance->delete();
        $request->session()->flash('delete', 'Enregistrement supprimé.');

        return back();
    }
}
