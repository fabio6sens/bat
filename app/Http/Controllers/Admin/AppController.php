<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\User;
use App\Device;
use DB;

class AppController extends Controller
{
    public function index()
    {
        //dd("ici");
    	$postUrl = "https://api.xselsms.com/sendsms/account/2a0975526878977eda639fa9de41eTH81kXfppwMWpULzq6S4psJL6a9C";
    	$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $postUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

		// Response of the POST request
		$response = curl_exec($ch);
		curl_close($ch);
		$contents = json_decode($response, true);
	    $sms_send = $contents['Crédit'];
	   	//dd($sms_send);
		//Utilisateurs collectés
		$users = User::where('ville_id', '<>', null)->count();
        //dd($users);
		//Most perform device
		$devices = DB::table('users')
            ->select(DB::raw('count(*) as devices_count, device_id'))
            ->groupBy('device_id')
            ->orderBy('devices_count', 'DESC')
            ->where('device_id', '<>', 0)
            ->get();
        
        if(count($devices) != 0){
            $device = Device::findOrFail($devices[0]->device_id);
            $device_name = $device->nom;
        }else{
            $device_name = "Aucun appareil";
        }
        
        //Sms délivrés
        $devices_delivred = DB::table('campagne_users')
            ->select(DB::raw('count(*) as statut'))
            ->where('statut', 'DELIVERED_TO_OPERATOR')
            ->get();
        
        foreach ($devices_delivred as $value) {
            //dd($value);
        	$devices_delivred = $value;
        }
        
    	return view('admin.index', compact('sms_send', 'users', 'device_name', 'devices_delivred'));
    }
}
