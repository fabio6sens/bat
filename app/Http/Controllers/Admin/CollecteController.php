<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Rules;
use App\User;
use App\Ville;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Response;

class CollecteController extends Controller
{
    public function instagram()
    {
    	$slug = 'instagram';
        $users = User::where('role_id', '4')->where('instagram', '<>', null)->get();
        //dd($users);
        return view('admin.users.collecteInstagram', compact('slug', 'users'));
    }

    public function export($slug)
    {
		$customerArray[] = array('NUM','NOM','GENRE','VILLE','CONTACT','MARQUE PREFERE','MARQUE','ECHANTILLONAGE','PAQUET ACHETER','EMAIL','INSTAGRAM','DATE DE MISE A JOUR');
		if ($slug != 'instagram') {
			$selectExport = User::where('role_id', '4')->where('instagram', $slug)->orderBy('id', 'DESC')->get();
		}
		else{
			$selectExport = User::where('role_id', '4')->orderBy('id', 'DESC')->get();
		}

        $i=1;
        foreach ($selectExport as $export){
        	if($export->genre == 'H'){
        		$export->genre = 'Homme';
        	}
        	else{
        		$export->genre = 'Femme';
        	}
            $customerArray[]= array(
                'NUM'=>$i,
                'NAME'=> $export->name,
                'SEXE'=> $export->genre,
                'VILLE'=> $export->ville->libelle,
                'CONTACT'=> $export->telephone,
                'MARQUE PREFERE'=> $export->marque_pref,
                'MARQUE'=> $export->marque_alt,
                'ECHANTILLONAGE'=> $export->echantillonage_pqt,
                'PAQUET ACHETER'=> $export->pqt_acht,
                'EMAIL'=> $export->email,
                'INSTAGRAM'=> 'https://instagram.com/'.$export->instagram,
                'DATE D AJOUT'=> $export->created_at->format('d-m-Y')
            );
            $i++;
        }
        $date=new \DateTime('now');
        $date = $date->format('d-m-Y');

        Excel::create("BAT-".$slug."-".$date,function($excel) use($customerArray,$date,$slug){
            $excel->setTitle("BAT-".$slug."-".$date);
            $excel->sheet("BAT-".$slug."-".$date,function ($sheet) use ($customerArray){
                $sheet->fromArray($customerArray,null,'A1',false,false);
            });

        })->download('xlsx');
    }
}
