<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Ville;
use App\User;
use App\Campagne;
use App\CampagneUsers;
use DB;

class SmsController extends Controller
{
    public static function ville($id)
    {
    	$ville = Ville::findOrFail($id);
    	//dd($ville);
    	return $ville->libelle;
    }

	public function index()
    {
    	$campagnes = Campagne::orderBy('id', 'DESC')->get();

    	return view('admin.sms.index', compact('campagnes'));
    }

    public function create()
    {
    	$villes = Ville::orderBy('id', 'ASC')->get();

    	return view('admin.sms.create', compact('villes'));
    }

    public function details($id)
    {
    	$campagne = Campagne::findOrFail($id);

    	$users_campagne = DB::table('campagne_users')
            ->join('users', 'campagne_users.telephone', '=', 'users.telephone')
            ->join('campagne', 'campagne.id', '=', 'campagne_users.id_campagne')
            ->where('campagne.id', $id)
            ->select('users.telephone', 'users.ville_id', 'users.name', 'campagne.titre','campagne_users.statut')
            ->get();

        //dd($users_campagne);

        $countTotal = CampagneUsers::where('id_campagne', $id)->count();
        $countSend = CampagneUsers::where('id_campagne', $id)->where('statut', 'DELIVERED_TO_OPERATOR')->count();
        $countNotSend = CampagneUsers::where('id_campagne', $id)->where('statut', 'NOT_ENOUGH_CREDITS')->count();

    	return view('admin.sms.details', compact('campagne','users_campagne','countNotSend','countTotal','countSend'));
    }

    public function store(Request $request)
    {
    	$params = $request->all();
    	$titre = $request->titre;
    	$msg = $request->msg;
        $allPefer = $request->pefer;
        //dd($params);

        if(isset($allPefer)=== true){
            if($allPefer[0]!='0'){
                foreach ($allPefer as $value) {
                    $users[] = User::where('role_id', '4')->where('pqt_acht','commune')->get();
                }

                foreach ($users as $use) {
                    foreach ($use as $us) {
                        if($us->telephone != null AND $us->telephone != '+22500000000')
                            $phone[] = $us->telephone;
                    }
                }
                //dd($phone);
            }
        }
        //dd('null');
        //Récuperer tous les contacts par la selection des zone (Select)
        if(true === isset($request->param)){
            //Option all select
            $allSelected = $request->param;
            if($allSelected[0] == 0){
                $users[] = User::where('role_id', '4')->get();

                //Tableau de numeros de téléphone
                foreach ($users as $user) {
                    foreach ($user as $v) {
                        if($v->telephone != null AND $v->telephone != '+22500000000')
                        $phone[] = $v->telephone;
                    }
                }
            }
            //Option par marque regulière
            else{
                foreach ($params as $value) {
                    $users[] = User::where('role_id', '4')->where('marque_pref', $value)->get();
                }

                //Tableau de numeros de téléphone
                foreach ($users as $user) {
                    foreach ($user as $v) {
                        if($v->telephone != null AND $v->telephone != '+22500000000')
                        $phone[] = $v->telephone;
                    }
                }
            }
        }
        //Récuperer tous les contacts saisis (Tags)
    	elseif(true === isset($request->tags)){
            $tags = $request->tags;
            $tags = explode(',', $tags);
            foreach ($tags as $value) {
                $users[] = '+225'.$value;
            }

            //Tableau de numeros de téléphone
            foreach ($users as $user) {
               if($user != null)
                $phone[] = $user;
                //dd($phone);
            }
        }
        else{
            $request->session()->flash('reject', 'Veuillez ajouter un destinataire svp.');
            return redirect('sms/create');
        }
        //dd($params);
    	// XselSMS API POST URL
		$postUrl = "https://api.xselsms.com/sendsms/json/";

		$jsonArray = [
			'authentification' => [
				"apiKey" => '2a0975526878977eda639fa9de41eTH81kXfppwMWpULzq6S4psJL6a9C'
			],
			'message' => [
				[
					"text" => $msg,
					"from" => "BAT",
					"to" => $phone
				]
			]
		];
		
		$jsonString = json_encode($jsonArray);

	 	// Insertion of the POST “JSON” variable name before the data in JSON format
 		$fields = urlencode($jsonString); 

	 	// In this example, the POST request is completed thanks to the Curl library
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $postUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonString);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

		// Response of the POST request
		$response = curl_exec($ch);
		curl_close($ch);

	 	// Writing of the response
	 	$contents = json_decode($response, true);
	 	$status = [];

	 	if (false === is_array($contents['messages'])) {
	 		$request->session()->flash('loose', 'L\'envoi a échoué.');
            return redirect('sms/create');
	 	}

	 	foreach ($contents['messages'] as $value) {
	 		if (false === isset($value['to']) || false === isset($value['status'])) {
	 			continue;
	 		}

	 		$status[$value['to']] = $value['status']['name'] ?? null;
	 	}

	 	$campagnes = new Campagne($request->all());

	 	$campagnes->titre = $request->titre;
	 	$campagnes->message = $request->msg;
	 	$campagnes->save();

	 	$last_insert_id = $campagnes->id;

	 	foreach ($status as $k => $value) {
	 		DB::insert('insert into campagne_users (id_campagne, telephone, statut) values (?, ?, ?)', 
	 					[$last_insert_id, $k, $value]);
	 	}
        $request->session()->flash('succes', 'Envois réussis.');
	 	return redirect('sms/index');
    }

    public function destroy(Request $request)
    {
        $delete = $request->get('delete');
        $instance = Campagne::find($delete);
        $instance->delete();
        $request->session()->flash('delete', 'Enregistrement supprimé.');

        return back();
    }
}
