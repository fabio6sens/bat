<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if($user->role_id=='4'){
            return redirect('/mon-compte');

        }else{
            return redirect('/admin');
        }
    }
    
    
    public function loginme(Request $request)
    {
        $reponse =$request->except(['_token']);
        //dd($reponse);
        $valider = Validator::make($request->all(),[
            'email' => 'required',
            'jour' => 'required',
            'mois' => 'required',
            'indice' => 'required',
            'password' => 'required|string',
        ]);

        $password = $reponse['password'] .'-'.$reponse['mois'].'-'.$reponse['jour'];
        //dd($password);
        $email = $reponse['indice'].$reponse['email'];

        if($valider->fails()){
            /*throw  ValidationException::withMessages([
                "email" => [trans('auth.failed')],
            ]);*/
            return redirect()->route('login')->withErrors($valider->errors());
        }else{
            $checkUser = User::where('telephone',$email)->where('datenaiss',$password)->first();
            //dd($reponse);

            if($checkUser){
                if($checkUser->role_id ==4){
                    Auth::loginUsingId($checkUser->id);
                    return redirect('/');
                }else{
                    //dd(base64_encode($checkUser->email));
                    return redirect()->route('getconfirm',base64_encode($checkUser->email));
                    //return view('admin/userconfirm',compact('checkUser'));
                    //return redirect('login');
                }
            }else{
                return redirect()->route('login')->with('error','Contact ou date de naissance incorrect');
            }
            //dd($checkUser);
        }
    }

    public function getconfirm($slug){
        $decode = base64_decode($slug);
        //dd($decode);
        $decodencode = $slug;
        $checkUser = User::where('email',$decode)->first();
        if($checkUser){
            return view('admin/userconfirm',compact('decodencode'));
        }else{
            return redirect()->route('login');
        }
    }

    public function loginadminme(Request $request)
    {
        $reponse =$request->except(['_token']);
        //dd($reponse);
        /*$valider = Validator::make($request->all(),[
            'password' => 'required|string',
        ]);*/
        $slug_encode = $reponse['slug'];
        $slug_decode = base64_decode($slug_encode);
        $checkUser = User::where('email',$slug_decode)->first();

        if(Hash::check($reponse['password'],$checkUser->password)){
            //dd($checkUser);
            Auth::loginUsingId($checkUser->id);
            return redirect('/admin');
        }else{
            return redirect()->route('getconfirm',compact('slug_encode'))->with('error','Mot de passe incorrect');

        }
    }
}

