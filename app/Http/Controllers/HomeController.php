<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function creatpass($token){
        $userExit = User::where('code_acces',$token)->first();
        if($userExit){
            return view('frontend/createdpass',compact('userExit'));
        }else{
            return redirect()->route('login')->with('error','Désolé ! ce lien ne semble plus valide');
        }

    }

    public function savecreatpass(Request $request){
        $response=$request->all();
        //dd($response);
        $valider =  Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ]);
        $firstUser = User::where('id',$response['slug'])->first();

        if($valider->fails()){
            return redirect()->route('creatpass',$firstUser->code_acces)->withErrors($valider->errors());
        }else{
            //dd($valider->errors());
            $firstUser->password = Hash::make($response['password']);
            $firstUser->code_acces = '' ;
            $firstUser->save();
            return redirect()->route('login');
        }
    }
}
