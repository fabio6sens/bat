<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\Ville;

use Illuminate\Support\Facades\Mail;
use Validator;
use URL;

use App\User;
use Illuminate\Support\Facades\Auth;

class MobileController extends Controller
{
    public function initDevice(Request $request)
    {
        try {
            $device = Device::where('uuid', $request->uuid)->firstOrfail();
        } catch (\Exception $e) {
            $count = Device::all()->count();
            $device = new Device;
            $device->nom = 'Appareil '.($count+1);
            $device->uuid = $request->uuid;
            $device->serial = $request->serial;
            $device->manufacturer = $request->manufacturer;
            $device->model = $request->model;
            $device->save();
        }

        $return = [
            'villes' => Ville::all()->toArray(),
            'device' => $device->toArray(),
        ];

        return response()->json($return);
    }

    public function sendSms($msg, $to)
    {
        // XselSMS API POST URL
        $postUrl = "https://api.xselsms.com/sendsms/json/";

        $jsonArray = [
            'authentification' => [
                "apiKey" => '2a0975526878977eda639fa9de41eTH81kXfppwMWpULzq6S4psJL6a9C'
                // "apiKey" => '2a0934ae1b9b315d135374e52OxsGuHIyUPGDQA2VfgxvRvr1Y37xqoJG'
            ],
            'message' => [
                [
                    "text" => $msg,
                    "from" => "BAT",
                    "to" => $to
                ]
            ]
        ];
        
        $jsonString = json_encode($jsonArray);

        // Insertion of the POST “JSON” variable name before the data in JSON format
        $fields = urlencode($jsonString); 

        // In this example, the POST request is completed thanks to the Curl library
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $postUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonString);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        // Response of the POST request
        $response = curl_exec($ch);
        curl_close($ch);

        // Writing of the response
        // echo $response; 
    }

    public function saveUser(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            //'email' => 'required|email|unique:users,email',
            'telephone' => 'required|unique:users,telephone'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 'ERROR',
                'message' => 'Ce client existe déjà.' 
            ]);
        }

        $code = \App\User::code();
        while(\App\User::where('code_acces', $code)->count() > 0){
            $code = \App\User::code();
        }

        try {
            $user = new \App\User;
            $user->name = $request->name;
            $user->genre = $request->genre;
            $user->ville_id = $request->ville_id;
            $user->telephone = '+225'.$request->telephone;
            $user->moy_contact = $request->moy_contact;
            $user->marque_pref = $request->marque_pref;
            $user->marque_alt = $request->marque_alt;
            $user->datenaiss = $request->date;
            $user->echantillonage_pqt = $request->echantillonage_pqt;
            $user->pqt_acht = $request->pqt_acht;
            $user->email = $request->email;
            $user->instagram = $request->instagram;
            $user->device_id = $request->device_id;
            $user->role_id = 4;
            $user->code_acces = $code;
            $user->password = '';
            $user->save();

            $data=array(
                "token"=>$user->code_acces,
                "nameUser"=>$user->name,
            );
            $emailme = $user->email;

            Mail::send('mail/validmail', $data, function ($message) use($emailme){
                $message->to($emailme);
                $message->subject('Acces au site internet');
            });

            // Send SMS
            $url_site = URL::to('/');
            $msg = 'Vous êtes desormais un membre privilégié. Accedez au site via le lien suivant: '.$url_site;
            $this->sendSms($msg, [$user->telephone]);

            $return = [
                'code' => 'OK',
                'message' => 'Client Ajouté avec succès.'
            ];


        } catch (\Exception $e) {
            $return = [
                'code' => 'ERROR',
                // 'message' => 'Erreur lors de l\'ajout. Veuillez réessayer SVP!'
                'message' => $e->getMessage()
            ];
        }

        return response()->json($return);
    }

    public function saveU($request)
    {   
        $validator = Validator::make($request, [
            // 'email' => 'required|email|unique:users,email',
            // 'telephone' => 'required|unique:users,telephone'
            'email' => 'required',
            'telephone' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 'ERROR',
                'message' => 'Ce client existe déjà.' 
            ]);
        }

        $code = \App\User::code();
        while(\App\User::where('code_acces', $code)->count() > 0){
            $code = \App\User::code();
        }

        try {
            $user = new \App\User;
            $user->name = $request['name'];
            $user->genre = $request['genre'];
            $user->ville_id = $request['ville_id'];
            $user->telephone = '+225'.$request['telephone'];
            $user->moy_contact = $request['moy_contact'];
            $user->marque_pref = $request['marque_pref'];
            $user->marque_alt = $request['marque_alt'];
            $user->datenaiss = $request['date'];
            $user->echantillonage_pqt = $request['echantillonage_pqt'];
            $user->pqt_acht = $request['pqt_acht'];
            $user->email = $request['email'];
            $user->instagram = $request['instagram'];
            $user->device_id = $request['device_id'];
            $user->role_id = 4;
            $user->code_acces = $code;
            $user->password = '';
            $user->save();

            $data=array(
                "token"=>$user->code_acces,
                "nameUser"=>$user->name,
            );
            $emailme = $user->email;

            Mail::send('mail/validmail', $data, function ($message) use($emailme){
                $message->to($emailme);
                $message->subject('Acces au site internet');
            });

            $return = [
                'code' => 'OK',
                'message' => 'Client Ajouté avec succès.'
            ];
            
            //Send SMS
            $url_site = URL::to('/');
            $msg = 'Vous êtes desormais un membre privilégié. Accedez au site via le lien suivant: '.$url_site;
            $this->sendSms($msg, [$user->telephone]);

        } catch (\Exception $e) {
            $return = [
                'code' => 'ERROR',
                'message' => 'Erreur lors de l\'ajout. Veuillez réessayer SVP!'
                // 'message' => $e->getMessage()
            ];
        }

        return response()->json($return);
    }

    public function saveUsers(Request $request)
    {
        $back = [];
        foreach ($request->users as $k => $v) {
            $back[] = $this->saveU($v);
        }

        return response()->json($back);
    }

    public function getUsers($device, $page = 1)
    {
        $currentPage = $page;
        \Illuminate\Pagination\Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        $users = \App\User::where([['role_id', 4], ['device_id', $device]])
                            ->orderBy('created_at', 'desc')
                            ->paginate(5);
        $u = [];
        foreach ($users as $user) {
            $median = $user->toArray();
            $median['ville'] = is_null($user->ville) ? [] : $user->ville->toArray();
            $u[] = $median;
        }

        return response()->json($u);
    }

    public function deleteUser($id)
    {
        try {
            $user = \App\User::find($id);
            $user->delete();

            return response()->json([
                'code' => 'OK',
                'message' => 'Suppression effectuée avec succès'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 'ERROR',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getgame($id){
        //$data = $request->except(['_token']);
        //$id = $data['id'];
        $check = User::where('id',$id)->first();
        Auth::loginUsingId($check->id);
        return redirect()->route('game');

    }

    public function villes()
    {
        $code = \App\User::code(15);
        echo $code;
    }
}
