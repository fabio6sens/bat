<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\User;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'ville_id','telephone','datenaiss',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rules()
    {
        return $this->belongsTo('App\Rules', 'role_id');
    }

    public function ville()
    {
        return $this->belongsTo('App\Ville', 'ville_id');
    }

    public function device()
    {
        return $this->belongsTo('App\Device', 'device_id');
    }

    public static function code($number = 10)
    {
        $characts = 'abcdefghijklmnopqrstuvwxyz'; 
        $characts .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';  
        $characts .= '1234567890'; 
        $code_aleatoire = ''; 

        for($i=0;$i < $number;$i++){ 
            $code_aleatoire .= $characts[ rand() % strlen($characts) ]; 
        } 

        return $code_aleatoire;
    }
}
