<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collecte extends Model
{
    public function roving()
    {
        return $this->belongsTo('App\Roving', 'roving_id');
    }
}
