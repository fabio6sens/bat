<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampagneUsers extends Model
{
    protected $table = 'campagne_users';

    protected $fillable = [
        'id_campagne', 'telephone', 'statut'
    ];
}
