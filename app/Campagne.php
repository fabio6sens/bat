<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campagne extends Model
{
    protected $table = 'campagne';

    protected $fillable = [
        'titre', 'message'
    ];
}
