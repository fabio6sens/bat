<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function userMessage()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function newMessage()
    {
        return $this->belongsTo('App\News', 'new_id');
    }
}
