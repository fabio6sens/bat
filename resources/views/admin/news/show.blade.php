@extends('admin.layouts.app')
@section('title', 'actualites')

@section('page_content_title', $item->title )

@push('styles')

@endpush

@section('content')

    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__body">
            <div class="m-section">
                <div class="col-md-12">
                    <img src="{{url('frontarticles/'.$item->imgactu)}}" alt="image actualite" style="width:100%">
                </div>
                <br>
                <div class="col-md-12">
                    {!! $item->des !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

@endpush

