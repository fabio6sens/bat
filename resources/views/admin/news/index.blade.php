@extends('admin.layouts.app')
@section('title', 'actualites')
@section('page_content_title', 'Actualités')

@push('styles')

@endpush

@section('content')
    <div class="row">
        @if(Session::has('error'))
            <div class="alert alert-danger">
                {{ Session::get('error')}}
            </div>
        @endif

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success')}}
            </div>
        @endif
    </div>

    <div class="m-portlet m-portlet--mobile">

        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <a href="{{route('addnews')}}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                            <i class="la la-plus"></i> Ajouter une actualité
                        </a>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <table class="m-datatable" id="html_table" width="100%">
                <thead>
                <tr>
                    <th title="Ref.No."> Ref.No. </th>
                    <th title="Titre"> Titre </th>
                    <th title="Date de publication"> Date de publication </th>
                    <th title="Status"> Status </th>
                    <th title="Action"> Action </th>
                </tr>
                </thead>
                @if(!empty($allactus))
                    <tbody>
                    @foreach($allactus as  $k =>$actu)
                        <tr>
                            <td>#0{{$k+1}} </td>
                            <td>{{$actu->title}} </td>
                            <td>{{ date("d/m/Y",strtotime($actu->created_at))}} </td>
                            <td>{!! $actu->status==0 ? '<span class="badge badge-danger">Inactif</span>' : '<span class="badge badge-success">Publie</span>' !!}</td>
                            <td>
                                <a href="{{route('newsshow',$actu->id)}}"  class="btn btn-outline-success"><i class="fa fa-eye text-success"></i> </a>
                                <a href="{{route('newsedit',$actu->id)}}" class="btn btn-outline-primary"><i class="fa fa-edit text-primary"></i> </a>
                                {{--<button class="btn btn-outline-danger" data-toggle="modal" data-target="#modal-{{$k+1}}"><i class="fa fa-trash text-danger"></i></button>--}}

                                {{--<div class="modal fade" id="modal-{{$k+1}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="width: 375px!important;margin-left: 37%;">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header bg-danger border-0">
                                                <h5 class="modal-title text-white">Confirmation</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>

                                            <div class="modal-body p-5">
                                                Voulez-vous réellement supprimer cette actualité.
                                            </div>

                                            <div class="modal-footer border-0">
                                                <button type="button" class="btn btn-link" data-dismiss="modal">Annuler</button>
                                                <form action="{{route('newsdestroy',$actu->id)}}" method="POST">
                                                    @csrf
                                                    --}}{{--<input name="_method" type="hidden" value="DELETE">--}}{{--
                                                    <button type="submit" class="btn btn-danger">Oui</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>--}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection

@push('scripts')
<!--begin::Page Resources -->
<script src="{{asset('bundles/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
<!--end::Page Resources -->
<script src="{{ asset('bundles/dropify.js')}}"></script>
<script>
    $('.dropify').dropify();
</script>
@endpush

