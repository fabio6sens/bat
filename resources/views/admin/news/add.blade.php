@extends('admin.layouts.app')
@section('title', 'actualites')
@section('page_content_title', 'Ajouter une Actualités')

@push('styles')
<link rel="stylesheet" href="{{ asset('bundles/dropify.css') }}">
@endpush

@section('content')
    <div class="row">
        @if(Session::has('error'))
            <div class="alert alert-danger">
                {{ Session::get('error')}}
            </div>
        @endif

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success')}}
            </div>
        @endif
    </div>
    <div class="m-portlet m-portlet--tab">
        <!--begin::Form-->
        <form action="{{route('newsstore')}}" class="m-form m-form--fit m-form--label-align-right" method="post" autocomplete="off" enctype="multipart/form-data">
            @csrf
            <div class="m-portlet__body">
                <div class="form-group m-form__group">
                    <label for="title"> Titre de l'article </label>
                    <input type="text" name="titre" class="form-control m-input" required id="title" aria-describedby="title" placeholder="Entrer le title">
                </div>
                <div class="form-group m-form__group">
                    <label for="des"> Description </label>
                    <textarea id="ckeditor_full" name="contenu" class="form-control" rows="10" style="width: 100%"></textarea>
                </div>
                <div class="form-group m-form__group">
                    <label for="exampleSelect1"> Photo de l'actualité </label>
                    <input type="file" id="file" name="file" class="dropify" required/>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <a href="{{route('new')}}" class="btn btn-secondary"> Cancel </a>
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i> Ajouter
                    </button>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
@endsection

@push('scripts')

<script src="{{ asset('bundles/dropify.js')}}"></script>
<script src="{{ asset('bundles/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>
    $('.dropify').dropify();

    var editor_config = {
        height: 120,
        path_absolute : "/",
        selector: "#ckeditor_full",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "formatselect | insertfile undo redo | styleselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };

    tinymce.init(editor_config);

</script>
@endpush

