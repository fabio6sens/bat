<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
	<i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
	<!-- BEGIN: Aside Menu -->
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
	    @if(Auth::user()->role_id !=2)
			<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
				<li class="m-menu__item {{ request()->is('users/*') ? ' m-menu__item--open' : '' }}" aria-haspopup="true" >
				<a  href="{{url('admin')}}" class="m-menu__link ">
					<i class="m-menu__link-icon flaticon-line-graph"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">
								Dashboard
							</span>
						</span>
					</span>
				</a>
			</li>

				<li class="m-menu__item m-menu__item--submenu {{ request()->is('users/*') ? 'm-menu__item--open' : '' }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
				<a href="#" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-users"></i>
					<span class="m-menu__link-text">
						Collecte
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">

						{{--PRODUIT 1--}}
						<li class="m-menu__item m-menu__item--submenu {{ request()->is('users/dunhill/*') ? 'm-menu__item--open' : '' }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
							<a href="#" class="m-menu__link m-menu__toggle">
								<i class=""> <span></span> </i> <span class="m-menu__link-text">DUNHILL</span>
								<i class="m-menu__ver-arrow la la-angle-right"></i>
							</a>
							<div class="m-menu__submenu" style="">
								<span class="m-menu__arrow"></span>
								<ul class="m-menu__subnav">
									<li class="m-menu__item {{ request()->is('users/dunhill/collecte') ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
										<a href="{{url('users/dunhill/collecte')}}" class="m-menu__link ">
											<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
											</i>
											<span class="m-menu__link-text">
												Tous
											</span>
										</a>
									</li>

									<li class="m-menu__item {{ request()->is('users/dunhill/rapport') ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
										<a href="{{url('users/dunhill/rapport')}}" class="m-menu__link ">
											<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
											</i>
											<span class="m-menu__link-text">
												Rapport
											</span>
										</a>
									</li>
								</ul>
							</div>
						</li>

						{{--PRODUIT 2--}}
						<li class="m-menu__item m-menu__item--submenu {{ request()->is('users/cravenarouge/*') ? ' m-menu__item--active' : '' }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
							<a href="#" class="m-menu__link m-menu__toggle">
								<i class=""> <span></span> </i> <span class="m-menu__link-text"> CRAVEN A ROUGE </span>
								<i class="m-menu__ver-arrow la la-angle-right"></i>
							</a>
							<div class="m-menu__submenu" style="">
								<span class="m-menu__arrow"></span>
								<ul class="m-menu__subnav">
									<li class="m-menu__item {{ request()->is('users/cravenarouge/collecte') ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
										<a href="{{url('users/cravenarouge/collecte')}}" class="m-menu__link ">
											<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
											</i>
											<span class="m-menu__link-text">
												Tous
											</span>
										</a>
									</li>

									<li class="m-menu__item {{ request()->is('users/cravenarouge/rapport') ? ' m-menu__item--active' : '' }}" aria-haspopup="true">
										<a href="{{url('users/cravenarouge/rapport')}}" class="m-menu__link ">
											<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
											</i>
											<span class="m-menu__link-text">
												Rapport
											</span>
										</a>
									</li>
								</ul>
							</div>
						</li>

						{{--OLD OPERATION--}}
						<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" data-menu-submenu-toggle="hover">
							<a href="#" class="m-menu__link m-menu__toggle">
								<i class=""> <span></span> </i> <span class="m-menu__link-text"> OLD  </span>
								<i class="m-menu__ver-arrow la la-angle-right"></i>
							</a>
							<div class="m-menu__submenu" style="">
								<span class="m-menu__arrow"></span>
								<ul class="m-menu__subnav">
									<li class="m-menu__item" aria-haspopup="true">
										<a href="{{url('users/collecte')}}" class="m-menu__link ">
											<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
											</i>
											<span class="m-menu__link-text">
												Tous
											</span>
										</a>
									</li>

									<li class="m-menu__item " aria-haspopup="true">
										<a href="{{url('users/collecte/rapport')}}" class="m-menu__link ">
											<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
											</i>
											<span class="m-menu__link-text">
									Rapport
								</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</li>

				<li class="m-menu__item m-menu__item--submenu {{ request()->is('sms/*') ? ' m-menu__item--active' : '' }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
				<a href="#" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-paper-plane"></i>
					<span class="m-menu__link-text">
						Campages SMS
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">

						<li class="m-menu__item " aria-haspopup="true">
							<a href="{{url('sms/index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Liste des campagnes
								</span>
							</a>
						</li>

						<li class="m-menu__item " aria-haspopup="true">
							<a href="{{url('sms/create')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Nouvelle campagne
								</span>
							</a>
						</li>
					</ul>
				</div>
			</li>

				<li class="m-menu__item  m-menu__item--submenu {{ request()->is('users/index')|| request()->is('users/roles') || request()->is('users/rovingteam') ? ' m-menu__item--open' : '' }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
					<a href="#" class="m-menu__link m-menu__toggle">
						<i class="m-menu__link-icon flaticon-layers"></i>
						<span class="m-menu__link-text">
							Administration
						</span>
						<i class="m-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="m-menu__submenu">
						<span class="m-menu__arrow"></span>
						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">
								<a href="{{url('users/index')}}" class="m-menu__link ">
									<i class="m-menu__link-bullet m-menu__link-bullet--dot">
										<span></span>
									</i>
									<span class="m-menu__link-text">
										Teams
									</span>
								</a>
							</li>

							<li class="m-menu__item " aria-haspopup="true">
								<a href="{{url('users/roles')}}" class="m-menu__link ">
									<i class="m-menu__link-bullet m-menu__link-bullet--dot">
										<span></span>
									</i>
									<span class="m-menu__link-text">
										Roles
									</span>
								</a>
							</li>

							<li class="m-menu__item " aria-haspopup="true">
								<a href="{{url('users/rovingteam')}}" class="m-menu__link ">
									<i class="m-menu__link-bullet m-menu__link-bullet--dot">
										<span></span>
									</i>
									<span class="m-menu__link-text">
										Roving Team
									</span>
								</a>
							</li>

							{{--<li class="m-menu__item " aria-haspopup="true">
								<a href="{{url('users/import')}}" class="m-menu__link ">
									<i class="m-menu__link-bullet m-menu__link-bullet--dot">
										<span></span>
									</i>
									<span class="m-menu__link-text">
										Importer
									</span>
								</a>
							</li>--}}
						</ul>
					</div>
				</li>
			</ul>
		@endif
		
		@if(Auth::user()->role_id ==2)
			<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
				<li class="m-menu__item {{ request()->is('admin') ? ' m-menu__item--active' : '' }}" aria-haspopup="true" >
					<a  href="{{url('admin')}}" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-line-graph"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">
									Dashboard
								</span>
							</span>
						</span>
					</a>
				</li>
			</ul>
		@endif

	</div>
	<!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->