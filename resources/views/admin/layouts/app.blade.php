<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
	<meta charset="utf-8" />
	<title>
		Dashboard BAT - @yield('title')
	</title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!--begin::Web font -->
	<script src="{{asset('bundles/webfontloader.js')}}"></script>
	<script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
	</script>
	<!--end::Web font -->
	@include('admin.include.styles')
	<style type="text/css">
		.no-padding{
			padding: 0 !important;
		}
		.alert{
			width: 98.6%;
			font-size: 16px;
		}
	</style>
</head>
<!-- end::Head -->

<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
@include('admin.layouts.header')
<!-- begin::Body -->
	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="margin-top: 48px;padding-top: 0!important;">
		@include('admin.layouts.left_menu')
		<div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color: white;">
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title ">
							@yield('page_content_title')
						</h3>
					</div>
				</div>
			</div>
			<div class="m-content">
				@yield('content')
			</div>
		</div>
	</div>
	<!-- end:: Body -->

    <!--begin::Modal-->
	<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">
						Confirmation de suppression
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">
							&times;
						</span>
					</button>
				</div>
				<div class="modal-body">
					<p>
						Voulez-vous supprimer?
					</p>
				</div>
				<div class="modal-footer">
					<form method="POST" id="form-delete" action="{{ url('#') }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <button class="btn btn-secondary" data-dismiss="modal" aria-hidden="true">Fermer</button>
                        <button type="submit" value="delete" class="btn btn-danger" name="delete" value="">Supprimer</button>
                    </form>
				</div>
			</div>
		</div>
	</div>
	<!--end::Modal-->

	@include('admin.layouts.footer')
</div>
<!-- end:: Page -->
@include('admin.include.scripts')
</body>
<!-- end::Body -->
</html>
