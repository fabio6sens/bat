@extends('admin.layouts.app')
@section('title', 'Créer une campagne sms')
@section('page_content_title', 'Créer une campagne sms')

@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('bundles/input_tag/jquery.tagsinput.css')}}" />
<style type="text/css">
	#tags_tagsinput{
		border: 1px solid #ebedf2 !important; 
		width: 100% !important;
    	height: 110px !important;
	}
</style>
@endpush

@section('content')
<div class="row">
	@if(Session::has('loose'))
		<div class="alert alert-warning">
		  {{ Session::get('loose')}}
		</div>
	@endif

	@if(Session::has('reject'))
		<div class="alert alert-danger">
		  {{ Session::get('reject')}}
		</div>
	@endif
</div>
<div class="row">
	<div class="col-md-12 no-padding">
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Formulaire de création de campagne sms
					</h3>
				</div>
			</div>
		</div>
		<!--begin::Form-->
		<form class="m-form m-form--fit m-form--label-align-left" method="post" action="{{url('sms/store')}}">
			@csrf
			<div class="m-portlet__body">
				<div class="row"> 
					<div class="col-md-7">
						<div class="form-group m-form__group col-md-12">
							<label for="exampleInputEmail1">
								Nom de la campagne
							</label>
							<input type="text" class="form-control m-input" name="titre" required>
						</div>

						<div class="form-group m-form__group col-md-12">
							<label for="exampleTextarea">
								Message
							</label>
							<textarea class="form-control m-input m-input--air" id="exampleTextarea" rows="12" name="msg" required></textarea>
						</div>
					</div>

					<div class="col-md-5">
						<div class="form-group m-form__group">
							<label for="exampleInputEmail1">
								{{--Destinataires (lieu d'engagement)--}}
								Marque reguilière
							</label>
							<select class="form-control m-select2" id="m_select2_1" name="param[]" multiple="multiple">
								<option value="0"> Tous </option>
								{{--@foreach($villes as $ville)
								<option value="{{$ville->id}}">
									{{$ville->libelle}}
								</option>
								@endforeach--}}
								<option value="Craven A Rouge">Craven A Rouge</option>
								<option value="Craven A Gold">Craven A Gold</option>
								<option value="Craven A Click">Craven A Click</option>
								<option value="Dunhill">Dunhill</option>
								<option value="Fine Light">Fine Light</option>
								<option value="Fine Duo">Fine Duo</option>
								<option value="Fine Rouge">Fine Rouge</option>
								<option value="Malboro rouge">Malboro rouge</option>
								<option value="Malboro light gold">Malboro Light Gold</option>
								<option value="Malboro bleu">Malboro Bleu</option>
								<option value="Oris pomme">Oris Pomme</option>
								<option value="Oris fraise">Oris Fraise</option>
								<option value="Oris">Oris</option>
								<option value="Autres">Autres</option>
							</select>
						</div>

						<div class="form-group m-form__group">
							<label for="prefe">Préférence</label>
							<select class="form-control m-select2" id="m_select2_1" name="pefer[]" multiple="multiple">
								<option value="0">Tous</option>
								<option value="Craven A Click Blue">Craven A Click Bleu</option>
								<option value="Craven A Click Biolet">Craven A Click Violet</option>
								<option value="NSP">NSP</option>
							</select>
						</div>

						<div class="form-group m-form__group col-md-12">
							<label for="exampleInputEmail1">
								Saisir manuellement
							</label>
							<input class="form-control m-input" name="tags" id="tags" />
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions">
					<div class="row">
						<div class="col-lg-9">
							<button type="submit" class="btn btn-brand">
								Envoyer
							</button>
							<button type="reset" class="btn btn-secondary">
								Annuler
							</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form-->
	</div>
	<!--end::Portlet-->
	</div>
</div>
@endsection

@push('scripts')
<script src="{{asset('bundles/input_tag/jquery.tagsinput.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#tags').tagsInput();
	});
</script>
<script src="{{asset('bundles/demo/default/custom/components/forms/widgets/select2.js')}}" type="text/javascript"></script>
@endpush

