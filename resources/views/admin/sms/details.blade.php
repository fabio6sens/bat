@extends('admin.layouts.app')
@section('title', 'sms')
@section('page_content_title', 'Détails campagne')

@section('content')
<div class="m-portlet ">
	<div class="m-portlet__body  m-portlet__body--no-padding">
		<div class="row m-row--no-padding m-row--col-separator-xl">
			<div class="col-md-12 col-lg-6 col-xl-6">
				<!--begin::Total Profit-->
				<div class="m-widget24">
					<div class="m-widget24__item">
						<h4 class="m-widget24__title">
							Total des sms envoyés
						</h4>
						<br>
						<span class="m-widget24__stats m--font-info">
							{{$countSend}}
						</span>
						<div class="m--space-10"></div>
						<div class="progress m-progress--sm">
							<?php 
								$percent = ($countSend * 100) / $countTotal;
							?>
							<div class="progress-bar m--bg-info" role="progressbar" style="width: <?php echo $percent; ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
						<span class="m-widget24__change">
							Pourcentage
						</span>
						<span class="m-widget24__number">
							<?php 
								$percent = ($countSend * 100) / $countTotal;
								echo $percent;
							?>
						</span>
					</div>
				</div>
				<!--end::Total Profit-->
			</div>

			<div class="col-md-12 col-lg-6 col-xl-6">
				<!--begin::Total Profit-->
				<div class="m-widget24">
					<div class="m-widget24__item">
						<h4 class="m-widget24__title">
							Total des sms rejetés
						</h4>
						<br>
						<span class="m-widget24__stats m--font-danger">
							{{$countNotSend}}
						</span>
						<div class="m--space-10"></div>
						<div class="progress m-progress--sm">
							<?php 
								$percent = ($countNotSend * 100) / $countTotal;
							?>
							<div class="progress-bar m--bg-danger" role="progressbar" style="width: <?php echo $percent; ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
						<span class="m-widget24__change">
							Pourcentage
						</span>
						<span class="m-widget24__number">
							<?php 
								$percent = ($countNotSend * 100) / $countTotal;
								echo $percent;
							?>
						</span>
					</div>
				</div>
				<!--end::Total Profit-->
			</div>

		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 no-padding">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Campagne: <strong>{{$campagne->titre}}</strong>
						</h3>
					</div>
				</div>
			</div>

			<div class="m-portlet__body">
				<div class="m-datatable m-datatable--default m-datatable--brand m-datatable--loaded"><table class="m-datatable__table" id="m-datatable--1516780828608" width="100%" style="display: block; height: auto; overflow-x: auto;">
					<table class="m-datatable__table" id="m-datatable--1516780828608" width="100%" style="display: block; height: auto; overflow-x: auto;">
						<thead class="m-datatable__head">
							<tr class="m-datatable__row" style="height: 53px;">
								<th class="m-datatable__cell m-datatable__cell--sort" data-field="Order ID">
									<span style="width: 110px;">N°</span>
								</th>
								<th title="Field #1" class="m-datatable__cell m-datatable__cell--sort" data-field="Order ID">
									<span style="width: 110px;">Nom</span>
								</th>
								<th title="Field #2" class="m-datatable__cell m-datatable__cell--sort" data-field="Order ID">
									<span style="width: 110px;">Téléphone</span>
								</th>
								<th title="Field #4" class="m-datatable__cell m-datatable__cell--sort" data-field="Order ID">
									<span style="width: 110px;">Lieu d'engagement</span>
								</th>
								<th title="Field #5" class="m-datatable__cell m-datatable__cell--sort" data-field="Order ID">
									<span style="width: 110px;">Status</span>
								</th>
							</tr>
						</thead>
						<tbody class="m-datatable__body" style="">
							@foreach($users_campagne as $k => $v)
							<?php $ville = App\Http\Controllers\Admin\SmsController::ville($v->ville_id); ?>
							<tr data-row="0" class="m-datatable__row m-datatable__row--even" style="height: 64px;">
								<td data-field="N°" class="m-datatable__cell">
									<span style="width: 110px;">{{$k+1}}</span>
								</td>
								<td data-field="Name" class="m-datatable__cell">
									<span style="width: 110px;">{{$v->name}}</span>
								</td>
								<td data-field="Name" class="m-datatable__cell">
									<span style="width: 110px;">{{$v->telephone}}</span>
								</td>
								<td data-field="Name" class="m-datatable__cell">
									<span style="width: 110px;">{{$ville}}</span>
								</td>
								<td class="m-datatable__cell">
									@switch($v->statut)
									    @case('NOT_ENOUGH_CREDITS')
									        <span style="color: #ff0000;">Message rejeté</span>
									        @break

									    @case('DELIVERED_TO_OPERATOR')
									        <span style="color: #34bfa3;">Message envoyé</span>
									        @break

									    @default
									        <span></span>
									@endswitch
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
    <script src="{{asset('bundles/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
@endpush

