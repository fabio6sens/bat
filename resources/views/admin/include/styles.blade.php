<!--begin::Base Styles -->  
<!--begin::Page Vendors -->
<link href="{{asset('bundles/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
<!--end::Page Vendors -->
<link href="{{asset('bundles/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('bundles/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
<!--end::Base Styles -->
<link rel="shortcut icon" href="{{asset('img/british.png')}}" />
@stack('styles')