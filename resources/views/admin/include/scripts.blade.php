<!--begin::Base Scripts -->
<script src="{{asset('bundles/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('bundles/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
<!--end::Base Scripts -->   
<!--begin::Page Vendors -->
<script src="{{asset('bundles/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->  
<!--begin::Page Snippets -->
<script src="{{asset('bundles/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
<script>
    $(document).ready(function(e){
        $(document).on("click", ".delete-modal", function(e) {
            var delete_id = $(this).attr('data-id');
            var route = $(this).attr('data-route');
            //console.log(route)
            $('button[name="delete"]').val(delete_id);
            $("#form-delete").each(function(){
                var value = $(this).attr('action');
                $(this).attr('action', value.replace('#', route+'/'+delete_id));
            });
        });
    });
</script>
@stack('scripts')