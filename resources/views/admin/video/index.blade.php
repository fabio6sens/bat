@extends('admin.layouts.app')
@section('title', 'video')
@section('page_content_title', 'Video')

@push('styles')

@endpush

@section('content')

    <div class="row">
        @if(Session::has('error'))
            <div class="alert alert-danger">
                {{ Session::get('error')}}
            </div>
        @endif

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success')}}
            </div>
        @endif
    </div>
    <!--begin::Modal-->
    <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> Ajouter une video  </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true"> &times;</span></button>
                </div>
                <form action="{{route('addvideo')}}" method="post" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group m-form__group">
                            <label for="libelle"> Titre </label>
                            <input type="text" name="libelle" class="form-control m-input m-input--square" id="libelle" aria-describedby="libelleHelp">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="url"> Lien de la video </label>
                            <input type="text" name="url" class="form-control m-input m-input--square" id="url" aria-describedby="urlHelp">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> Annuler </button>
                        <button type="submit" name="addimg" class="btn btn-primary"> <i class="fa fa-save"></i> Ajouter </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <button type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" data-toggle="modal" data-target="#m_modal_1">
                            <i class="la la-plus"></i> Ajouter une video
                        </button>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <table class="m-datatable" id="html_table" width="100%">
                <thead>
                <tr>
                    <th title="Ref.No."> Ref.No. </th>
                    <th title="Titre"> Titre </th>
                    <th title="Lien de la video"> Lien </th>
                    <th title="Action"> Action </th>
                </tr>
                </thead>
                @if(!empty($allvideo))
                <tbody>
                @foreach($allvideo as  $k =>$video)
                    <tr>
                        <td>#0{{$k+1}} </td>
                        <td>{{$video->title}} </td>
                        <td><a href="https://www.youtube.com/embed/{{$video->url }}" target="_blank" class="btn btn-primary">Voir</a></td> </td>
                        <td><a href="{{route('delvideo',$video->id)}}" class="btn btn-outline-danger"><i class="fa fa-trash text-danger"></i> </a></td>
                    </tr>
                @endforeach
                </tbody>
                @endif
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection

@push('scripts')
    <!--begin::Page Resources -->
    <script src="{{asset('bundles/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
    <!--end::Page Resources -->
    <script src="{{ asset('bundles/dropify.js')}}"></script>
    <script>
        $('.dropify').dropify();
    </script>
@endpush

