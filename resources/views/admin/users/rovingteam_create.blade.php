@extends('admin.layouts.app')
@section('title', 'Ajouter un roving team')
@section('page_content_title', 'Ajouter un roving team')

@section('content')

    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-md-12 no-padding">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">

                            </h3>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__body">
                    <form class="m-form m-form--fit m-form--label-align-right" id="m_form_1" method="POST" action="{{ url('users/rovingteam/store') }}" autocomplete="off">
                        @csrf
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12">
                                    Nom&Prenom
                                </label>
                                <div class="col-lg-4 col-md-9 col-sm-12">
                                    <input type="text" class="form-control m-input" name="name" placeholder="Nom">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12">
                                    Contact
                                </label>
                                <div class="col-lg-4 col-md-9 col-sm-12">
                                    <input type="number" class="form-control m-input" name="telephone" placeholder="Contact">
                                    <p class="help-block">Example: 01020304</p>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Sexe</label>
                                <div class="col-lg-4 col-md-9 col-sm-12">
                                    <select class="form-control m-input" name="sexe">
                                        <option value="">Selectionnez un sexe</option>
                                        <option value="Homme">Homme</option>
                                        <option value="Femme">Femme</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12"> Adresse</label>
                                <div class="col-lg-4 col-md-9 col-sm-12">
                                    <input type="text" class="form-control m-input" name="adresse" placeholder="Adresse">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Mot de passe</label>
                                <div class="col-lg-4 col-md-9 col-sm-12">
                                    <input type="password" class="form-control m-input" name="password" placeholder="Mot de passe">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Confirmez le mot de passe</label>
                                <div class="col-lg-4 col-md-9 col-sm-12">
                                    <input id="password-confirm" type="password" class="form-control m-input" name="password_confirmation" placeholder="Confirmez le mot de passe">
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-9 ml-lg-auto">
                                        <button type="submit" class="btn btn-success">
                                            ENREGISTRER
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            ANNULER
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection

