@extends('admin.layouts.app')
@section('title', 'Modifier un rôles')
@section('page_content_title', 'Modifier un rôle')

@section('content')

<div class="row">
	@if(Session::has('error'))
		<div class="alert alert-danger">
		  {{ Session::get('error')}}
		</div>
	@endif

	@if(Session::has('success'))
		<div class="alert alert-success">
		  {{ Session::get('success')}}
		</div>
	@endif

	@if(Session::has('delete'))
		<div class="alert alert-success">
		  {{ Session::get('delete')}}
		</div>
	@endif
</div>

<div class="row">
	<div class="col-md-12">
		<div class="m-portlet m-portlet--tab">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
							<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text">
							 
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('updateroles')}}">
				@csrf
				<div class="m-portlet__body">
					<div class="form-group m-form__group">
						<label for="exampleInputEmail1">
							Nom du rôle
						</label>
						<input type="text" name="role" class="form-control m-input" value="{{$role->role}}">
						<input type="hidden" name="slug" value="{{$role->id}}">
					</div>
				</div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions">
						<button type="Submit" class="btn btn-success">
							Enregistrer
						</button>
						<button type="reset" class="btn btn-secondary">
							Annuler
						</button>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
@endsection

@push('scripts')
    <script src="{{asset('bundles/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
@endpush

