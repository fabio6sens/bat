@extends('admin.layouts.app')
@section('title', 'Messages')
@section('page_content_title', 'Messages')

@push('styles')

@endpush

@section('content')
    <div class="row">
        @if(Session::has('error'))
            <div class="alert alert-danger">
                {{ Session::get('error')}}
            </div>
        @endif

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success')}}
            </div>
        @endif
    </div>

    <div class="m-portlet m-portlet--mobile">

        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <table class="m-datatable" id="html_table" width="100%">
                <thead>
                <tr>
                    <th title="Ref.No."> Ref.No. </th>
                    <th title="Titre article"> Titre actualité </th>
                    <th title="Utilisateur"> Nom&Prénom </th>
                    <th title="Date de publication"> Date de message </th>
                    <th title="Status"> Status </th>
                    <th title="Action"> Action </th>
                </tr>
                </thead>
                @if(!empty($allmsg))
                    <tbody>
                    @foreach($allmsg as  $k =>$actu)
                        <tr>
                            <td>#0{{$k+1}} </td>
                            <td>{{$actu->newMessage->title}} </td>
                            <td>{{$actu->userMessage->name}} </td>
                            <td>{{ date("d/m/Y",strtotime($actu->created_at))}} </td>
                            <td>{!! $actu->etat==0 ? '<span class="badge badge-danger">Pas vu</span>' : '<span class="badge badge-success">Vu</span>' !!}</td>
                            <td>
                                <a href="{{route('showmessage',$actu->id)}}" class="btn btn-outline-success"><i class="fa fa-eye text-success"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection

@push('scripts')
<!--begin::Page Resources -->
<script src="{{asset('bundles/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
<!--end::Page Resources -->
<script src="{{ asset('bundles/dropify.js')}}"></script>
<script>
    $('.dropify').dropify();
</script>
@endpush

