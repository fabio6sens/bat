@extends('admin.layouts.app')
@section('title', 'collecte')
@section('page_content_title', 'Collecte de données')

@section('content')
<div class="row">
	@if(Session::has('delete'))
		<div class="alert alert-success">
		  {{ Session::get('delete')}}
		</div>
	@endif
</div>
<!--begin::Modal-->
<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<p>
					Veuillez télécharger la base de données en deux parties
				</p>
				<br>
				<div class="row">
					<div class="col-md-4" style="margin-left: 50px;">
						<a href="{{url('users/exportphase/1')}}" class="btn btn-success"> Partie 1 </a>
					</div>
					<div class="col-md-4">
						<a href="{{url('users/exportphase/2')}}" class="btn btn-danger"> Partie 2 </a>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal"> Annuler </button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<!--<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--	<div class="modal-dialog" role="document">-->
<!--		<div class="modal-content">-->
<!--			<div class="modal-header">-->
<!--				<h5 class="modal-title" id="exampleModalLabel"> Ajouter un fichier  </h5>-->
<!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true"> &times;</span></button>-->
<!--			</div>-->
<!--			<form action="{{route('addExcelContact')}}" method="post" autocomplete="off" enctype="multipart/form-data">-->
<!--				@csrf-->
<!--				<div class="modal-body">-->
<!--					<div class="col-md-12">-->
<!--						<div class="form-group">-->
<!--							<label for="file"><strong>Fichier </strong></label>-->
<!--							<input type="file" id="file" name="file" required/>-->
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
<!--				<div class="modal-footer">-->
<!--					<button type="button" class="btn btn-secondary" data-dismiss="modal"> Annuler </button>-->
<!--					<button type="submit" name="addimg" class="btn btn-primary"> <i class="fa fa-save"></i> Ajouter </button>-->
<!--				</div>-->
<!--			</form>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->
<!--end::Modal-->


<div class="row">
	<div class="col-md-12 no-padding">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="col-md-6 m-portlet__head-text">
							@if($slug != 'all')
								Liste des collectés DUNHILL par {{$slug}}
							@else
								Liste des collectés DUNHILL
								<br>
								Total: {{$countUsers}}
							@endif
						</h3>
					</div>
				</div>
			</div>

			<div class="m-portlet__body">
				<div class="m-form m-form--label-align-left m--margin-top-20 m--margin-bottom-30">
					<div class="row align-items-center" style="margin: 0">
						<div class="col-xl-12" style="border: solid 1px #eee;  margin-bottom: 30px;">
							<form method="post" action="{{url('users/dunhill/collecte/export')}}" class="m-form m-form--fit m-form--label-align-left">
								@csrf
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-12 col-sm-12" style="padding-left: 0; font-size: 17px;">
											Trie par critères
										</label>
										<div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0">
											<div class="row">
												<div class="col-lg-3" style="margin-bottom: 10px; margin-left: 0">
													<label class="col-form-label">
														Marque regulière
													</label>
													<select onchange="myFunction()" class="form-control m-select2 regular" id="m_select2_1" name="regular">
														<option value="0"> Tous</option>
														<option value="Craven A Rouge">Craven A Rouge</option>
														<option value="Craven A Gold">Craven A Gold</option>
														<option value="Craven A Click Blue">Craven A Click Blue</option>
														<option value="Craven A Click Violet">Craven A Click Violet</option>

														<option value="Dunhill International">Dunhill International</option>
														<option value="Dunhill King Size Evoque Bleu">Dunhill King Size Evoque Bleu</option>
														<option value="Dunhill King Size Evoque Kasha">Dunhill King Size Evoque Kasha</option>

														<option value="Marlboro Rouge">Marlboro Rouge</option>
														<option value="Marlboro Bleu">Marlboro Bleu</option>
														<option value="Marlboro Gold">Marlboro Gold</option>

														<option value="L&M">L&M</option>

														<option value="Davidoff Rouge">Davidoff Rouge</option>
														<option value="Davidoff Gold">Davidoff Gold</option>

														<option value="Fine Rouge">Fine Rouge</option>
														<option value="Fine Light">Fine Light</option>
														<option value="Fine Duo">Fine Duo</option>

														<option value="Excellence Rouge">Excellence Rouge</option>
														<option value="Excellence Light">Excellence Light</option>

														<option value="News Rouge">News Rouge</option>
														<option value="News Light">News Light</option>

														<option value="Winston Rouge">Winston Rouge</option>
														<option value="News Light">Winston Light</option>

														<option value="Oris">Oris</option>

														<option value="Autres">Autres</option>
													</select>
												</div>
												<div class="col-lg-3" style="margin-bottom: 10px; margin-left: 0">
													<label class="col-form-label">
														Variante Acheté
													</label>
													<select onchange="myFunction2()" class="form-control m-select2 paquet" id="m_select2_1" name="paquet">
														<option value="0"> Tous</option>
														<option value="Dunhill International">Dunhill International</option>
														<option value="Dunhill King Size Evoque Bleu">Dunhill King Size Evoque Bleu</option>
														<option value="Dunhill King Size Evoque Kasha">Dunhill King Size Evoque Kasha</option>
														<option value="Aucun">Aucun</option>
														<option value="Autre">Autre</option>
													</select>
												</div>
												<div class="col-lg-3" style="margin-bottom: 10px; margin-left: 0">
													<label class="col-form-label">
														Moyen de contact
													</label>
													<select onchange="myFunction3()" class="form-control m-select2 preference" id="m_select2_1" name="preference">
														<option value="0"> Tous</option>
														<option value="SMS">SMS</option>
														<option value="Email">Email</option>
														<option value="WhatsApp">WhatsApp</option>
														<option value="Messenger">Messenger</option>
													</select>
												</div>
												<div class="col-lg-3" style="position: relative; top: 35px;">
													<button type="submit" class="btn btn-success m-btn m-btn--pill" name="btn" value="trie">
														Trier
													</button>
													<button type="submit" class="btn btn-success m-btn m-btn--pill" name="btn" id="export" value="export" style="display: none">
														Exporter
													</button>
													<button type="button" class="btn btn-success m-btn m-btn--pill" data-toggle="modal" data-target="#m_modal_1" id="export2">
														Exporter
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>

				<table class="table">
					<thead class="m-datatable__head">
						<tr class="m-datatable__row" style="height: 53px;">
							<th data-field="N°">
								<span style="width: 110px;">N°</span>
							</th>
							<th title="Field #1">
								<span style="width: 110px;">Nom&Prenom</span>
							</th>
							<th title="Field #2">
								<span style="width: 110px;">Sexe</span>
							</th>
							<th title="Field #3">
								<span style="width: 110px;">Téléphone</span>
							</th>
							<th title="Field #4">
								<span style="width: 110px;">Marque regulière</span>
							</th>
							<th title="Field #5">
								<span style="width: 110px;">Roving Team</span>
							</th>
							{{--<th title="Field #5">
								<span style="width: 110px;">Zone</span>
							</th>--}}
							<th title="Field #6">
								<span style="width: 110px;">Actions</span>
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $k => $user)
						<tr data-row="0" class="m-datatable__row m-datatable__row--even" style="height: 64px;">
							<td class="m-datatable__cell">
								<span style="width: 110px;">{{$k+1}}</span>
							</td>
							<td class="m-datatable__cell">
								<span style="width: 110px;">{{$user->nom}}</span>
							</td>
							<td class="m-datatable__cell">
								<span style="width: 110px;">{{$user->genre}}</span>
							</td>
							<td class="m-datatable__cell">
								<span style="width: 110px;">{{$user->telephone}}</span>
							</td>
							<td class="m-datatable__cell">
								<span style="width: 110px;">{{$user->marque_reg}}</span>
							</td>
							<td class="m-datatable__cell">
								<span style="width: 110px;">{{$user->roving->nom}}</span>
							</td>
							<td class="m-datatable__cell">
								<a href="{{route('duncrashow',$user->id)}}"  class="btn btn-outline-success"><i class="fa fa-eye text-success"></i> </a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>

				@if(!isset($slug))
					@if($countUsers != 0)
						{!! $users->render() !!}
					@endif
				@endif
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
    <script src="{{asset('bundles/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
    <script src="{{asset('bundles/demo/default/custom/components/forms/widgets/select2.js')}}" type="text/javascript"></script>
<script>

	function myFunction() {
		var x = $(".regular").val();
        console.log(x);
		if(x=='0'){
            $("#export2").show();
            $("#export").hide();
		}else{
            $("#export2").hide();
            $("#export").show();
		}
	}
	function myFunction2() {
		var x = $(".paquet").val();
		console.log(x);
		if(x=='0'){
			$("#export2").show();
			$("#export").hide();
		}else{
			$("#export2").hide();
			$("#export").show();
		}
	}
	function myFunction3() {
		var x = $(".preference").val();
		console.log(x);
		if(x=='0'){
			$("#export2").show();
			$("#export").hide();
		}else{
			$("#export2").hide();
			$("#export").show();
		}
	}

</script>
@endpush

