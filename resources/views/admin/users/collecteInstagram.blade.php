@extends('admin.layouts.app')
@section('title', 'collecte')
@section('page_content_title', 'Collecte de données')

@section('content')
<div class="row">
	@if(Session::has('delete'))
		<div class="alert alert-success">
		  {{ Session::get('delete')}}
		</div>
	@endif
</div>
<div class="row">
	<div class="col-md-12 no-padding">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Liste des collectés <strong>({{$slug}})</strong>
						</h3>
					</div>
				</div>
			</div>

			<div class="m-portlet__body">
				<div class="m-form m-form--label-align-left m--margin-top-20 m--margin-bottom-30">
					<div class="row align-items-center" style="margin: 0">
						<div class="col-xl-4 order-2 order-xl-1" style="padding-right: 0;">
							<div class="form-group m-form__group row align-items-center">
								<div class="col-md-12" style="padding-left: 0;">
									<div class="m-input-icon m-input-icon--left">
										<input type="text" class="form-control m-input m-input--solid" placeholder="Recherche individuelle..." id="generalSearch">
										<span class="m-input-icon__icon m-input-icon__icon--left">
											<span>
												<i class="la la-search"></i>
											</span>
										</span>
									</div>
								</div>
							</div>
						</div>


						<div class="col-xl-8 order-1 order-xl-2 m--align-right">
							<a href="{{url('users/export/'.$slug)}}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
								<span>
									<i class="la la-plus"></i>
									<span>
										Exporter
									</span>
								</span>
							</a>
						</div>
						
					</div>
				</div>

					<table class="m-datatable" id="html_table" width="100%">
						<thead class="m-datatable__head">
							<tr class="m-datatable__row" style="height: 53px;">
								<th data-field="Order ID">
									<span style="width: 110px;">N°</span>
								</th>
								<th title="Field #1">
									<span style="width: 110px;">Nom</span>
								</th>
								<th title="Field #2">
									<span style="width: 110px;">Instagram</span>
								</th>
								
								<th title="Field #6">
									<span style="width: 110px;">Actions</span>
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $k => $user)
							<tr data-row="0" class="m-datatable__row m-datatable__row--even" style="height: 64px;">
								<td class="m-datatable__cell">
									<span style="width: 110px;">{{$k+1}}</span>
								</td>
								<td class="m-datatable__cell">
									<span style="width: 110px;">{{$user->name}}</span>
								</td>
								<td class="m-datatable__cell">
									<span style="width: 110px;">
										<a href="{{url('https://instagram.com/'.$user->instagram)}}">
											https://instagram.com/{{$user->instagram}}
										</a>
									</span>
								</td>
								<td class="m-datatable__cell">
									<div class="dropdown">
										<a href="{{route('showcollecte',$user->id)}}"  class="btn btn-outline-success"><i class="fa fa-eye text-success"></i> </a>
										</div>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
    <script src="{{asset('bundles/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
    <script src="{{asset('bundles/demo/default/custom/components/forms/widgets/select2.js')}}" type="text/javascript"></script>
@endpush

