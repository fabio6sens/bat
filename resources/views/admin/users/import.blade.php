@extends('admin.layouts.app')
@section('title', 'index')
@section('page_content_title', 'Importer')

@section('content')

    <div class="row">
        @if(Session::has('error'))
            <div class="alert alert-danger">
                {{ Session::get('error')}}
            </div>
        @endif

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success')}}
            </div>
        @endif
    </div>
    
    <!-- BEGIN: Subheader -->
    <div class="m-content">

        <div class="m-portlet">
            <div class="m-portlet__body">
                <div class="col-md-6">
                    <button type="button" class="btn btn-accent btn-lg" data-toggle="modal" data-target="#m_modal_1" style="margin-left: 80%;">
                        Importer
                    </button>
                </div>
            </div>
        </div>

        <!--begin::Modal-->
        <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> Ajouter un fichier  </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true"> &times;</span></button>
                    </div>
                    <form action="{{route('addExcelContact')}}" method="post" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="file"><strong>Fichier </strong></label>
                                    <input type="file" id="file" name="file" required/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"> Annuler </button>
                            <button type="submit" name="addimg" class="btn btn-primary"> <i class="fa fa-save"></i> Ajouter </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--end::Modal-->

    </div>
    <!-- END: Subheader -->
@endsection