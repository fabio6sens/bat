@extends('admin.layouts.app')
@section('title', 'roles')
@section('page_content_title', 'Roving Team')

@section('content')

<div class="row">
	@if(Session::has('error'))
		<div class="alert alert-danger">
		  {{ Session::get('error')}}
		</div>
	@endif

	@if(Session::has('success'))
		<div class="alert alert-success">
		  {{ Session::get('success')}}
		</div>
	@endif

	@if(Session::has('delete'))
		<div class="alert alert-success">
		  {{ Session::get('delete')}}
		</div>
	@endif
</div>

<div class="row">
	<div class="col-md-12 no-padding">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Liste des Roving Team
						</h3>
					</div>
				</div>
			</div>

			<div class="m-portlet__body">
				<!--begin: Search Form -->
				<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
					<div class="row align-items-center">
						<div class="col-xl-8 order-2 order-xl-1">
							<div class="form-group m-form__group row align-items-center">
								<div class="col-md-4">
									<div class="m-input-icon m-input-icon--left">
										<input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
										<span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 order-1 order-xl-2 m--align-right">
							<a href="#" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-plus"></i>
													<span>
														Créer un roving team
													</span>
												</span>
							</a>
							<div class="m-separator m-separator--dashed d-xl-none"></div>
						</div>
					</div>
				</div>
				<!--end: Search Form -->
				<!--begin: Datatable -->
				<table class="roving m-datatable" id="html_table" width="100%">
					<thead>
						<tr>
							<th title="Field #1"><span style="width: 2%!important;">N°</span></th>
							<th title="Field #2"><span style="width: 110px!important;">Identifiant</span></th>
							<th title="Field #3"><span style="width: 110px!important;">Mot de passe</span></th>
							<th title="Field #4"><span style="width: 110px!important;">Nom&Prénom</span></th>
							<th title="Field #5"><span style="width: 110px!important;">Contact</span></th>
							<th title="Field #6"><span style="width: 110px!important;">Sexe</span></th>
							<th title="Field #7"><span style="width: 110px!important;">Adresse</span></th>
							<th title="Field #8"><span style="width: 110px!important;">Action</span></th>
						</tr>
					</thead>
					<tbody>
						@foreach($rovings as $k => $roving)
						<tr>
							<td><span style="width: 2%!important;">{{$k+1}}</span></td>
							<td><span style="width: 110px!important;">{{$roving->identifiant}}</span></td>
							<td><span style="width: 110px!important;">{{$roving->password}}</span></td>
							<td><span style="width: 110px!important;">{{$roving->nom}}</span></td>
							<td><span style="width: 110px!important;">{{$roving->contact}}</span></td>
							<td><span style="width: 110px!important;">{{$roving->sexe}}</span></td>
							<td><span style="width: 110px!important;">{{$roving->adresse}}</span></td>
							<td>
								<a class="" href="{{route('roving_edit',$roving->identifiant)}}" style="margin-right: 10px">
									<i class="fa fa-edit text-success"></i>
								</a>
								{{--<a href="{{route('roving_edit',$roving->identifiant)}}"  class="btn btn-xs btn-outline-success"><i class="fa fa-edit text-success"></i> </a>
								<a href="{{route('roving_delete',$roving->identifiant)}}"  class="btn btn-xs btn-outline-success delete"><i class="fa fa-trash text-danger"></i> </a>
--}}
								<a class="delete" href="{{route('roving_delete',$roving->identifiant)}}" >
									<i class="fa fa-trash text-danger"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script src="{{asset('bundles/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
<script src="{{asset('bundles/demo/default/custom/components/jquery.sweetalert.min.js')}}"></script>
<script>
    $('.roving').on('click', '.delete', function (event) {
        event.preventDefault();
        var href = $(this).attr('href');
        swal({
            title: "Êtes-vous sûr?",
            text: "Voulez vous vraiment supprimer cet roving team",
            icon: "warning",
            buttons: true,
            buttons: ["Annuler", "Oui"],
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                window.location = href;
            }
        });
    });
</script>
@endpush

