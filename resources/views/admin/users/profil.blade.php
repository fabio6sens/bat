@extends('admin.layouts.app')
@section('title', 'Profil')
@section('page_content_title', 'Mon profil')

@push('styles')
<link rel="stylesheet" href="{{ asset('bundles/dropify.css') }}">
@endpush

@section('content')

    <div class="row">
        @if(Session::has('error'))
            <div class="alert alert-danger">
                {{ Session::get('error')}}
            </div>
        @endif

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success')}}
            </div>
        @endif

        @if(Session::has('delete'))
            <div class="alert alert-success">
                {{ Session::get('delete')}}
            </div>
        @endif
    </div>

    <div class="m-content">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="m-card-profile">
                            <div class="m-card-profile__title m--hide">
                                Mon profil
                            </div>
                            <div class="m-card-profile__pic">
                                <div class="m-card-profile__pic-wrapper">
                                    <img src="{{ Auth::user()->img  ? url('userAvatar/'.Auth::user()->img.'') : url('img/user.jpeg') }}" alt=""/>
                                </div>
                            </div>
                            <div class="m-card-profile__details">
                                <span class="m-card-profile__name">
                                    {{Auth::user()->name}}
                                </span>
                                <a href="" class="m-card-profile__email m-link">
                                    {{Auth::user()->email}}
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                        <i class="flaticon-share m--hide"></i>
                                        Modifier mon profil
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                        Changer de mot de passe
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_3" role="tab">
                                        Mon Avatar
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_user_profile_tab_1">
                            <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{route('profilUpdate')}}">
                                @csrf
                                <div class="m-portlet__body">

                                    <div class="form-group m-form__group row">
                                        <label for="nom" class="col-2 col-form-label">
                                            Nom&Prénom
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="nom" id="nom" value="{{Auth::user()->name}}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Sexe
                                        </label>
                                        <div class="col-7">
                                            <select class="form-control" name="sexe" id="sexe" required="" readonly>
                                                <option value="H" {{Auth::user()->genre=='H' ? 'selected' : ''}}>Homme</option>
                                                <option value="F" {{Auth::user()->genre=='F' ? 'selected' : ''}}>Femme</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2"></div>
                                            <div class="col-7">
                                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                    Enregistrer
                                                </button>
                                                &nbsp;&nbsp;
                                                <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                    Annuler
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="m_user_profile_tab_2">
                            <form class="m-form m-form--fit m-form--label-align-right" action="{{route('profilUpdatePass')}}" method="POST">
                                @csrf
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label class="col-4 col-form-label"> Mot de passe actuel </label>
                                        <div class="col-8">
                                            <input class="form-control" name="motpass" required="required" placeholder="Mot de passe actuel" type="password">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label class="col-4 col-form-label">Nouveau mot de passe</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="newpass" required="required" placeholder="Nouveau mot de passe" type="password">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label class="col-4 col-form-label">Confirmer mot de passe</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="confirmpass" required="required" placeholder="Confirmer mot de passe" type="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2"></div>
                                            <div class="col-7">
                                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                    Enregistrer
                                                </button>
                                                &nbsp;&nbsp;
                                                <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                    Annuler
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="m_user_profile_tab_3">
                            <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{route('profilUpdateAvatar')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <div class="col-12">
                                            <input type="file" id="fileUser" name="fileUser" class="dropify" data-default-file="{{ Auth::user()->img  ? url('userAvatar/'.Auth::user()->img.'') : url('img/user.jpeg') }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2"></div>
                                            <div class="col-7">
                                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                    Enregistrer
                                                </button>
                                                &nbsp;&nbsp;
                                                <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                    Annuler
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset('bundles/dropify.js')}}"></script>
<script>
    $('.dropify').dropify();
</script>
@endpush

