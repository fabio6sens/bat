@extends('admin.layouts.app')
@section('title', 'actualites')

@section('page_content_title', 'Message')

@push('styles')

@endpush

@section('content')

    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__body">
            <div class="m-section">
                <div class="col-md-12">
                    <h3>Utilisateur</h3>
                    <p>{{$msg->userMessage->name }}</p>
                    <h3>Titre article</h3>
                    <p>{{$msg->newMessage->title}}</p>
                </div>
                <br>
                <div class="col-md-12">
                    <h3>Message</h3>
                    {{$msg->msg }}
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

@endpush

