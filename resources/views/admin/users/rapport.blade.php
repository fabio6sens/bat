@extends('admin.layouts.app')
@section('title', 'collecte')
@section('page_content_title', 'Rapport des saisies')

@section('content')
    <div class="row">
        @if(Session::has('delete'))
            <div class="alert alert-success">
                {{ Session::get('delete')}}
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-md-12 no-padding">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="col-md-6 m-portlet__head-text">
                                Rapport
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-left m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center" style="margin: 0">
                            <div class="col-xl-4 order-2 order-xl-1" style="padding-right: 0;">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-12" style="padding-left: 0;">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="Recherche individuelle..." id="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
											<span>
												<i class="la la-search"></i>
											</span>
										</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12" style="border: solid 1px #eee;  margin-bottom: 30px;">
                                <form method="post" action="{{url('users/collecte/rapport')}}" class="m-form m-form--fit m-form--label-align-left">
                                    @csrf
                                    <div class="m-portlet__body">
                                        <div class="form-group m-form__group row">
                                            {{--<label class="col-form-label col-lg-12 col-sm-12" style="padding-left: 0; font-size: 17px;">--}}
                                                {{--Trie par critères--}}
                                            {{--</label>--}}
                                            <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0">
                                                <div class="row">
                                                    <div class="col-lg-2" style="margin-bottom: 10px; margin-left: 0">
                                                        <label class="col-form-label"> Appareil </label>
                                                        <select class="form-control m-select2" id="m_select2_1" name="device">
                                                            <option value="0"> Tous</option>
                                                            <option value="3"> Appareil 3</option>
                                                            <option value="4"> Appareil 4</option>
                                                            <option value="5"> Appareil 5</option>
                                                            <option value="6"> Appareil 6</option>
                                                            <option value="7"> Appareil 7</option>
                                                            <option value="8"> Appareil 8</option>
                                                            <option value="9"> Appareil 9</option>
                                                            <option value="10"> Appareil 10</option>
                                                            <option value="11"> Appareil 11</option>
                                                            <option value="12"> Appareil 12</option>
                                                            <option value="13"> Appareil 13</option>
                                                            <option value="14"> Appareil 14</option>
                                                            <option value="15"> Appareil 15</option>
                                                            <option value="16"> Appareil 16</option>
                                                            <option value="17"> Appareil 17</option>
                                                            <option value="18"> Appareil 18</option>
                                                            <option value="19"> Appareil 19</option>
                                                            <option value="20"> Appareil 20</option>
                                                            <option value="21"> Appareil 21</option>
                                                            <option value="22"> Appareil 22</option>
                                                            <option value="23"> Appareil 23</option>
                                                            <option value="24"> Appareil 24</option>
                                                            <option value="25"> Appareil 25</option>
                                                            <option value="26"> Appareil 26</option>
                                                            <option value="27"> Appareil 27</option>
                                                            <option value="28"> Appareil 28</option>
                                                            <option value="29"> Appareil 29</option>
                                                            <option value="30"> Appareil 30</option>
                                                            <option value="31"> Appareil 31</option>
                                                            <option value="32"> Appareil 32</option>
                                                            <option value="33"> Appareil 33</option>
                                                            <option value="34"> Appareil 34</option>
                                                            <option value="35"> Appareil 35</option>
                                                            <option value="36"> Appareil 36</option>
                                                            <option value="37"> Appareil 37</option>
                                                            <option value="38"> Appareil 38</option>
                                                            <option value="39"> Appareil 39</option>
                                                            <option value="40"> Appareil 40</option>
                                                            <option value="41"> Appareil 41</option>
                                                            <option value="42"> Appareil 42</option>
                                                            <option value="43"> Appareil 43</option>
                                                            <option value="44"> Appareil 44</option>
                                                            <option value="45"> Appareil 45</option>
                                                            <option value="46"> Appareil 46</option>
                                                            <option value="47"> Appareil 47</option>
                                                            <option value="48"> Appareil 48</option>
                                                            <option value="49"> Appareil 49</option>
                                                            <option value="50"> Appareil 50</option>
                                                            <option value="51"> Appareil 51</option>
                                                            <option value="52"> Appareil 52</option>
                                                            <option value="53"> Appareil 53</option>
                                                            <option value="54"> Appareil 54</option>
                                                            <option value="55"> Appareil 55</option>
                                                            <option value="56"> Appareil 56</option>
                                                            <option value="57"> Appareil 57</option>
                                                            <option value="58"> Appareil 58</option>
                                                            <option value="59"> Appareil 59</option>
                                                            <option value="60"> Appareil 60</option>
                                                            <option value="61"> Appareil 61</option>
                                                            <option value="62"> Appareil 62</option>
                                                            <option value="63"> Appareil 63</option>
                                                            <option value="64"> Appareil 64</option>
                                                            <option value="65"> Appareil 65</option>
                                                            <option value="66"> Appareil 66</option>
                                                            <option value="67"> Appareil 67</option>
                                                            <option value="68"> Appareil 68</option>
                                                            <option value="69"> Appareil 69</option>
                                                            <option value="70"> Appareil 70</option>
                                                            <option value="71"> Appareil 71</option>
                                                            <option value="72"> Appareil 72</option>
                                                            <option value="73"> Appareil 73</option>
                                                            <option value="74"> Appareil 74</option>
                                                            <option value="75"> Appareil 75</option>
                                                            <option value="76"> Appareil 76</option>
                                                            <option value="77"> Appareil 77</option>
                                                            <option value="78"> Appareil 78</option>
                                                            <option value="79"> Appareil 79</option>
                                                            <option value="80"> Appareil 80</option>
                                                            <option value="81"> Appareil 81</option>
                                                            <option value="82"> Appareil 82</option>
                                                            <option value="83"> Appareil 83</option>
                                                            <option value="84"> Appareil 84</option>
                                                            <option value="85"> Appareil 85</option>
                                                            <option value="86"> Appareil 86</option>
                                                            <option value="87"> Appareil 87</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3" style="margin-bottom: 10px; margin-left: 18px">
                                                        <label class="col-form-label"> Date Debut</label>
                                                        <input type="date" class="form-control m-input" name="date" required>
                                                    </div>
                                                    <div class="col-lg-3" style="margin-bottom: 10px; margin-left: 18px">
                                                        <label class="col-form-label"> Date Fin</label>
                                                        <input type="date" class="form-control m-input" name="datefin" required>
                                                    </div>
                                                    <div class="col-lg-3" style="position: relative; top: 35px;">
                                                        <button type="submit" class="btn btn-success m-btn m-btn--pill" name="btn" value="trie">
                                                            Rapport
                                                        </button>
                                                        <button type="submit" class="btn btn-success m-btn m-btn--pill" name="btn" value="export">
                                                            Exporter
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <table class="m-datatable" id="html_table" width="100%">
                        <thead class="m-datatable__head">
                        <tr class="m-datatable__row" style="height: 53px;">
                            <th data-field="N°">
                                <span style="width: 110px;">Appareil</span>
                            </th>
                            <th title="Field #1">
                                <span style="width: 110px;">Nombre d'enregistrement</span>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            @if($datas)
                                @foreach($datas as $data)
                                    <tr data-row="0" class="m-datatable__row m-datatable__row--even" style="height: 64px;">
                                        <td class="m-datatable__cell">
                                            <span style="width: 110px;">Appareil {{$data->device_id}}</span>
                                        </td>
                                        <td class="m-datatable__cell">
                                            <span style="width: 110px;">{{$data->views}}</span>
                                        </td>

                                        {{--<th data-field="N°"> <span style="width: 110px;">Appareil {{$data->device_id}}</span> </th>--}}
                                        {{--<td>{{$data->views}}</td>--}}
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{asset('bundles/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
<script src="{{asset('bundles/demo/default/custom/components/forms/widgets/select2.js')}}" type="text/javascript"></script>
@endpush

