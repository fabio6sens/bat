@extends('admin.layouts.app')
@section('title', 'collecte')
@section('page_content_title', 'Rapport des saisies CRAVEN A ROUGE')

@section('content')
    <div class="row">
        @if(Session::has('delete'))
            <div class="alert alert-success">
                {{ Session::get('delete')}}
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-md-12 no-padding">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="col-md-6 m-portlet__head-text">
                                Rapport
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-left m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center" style="margin: 0">
                            <div class="col-xl-4 order-2 order-xl-1" style="padding-right: 0;">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-12" style="padding-left: 0;">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="Recherche individuelle..." id="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
											<span>
												<i class="la la-search"></i>
											</span>
										</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12" style="border: solid 1px #eee;  margin-bottom: 30px;">
                                <form method="post" action="{{url('users/cravenarouge/rapport')}}" class="m-form m-form--fit m-form--label-align-left">
                                    @csrf
                                    <div class="m-portlet__body">
                                        <div class="form-group m-form__group row">
                                            {{--<label class="col-form-label col-lg-12 col-sm-12" style="padding-left: 0; font-size: 17px;">--}}
                                                {{--Trie par critères--}}
                                            {{--</label>--}}
                                            <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0">
                                                <div class="row">
                                                    <div class="col-lg-2" style="margin-bottom: 10px; margin-left: 0">
                                                        <label class="col-form-label"> Roving team </label>
                                                        <select class="form-control m-select2" id="m_select2_1" name="device">
                                                            <option value="0"> Tous</option>
                                                            @foreach($roving as $roving)
                                                                <option value="{{$roving->id}}">{{$roving->identifiant}} {{$roving->nom}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3" style="margin-bottom: 10px; margin-left: 18px">
                                                        <label class="col-form-label"> Date Debut</label>
                                                        <input type="date" class="form-control m-input" name="date" required>
                                                    </div>
                                                    <div class="col-lg-3" style="margin-bottom: 10px; margin-left: 18px">
                                                        <label class="col-form-label"> Date Fin</label>
                                                        <input type="date" class="form-control m-input" name="datefin" required>
                                                    </div>
                                                    <div class="col-lg-3" style="position: relative; top: 35px;">
                                                        <button type="submit" class="btn btn-success m-btn m-btn--pill" name="btn" value="trie">
                                                            Rapport
                                                        </button>
                                                        <button type="submit" class="btn btn-success m-btn m-btn--pill" name="btn" value="export">
                                                            Exporter
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <table class="m-datatable" id="html_table" width="100%">
                        <thead class="m-datatable__head">
                        <tr class="m-datatable__row" style="height: 53px;">
                            <th>
                                <span style="width: 110px;">Roving team</span>
                            </th>
                            <th title="Field #1">
                                <span style="width: 110px;">Nombre d'enregistrement</span>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            @if($datas)
                                @foreach($datas as $data)
                                    <tr data-row="0" class="m-datatable__row m-datatable__row--even" style="height: 64px;">
                                        <td class="m-datatable__cell">
                                            <span style="width: 110px;">{{$data->device_id}}</span>
                                        </td>
                                        <td class="m-datatable__cell">
                                            <span style="width: 110px;">{{$data->views}}</span>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{asset('bundles/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
<script src="{{asset('bundles/demo/default/custom/components/forms/widgets/select2.js')}}" type="text/javascript"></script>
@endpush

