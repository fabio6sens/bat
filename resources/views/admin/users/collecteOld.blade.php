@extends('admin.layouts.app')
@section('title', 'collecte')
@section('page_content_title', 'Toute la collecte de données')

@section('content')
<div class="row">
	@if(Session::has('delete'))
		<div class="alert alert-success">
		  {{ Session::get('delete')}}
		</div>
	@endif
</div>
<div class="row">
	<div class="col-md-12 no-padding">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Liste des collectés
						</h3>
					</div>
				</div>
			</div>

			<div class="m-portlet__body">
				<div class="m-form m-form--label-align-left m--margin-top-20 m--margin-bottom-30">
					<div class="row align-items-center" style="margin: 0">
						<div class="col-xl-4 order-2 order-xl-1" style="padding-right: 0;">
							<div class="form-group m-form__group row align-items-center">
								<div class="col-md-12">
									<div class="m-input-icon m-input-icon--left">
										<input type="text" class="form-control m-input m-input--solid" placeholder="Recherche par item..." id="generalSearch">
										<span class="m-input-icon__icon m-input-icon__icon--left">
											<span>
												<i class="la la-search"></i>
											</span>
										</span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-xl-8" style="border: solid 1px #eee;">
							<form method="post" action="{{url('users/export')}}" class="m-form m-form--fit m-form--label-align-left">
								@csrf
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-4 col-sm-12">
											Recherche par critères
										</label>
										<div class="col-lg-6 col-md-8 col-sm-12" style="padding-left: 0">
											<select class="form-control m-select2" id="m_select2_1" name="param">
												@foreach($villes as $ville)
												<option>
													{{$ville->libelle}}
												</option>
												@endforeach
											</select>
										</div>
										<div class="col-sm-2">
											<button type="submit" class="btn btn-success m-btn m-btn--pill">
												Exporter
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>

					</div>
				</div>

				<div class="m-datatable m-datatable--default m-datatable--brand m-datatable--loaded"><table class="m-datatable__table" id="m-datatable--1516780828608" width="100%" style="display: block; height: auto; overflow-x: auto;">
					<table class="m-datatable__table" id="m-datatable--1516780828608" width="100%" style="display: block; height: auto; overflow-x: auto;">
						<thead class="m-datatable__head">
							<tr class="m-datatable__row" style="height: 53px;">
								<th class="m-datatable__cell m-datatable__cell--sort" data-field="Order ID">
									<span style="width: 110px;">N°</span>
								</th>
								<th title="Field #1" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">Nom</span>
								</th>
								<th title="Field #2" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">Email</span>
								</th>
								<th title="Field #3" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">Téléphone</span>
								</th>
								<th title="Field #4" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">Device</span>
								</th>
								<th title="Field #5" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">Zone</span>
								</th>
								<th title="Field #6" class="m-datatable__cell m-datatable__cell--sort">
									<span style="width: 110px;">Actions</span>
								</th>
							</tr>
						</thead>
						<tbody class="m-datatable__body" style="">
							@foreach($users as $k => $user)
							<tr data-row="0" class="m-datatable__row m-datatable__row--even" style="height: 64px;">
								<td class="m-datatable__cell">
									<span style="width: 110px;">{{$k+1}}</span>
								</td>
								<td class="m-datatable__cell">
									<span style="width: 110px;">{{$user->name}}</span>
								</td>
								<td class="m-datatable__cell">
									<span style="width: 110px;">{{$user->email}}</span>
								</td>
								<td class="m-datatable__cell">
									<span style="width: 110px;">{{$user->telephone}}</span>
								</td>
								<td class="m-datatable__cell">
									<span style="width: 110px;">{{$user->device->nom}}</span>
								</td>
								<td class="m-datatable__cell">
									<span style="width: 110px;">{{$user->ville->libelle}}</span>
								</td>
								<td class="m-datatable__cell">
									<div class="dropdown">
										<button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											Actions
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item" href="#">
												Détails
											</a>
											<a data-toggle="modal" data-target="#m_modal_1" role="button" class="delete-modal dropdown-item" data-route="/users" data-id="{{$user->id}}" data-toggle="modal">
												Supprimer
											</a>
										</div>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
    <script src="{{asset('bundles/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
    <script src="{{asset('bundles/demo/default/custom/components/forms/widgets/select2.js')}}" type="text/javascript"></script>
@endpush

