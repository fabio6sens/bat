@extends('admin.layouts.app')
@section('title', 'Collecte')

@section('page_content_title', 'Collecte')

@push('styles')

@endpush

@section('content')

    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__body">
            <div class="m-section">
               <div class='row'>
                    <div class="col-md-6">
                        <h4>Nom&Prénom</h4>
                        <p>{{$user->name }}</p>
                        
                        <h4>Sexe</h4>
                        <p>{{$user->genre=='H' ? 'Homme' : 'Femme'}}</p>
                        
                        <h4>Contact</h4>
                        <p>{{$user->telephone }}</p>
                        
                        <h4>Date de naissance</h4>
                        <p>{{ date("d/m/Y",strtotime($user->created_at))}}</p>
                        
                        <h4>Email</h4>
                        <p>{{$user->email }}</p>
                        
                        <h4>Instagram</h4>
                        <p>{{$user->instagram }}</p>
                        
                    </div>
                    
                    <div class="col-md-6">
                        {{--<h4>Ville</h4>
                        <p>{{$user->ville->libelle }}</p>--}}
                        
                        <h4>Préférence</h4>
                        <p>{{$user->pqt_acht }}</p>
                        
                        <h4>Moyen de contact</h4>
                        <p>{{$user->moy_contact }}</p>
                        
                        <h4>Marque reguilière</h4>
                        <p>{{$user->marque_pref }}</p>
                        
                        <h4>Parquet acheté</h4>
                        <p>{{$user->marque_alt }}</p>
                        
                        <h4>Paquet d'echantillonage</h4>
                        <p>{{$user->echantillonage_pqt }}</p>
                    </div>
               </div>
                
            </div>
        </div>
    </div>

@endsection

@push('scripts')

@endpush

