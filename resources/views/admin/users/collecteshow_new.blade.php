@extends('admin.layouts.app')
@section('title', 'Collecte')

@section('page_content_title', 'Collecte')

@push('styles')

@endpush

@section('content')

    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__body">
            <div class="m-section">
               <div class='row'>
                   <div class="col-md-12">
                       <h3>ROVING TEAM</h3>
                       <p>{{$user->roving->nom }}</p>
                   </div>
                    <div class="col-md-6">
                        <h4>PRODUIT</h4>
                        <p>{{$user->type_pdt }}</p>

                        <h4>Nom&Prénom</h4>
                        <p>{{$user->nom }}</p>
                        
                        <h4>Sexe</h4>
                        <p>{{$user->genre}}</p>
                        
                        <h4>Contact</h4>
                        <p>{{$user->telephone}}</p>
                        
                        <h4>Date de naissance</h4>
                        <p>{{ date("d/m/Y",strtotime($user->datenaiss))}}</p>

                        <h4>Adresse</h4>
                        <p>
                            @if(!empty($user->ads_quartier)) {{$user->ads_quartier }} @endif
                            @if(!empty($user->ads_commun))<br> {{$user->ads_commun }} @endif
                            @if(!empty($user->ads_ville))<br> {{$user->ads_ville }} @endif
                            @if(!empty($user->ads_region))<br> {{$user->ads_region }} @endif
                        </p>

                    </div>
                    
                    <div class="col-md-6">
                        <h4>Moyenne de contact</h4>
                        <p>{{$user->moy_contact }} <br> {{$user->moy_contact_value }}</p>

                        <h4>Marque regulière</h4>
                        <p>{{$user->marque_reg }}</p>
                        
                        <h4>Paquet d'echantillonage</h4>
                        <p>{{$user->echantillonage_pqt }}</p>
                        
                        <h4>Parquet acheté</h4>
                        <p>{{$user->pqt_acht }}</p>
                        
                        <h4>Variante acheté</h4>
                        <p>{{$user->variante_acht }}</p>

                        <h4>Lot</h4>
                        <p>{{$user->lot }}</p>


                    </div>
               </div>
                
            </div>
        </div>
    </div>

@endsection

@push('scripts')

@endpush

