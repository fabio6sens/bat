@extends('admin.layouts.app')
@section('title', 'index')
@section('page_content_title', 'Dashboard')

@push('styles')
.privacy-link {
    display: none!important;
}
.app-name.ng-binding {
    display: none!important;
}
@endpush

@section('content')
    <div class="m-portlet">
	<div class="m-portlet__body  m-portlet__body--no-padding">
	    @if(Auth::user()->role_id !=2)
		<div class="row m-row--no-padding m-row--col-separator-xl">
			<div class="col-xl-6">
				<!--begin:: Widgets/Stats2-1 -->
				<div class="m-widget1">
					<div class="m-widget1__item col-xl-12">
						<div class="row m-row--no-padding align-items-center">
							<div class="col">
								<h3 class="m-widget1__title">
									Collecte
								</h3>
								<span class="m-widget1__desc">
									DUNHILL
								</span>
							</div>
							<div class="col m--align-right">
								<span class="m-widget1__number m--font-brand">
									{{$users}}
								</span>
							</div>
						</div>
					</div>
				</div>
				<!--end:: Widgets/Stats2-1 -->
			</div>

			<div class="col-xl-6">
				<!--begin:: Widgets/Stats2-1 -->
				<div class="m-widget1">
					<div class="m-widget1__item col-xl-12">
						<div class="row m-row--no-padding align-items-center">
							<div class="col">
								<h3 class="m-widget1__title">
									Solde sms
								</h3>
								<span class="m-widget1__desc">
									disponible
								</span>
							</div>
							<div class="col m--align-right">
								<span class="m-widget1__number m--font-brand">
									{{$sms_send}}
								</span>
							</div>
						</div>
					</div>
				</div>
				<!--end:: Widgets/Stats2-1 -->
			</div>

		</div>

		<div class="row m-row--no-padding m-row--col-separator-xl">
			<div class="col-xl-6">
				<!--begin:: Widgets/Stats2-1 -->
				<div class="m-widget1">
					<div class="m-widget1__item col-xl-12">
						<div class="row m-row--no-padding align-items-center">
							<div class="col">
								<h3 class="m-widget1__title">
									Collecte
								</h3>
								<span class="m-widget1__desc">
									CRAVEN A ROUGE
								</span>
							</div>
							<div class="col m--align-right">
								<span class="m-widget1__number m--font-brand">
									{{$devices_delivred->statut}}
								</span>
							</div>
						</div>
					</div>
				</div>
				<!--end:: Widgets/Stats2-1 -->
			</div>

			<div class="col-xl-6">
				<!--begin:: Widgets/Stats2-1 -->
				<div class="m-widget1">
					<div class="m-widget1__item col-xl-12">
						<div class="row m-row--no-padding align-items-center">
							<div class="col">
								<h3 class="m-widget1__title">
									Roving team
								</h3>
								<span class="m-widget1__desc">
									meilleur enregistreur
								</span>
							</div>
							<div class="col m--align-right">
								<span class="m-widget1__number m--font-brand">
									{{$device_name}}
								</span>
							</div>
						</div>
					</div>
				</div>
				<!--end:: Widgets/Stats2-1 -->
			</div>
		</div>
		@endif

		{{--<div class="row m-row--no-padding m-row--col-separator-xl">
			<iframe width="100%" height="450" src="https://datastudio.google.com/embed/reporting/1iJdLn7B69xji0QntvToHfh2oRe2XhDXl/page/H38k" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>--}}
	</div>
</div>
@endsection