@extends('layouts.app')

@section('content')
    <div class="m-login__head">
        <h3 class="m-login__title">
            Confirmer votre identité
        </h3>
    </div>
    
    @if(Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error')}}
        </div>
    @endif

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success')}}
        </div>
    @endif
    
    <form class="m-login__form m-form" action="{{ route('loginadminme') }}" method="POST">
        @csrf
        {{--<div class="form-group m-form__group">
            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} m-input" type="text" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>--}}

        <div class="form-group m-form__group">
            <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Mot de passe" name="password">
            <label class="col-md-12 labeLogin">Entrez votre mot de passe</label>
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>

        {{--<div class="row m-login__form-sub">
            <div class="col m--align-left m-login__form-left">
                <label class="m-checkbox  m-checkbox--light">
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    Se souvenir de moi
                    <span></span>
                </label>
            </div>
        </div>--}}
        <div class="m-login__form-action">
            <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
                Connexion
            </button>
            <input type="hidden" name="slug" value="{{$decodencode}}">
        </div>
    </form>
@endsection
