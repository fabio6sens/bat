@extends('admin.layouts.app')
@section('title', 'galerie')
@section('page_content_title', 'Galerie')

@push('styles')
<link rel="stylesheet" href="{{ asset('bundles/dropify.css') }}">
@endpush

@section('content')
<div class="row">
	@if(Session::has('error'))
		<div class="alert alert-danger">
		  {{ Session::get('error')}}
		</div>
	@endif

	@if(Session::has('success'))
		<div class="alert alert-success">
		  {{ Session::get('success')}}
		</div>
	@endif
</div>

    <!--begin::Modal-->
    <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> Ajouter une image  </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true"> &times;</span></button>
                </div>
                <form action="{{route('addgallery')}}" method="post" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    {{--<div class="m-dropzone dropzone" action="{{route('addgallery')}}" id="m-dropzone-one">
                        <div class="m-dropzone__msg dz-message needsclick">
                            <h3 class="m-dropzone__msg-title">
                                Télécharger vos fichier ici.
                            </h3>
                        </div>
                    </div>--}}
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="file"><strong>Photo</strong></label>
                                <input type="file" id="file" name="file" class="dropify" required/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> Annuler </button>
                        <button type="submit" name="addimg" class="btn btn-primary"> <i class="fa fa-save"></i> Ajouter </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->

    <div class="m-portlet m-portlet--mobile">

        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <button type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" data-toggle="modal" data-target="#m_modal_1">
                            <i class="la la-plus"></i> Ajouter une image
                        </button>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <table class="m-datatable" id="html_table" width="100%">
                <thead>
                <tr>
                    <th title="Ref.No."> Ref.No. </th>
                    <th title="Images"> Images </th>
                    <th title="Action"> Action </th>
                </tr>
                </thead>
                @if(!empty($allimg))
                <tbody>
                @foreach($allimg as  $k =>$img)
                    <tr>
                        <td>#0{{$k+1}} </td>
                        <td><img src="{{url('frontgalerie/'.$img->imgs) }}" alt="photo" width="100px" height="70px"> </td>
                        <td><a href="{{route('delimggallery',$img->id)}}" class="btn btn-outline-danger"><i class="fa fa-trash text-danger"></i> </a></td>
                    </tr>
                @endforeach
                </tbody>
                @endif
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection

@push('scripts')
    <!--begin::Page Resources -->
    <script src="{{asset('bundles/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
    <!--end::Page Resources -->
    <script src="{{ asset('bundles/dropify.js')}}"></script>
    <script>
        $('.dropify').dropify();
    </script>
@endpush

