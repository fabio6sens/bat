@extends('layouts.app')

@section('content')
<div class="modal fade show" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="myModal" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModal">TERMES ET CONDITIONS</h4>
                </div>

                <div class="modal-body modal-short">
                    <h4><b>1.INTRODUCTION</b></h4>
                    <p>1.1 Veuillez vous assurer d'avoir lu attentivement les présentes conditions générales du site Web, en particulier les limitations de notre responsabilité, avant d'utiliser le site Web.</p>
                    <p>1.2 Les présentes conditions générales du site Web ("Conditions"), ainsi que toutes les informations et tous les documents qui y sont mentionnés, régissent votre utilisation de ce site Web (le "Site Web"). L'utilisation inclut l'accès, la navigation ou l'enregistrement pour utiliser notre site Web.</p>
                    <p>1.3 En utilisant ce site Web, vous acceptez ces Conditions. Vous devez imprimer une copie de ces conditions ou les sauvegarder pour pouvoir vous y référer ultérieurement.</p>
                    <p>1.4 Nous pouvons modifier les présentes conditions de temps à autre. Veuillez les consulter régulièrement pour vous assurer de comprendre les termes qui s'appliquent à chaque fois que vous utilisez le site Web. Ces conditions ont été mises à jour le 1er février 2019.</p>
                    <p>1.5 Si vous n'acceptez pas les conditions, vous ne devez pas utiliser le site Web.</p>
                    <h4><b>2.INFORMATIONS SUR NOUS</b></h4>
                    <p>2.1 Ce site Web est exploité par British American Tobacco Marketing Nigeria Limited, une société à responsabilité limitée enregistrée en République fédérale du Nigéria sous le numéro RC483119 et dont le siège social est situé 2 Olumegbon Road, Ikoyi, Lagos.</p>
                    <p>2.2 Pour nous contacter, veuillez consulter la page Contactez-nous sur notre site internet, envoyez-nous un courrier électronique à l'adresse (batna_feedback@bat.com) ou écrivez-nous par courrier à l'adresse suivante: Services des affaires commerciales, British American Marketing Nigeria Limited, 2 Olumegbon Road, Ikoyi, Lagos.</p>
                    <p><b>3. ACCÉDER À NOTRE SITE WEB</b></p>
                    <p>3.1 Vous acceptez d'utiliser le site Web uniquement pour votre utilisation personnelle et non commerciale.</p>
                    <p>3.2 Nous ne garantissons pas que notre site Web, ou son contenu, sera toujours disponible ou ininterrompu. L'accès à notre site Web est autorisé sur une base temporaire. Nous pouvons suspendre, retirer, interrompre ou modifier tout ou partie de notre site Web sans préavis. Nous ne serons pas responsables envers vous si, pour une raison quelconque, le site Web n'est pas disponible à tout moment ou pour toute période.</p>
                    <p>3.3 Vous êtes responsable de vous assurer que vous avez tout le matériel et les logiciels nécessaires pour accéder au site Web. Vous devez utiliser votre propre logiciel de protection antivirus lorsque vous accédez au site Web. Vous êtes également responsable de veiller à ce que toutes les personnes accédant à notre site Web par le biais de votre connexion Internet soient informées des présentes Conditions et des autres conditions applicables, et qu'elles s'y conforment.</p>
                    <p>3.4 Vous acceptez de ne pas: <br>
                        a. utiliser le site Web pour enfreindre de quelque manière que ce soit la vie privée ou les autres droits des autres utilisateurs du site Web ou de tiers;<br>
                        b. utiliser le site Web de quelque manière que ce soit qui puisse encourager, obtenir ou mener à bien une activité criminelle ou illégale, ou causer de la détresse, un préjudice ou des inconvénients à une autre personne;<br>
                        c. faire quelque chose qui puisse causer des dommages au site Web ou à nos serveurs, systèmes ou équipements ou ceux de tiers, ni accéder ou tenter d'accéder aux données des utilisateurs. Vous ne devez pas attaquer notre site Web via une attaque par déni de service ou une attaque par déni de service distribué. Nous signalerons toute violation de ce type aux autorités répressives compétentes et nous coopérerons avec ces autorités en leur divulguant votre identité. Dans le cas d'une telle violation, votre droit d'utiliser notre site Web cessera immédiatement;<br>
                        e. faire quoi que ce soit qui soumet le site Web à un traitement dérogatoire ou qui jette (ou pourrait jeter) le discrédit sur le site Web;<br>
                        f. mine de données, écran ou Web gratter ou explorer ce site Web, ses pages ou son contenu ou utiliser tout processus qui envoie des requêtes automatisées à ce site Web à moins d'avoir obtenu notre consentement écrit préalable;<br>
                        g. utiliser à mauvais escient ou faire quelque chose qui perturbe tout ou une partie du site Web, y compris, sans toutefois s'y limiter, l'introduction délibérée de virus de logiciels, de chevaux de Troie ou de tout autre matériel pouvant être nuisible sur le plan technologique;<br>
                        h. Indiquez, suggérez ou donnez l'impression que vous entretenez des relations commerciales avec nous, que vous êtes notre agent ou que nous avons endossé vos propositions au site Web.
                    </p>
                    <p>3.5 Nous ne garantissons pas que le site Web sera sécurisé ou exempt de bugs ou de virus.</p>
                    <h4><b>4. NOTRE CONTENU</b></h4>
                    <p>4.1 Tous les droits d'auteur et autres droits de propriété intellectuelle sur les œuvres d'art, les graphiques, les textes, les clips audio et vidéo, les marques commerciales, les logos et tout autre contenu disponible sur ce site Web (le «Contenu») sont notre propriété ou sont utilisés avec autorisation.</p>
                    <p>4.2 Bien que vous puissiez copier une partie de ce site Web sur votre propre ordinateur pour votre usage personnel, vous ne pouvez pas copier ou incorporer le contenu disponible sur le site Web dans un autre ouvrage, y compris votre propre site Web, ni utiliser le contenu dans
                        un autre de manière publique ou commerciale. Notre statut (et celui de tout contributeur identifié) en tant qu'auteur du contenu doit toujours être reconnu. Vous ne pouvez publier ou redistribuer aucune partie de notre site Web à moins que vous ne disposiez d'une licence de notre part. Nous conservons l'intégralité du titre de ce Contenu, y compris tout logiciel ou code téléchargeable, toutes images incorporées dans ou générées par le logiciel, ainsi que toutes les données l'accompagnant. Vous ne devez pas copier, modifier, reproduire, télécharger, transmettre, distribuer, faire de l'ingénierie inverse, désassembler ou convertir de quelque manière que ce soit sous une autre forme.
                    </p>
                    <p>
                        4.3 Vous ne pouvez modifier aucun des avis relatifs au droit d'auteur, aux marques commerciales ou aux autres marques pouvant accompagner le contenu. Vous pouvez créer un lien vers la page d'accueil de notre site Web, mais vous ne pouvez pas utiliser notre contenu sur votre propre site. Vous ne pouvez pas créer de lien profond (par exemple, un lien vers une page autre que la page d'accueil) vers notre site Web ou créer un cadre pour notre site Web sur d'autres sites Web sans notre autorisation écrite spécifique.
                    </p>
                    <p>4.4 Sauf indication contraire, toutes les marques utilisées sur ce site Web restent notre propriété ou celle des sociétés de notre groupe.</p>
                    <p>4.5 Si vous imprimez, copiez ou téléchargez le contenu en violation des présentes conditions, votre droit d'utiliser le site Web cessera immédiatement et vous devrez, à notre choix, renvoyer ou détruire toute copie du contenu que vous avez créé.</p>
                    <h4><b>5. VOTRE VIE PRIVÉE ET NOTRE UTILISATION DE COOKIES</b></h4>
                    <p>5.1 Toutes les données personnelles que vous nous fournissez via notre site Web ne seront utilisées que conformément aux présentes conditions et à notre politique de confidentialité. Assurez-vous d'avoir lu notre politique de confidentialité avant de poursuivre.</p>
                    <p>5.2 En nous fournissant vos données personnelles, vous consentez à leur utilisation conformément aux présentes conditions et à notre politique de confidentialité.</p>
                    <p>
                        5.3 Comme de nombreux services en ligne, nous utilisons une fonctionnalité appelée «cookie», un petit fichier de données envoyé à votre navigateur par un serveur Web et stocké sur le disque dur de votre appareil. Les références aux «cookies» dans ces Conditions incluent également d’autres moyens d’accéder ou d’enregistrer automatiquement des informations sur votre appareil. En acceptant ces Conditions, vous nous autorisez à utiliser les cookies de la manière décrite dans notre Politique de gestion des cookies. Cependant, vous pouvez supprimer ces cookies à tout moment si vous le souhaitez. Veuillez consulter notre politique en matière de cookies pour obtenir des informations détaillées sur les types de cookies que nous utilisons sur le site Web, sur les finalités pour lesquelles nous utilisons chaque cookie, sur la manière dont vous pouvez désactiver et permettre l'utilisation de certains cookies et sur les conséquences.
                    </p>
                    <h4><b>6. RESPONSABILITÉ</b></h4>
                    <p>6.1 Nous ne serons pas responsables vis-à-vis des utilisateurs pour toute perte ou tout dommage, contractuel, délictuel (y compris la négligence), manquement à un e obligation légale ou autre, même si prévisible, découlant ou lié à:
                        <br>
                        6.1.1 l'utilisation ou l'impossibilité d'utiliser le site Web; ou <br>
                        6.1.2 utilisation ou confiance en tout contenu affiché sur le site Web.
                    </p>
                    <p>6.2 Si vous êtes un utilisateur professionnel, veuillez noter qu'en particulier, nous ne serons pas responsables de:
                        <br>
                        6.2.1 manque à gagner, ventes, affaires ou revenus; <br>
                        6.2.2 interruption d'activité; <br>
                        6.2.3 perte d'économies prévues;<br>
                        6.2.4 perte d'opportunité commerciale, de clientèle ou de réputation; <br>
                        6.2.5 toute perte ou tout dommage indirect ou consécutif.
                    </p>
                    <p>6.3 Veuillez noter que nous fournissons le site Web uniquement pour un usage domestique et privé. Vous acceptez de ne pas utiliser notre site à des fins commerciales ou commerciales.</p>
                    <p>6.4 Bien que nous déployions des efforts raisonnables pour mettre à jour les informations sur notre site Web, nous ne faisons aucune déclaration, garantie ou garantie, expresse ou implicite, que le contenu de notre site est exact, complet et à jour.</p>
                    <p>6.5 Nous ne serons pas responsables des pertes ou dommages causés par un virus, une attaque par déni</p>
                    <p>6.6 Rien dans les présentes Conditions ne limite ni n’exclut notre responsabilité en cas de déclaration frauduleuse, de mort ou de blessure résultant de notre négligence ou de la négligence de nos agents ou employés, ni en ce qui concerne toute autre responsabilité qui ne peut être limitée ou exclue par la loi.</p>
                    <p>6.7 Ce site Web peut faire référence à des produits ou services de tiers ou créer un lien vers des sites tiers ou des informations. Nous n'approuvons ni ne faisons aucune garantie ou déclaration concernant ces produits ou services et déclinons toute responsabilité quant au contenu des sites Web liés à notre site Web. Tous les liens vers d’autres sites Web sont fournis uniquement pour votre commodité. Nous ne serons pas responsables des pertes ou des dommages pouvant découler de votre utilisation de ceux-ci. Vous devez décider vous-même si vous souhaitez utiliser des produits, services et sites Web tiers.</p>
                    <p>6.8 Le contenu de notre site Web est fourni à titre d'information générale seulement. Il ne s'agit pas d'un conseil sur lequel vous devriez vous appuyer. Bien que nous déployions des efforts raisonnables pour mettre à jour les informations sur notre site Web, nous ne faisons aucune déclaration, garantie ou garantie, expresse ou implicite, que le contenu de notre site Web est exact, complet et à jour.</p>
                    <h4><b>7. ÉVÉNEMENTS OU CIRCONSTANCES HORS DE NOTRE CONTRÔLE</b></h4>
                    <p>Nous ne serons en aucun cas responsables des pertes, dommages ou dépenses que vous encourez, résultant directement ou indirectement de toute défaillance ou retard dans l'exécution de l'une de nos obligations en vertu des présentes Conditions, causés par des circonstances indépendantes de notre volonté, notamment: vandalisme, accident, panne ou endommagement de la machinerie ou de l'équipement, incendie, inondation, catastrophe naturelle, grève, lock-out ou autre conflit du travail (impliquant ou non nos employés) ou pénurie de matériaux ou de carburant aux taux du marché en vigueur l'ordre est accepté, ingérence législative ou administrative.</p>
                    <h4><b>8. AUTRES TERMES IMPORTANTS</b></h4>
                    <p>8.1 Notre confiance dans ces conditions. Nous avons l'intention de nous appuyer sur ces Conditions écrites et sur tout document expressément mentionné dans celles-ci en rapport avec leur sujet. Nous et vous serez légalement liés par ces Conditions.</p>
                    <p>8.2 Si un tribunal estime qu'une partie de ces Conditions est illégale, le reste restera en vigueur. Chacune des clauses de ces Termes fonctionne séparément. Si un tribunal ou une autorité compétente décide que l’une quelconque de ces Conditions est illégale, invalide ou inexécutable, cela n’affectera pas la validité des Conditions restantes, qui resteront pleinement en vigueur.</p>
                    <p>8.3 Même si nous différons l’application de ces Conditions, nous pourrons les appliquer ultérieurement. Si nous omettons d'insister pour que vous remplissiez l'une de vos obligations en vertu des présentes Conditions, ou si nous retardons ou n'appliquons pas nos droits à votre encontre, cela ne signifie pas que nous avons renoncé à nos droits à votre encontre ni que vous n'avez pas pour exécuter vos obligations.</p>
                    <p>8.4 Vous avez besoin de notre consentement pour transférer vos droits à quelqu'un d'autre. Vous ne pouvez transférer vos droits ou vos obligations découlant des présentes Conditions à une autre personne que si nous acceptons cela par écrit.</p>
                    <p>8.5 Nous pouvons transférer notre accord en vertu de ces Conditions à quelqu'un d'autre. Nous pouvons transférer nos droits et obligations aux termes des présentes Conditions à toute société de notre groupe, sans autre consentement de votre part.</p>
                    <p>8.6 Personne d'autre n'a aucun droit en vertu de ces Conditions. Ces conditions sont entre vous et nous. Sauf en ce qui concerne toute société de notre groupe, aucune autre personne ne peut appliquer l'une quelconque de ces Conditions.</p>
                    <p>8.7 Communications écrites. Nous vous ferons parvenir des avis et d’autres communications à l’adresse électronique que vous nous avez fournie. Vous devez nous envoyer tous les avis et autres communications en utilisant l’une des méthodes de communication mentionnées à la clause 2.2.</p>
                    <p>8.9 Nous pouvons changer ces conditions. Nous pouvons changer, modifier ou réviser ces Conditions à tout moment. Toute modification apportée aux conditions s'appliquera 7 jours après la date à laquelle nous publions les conditions modifiées sur notre site Web. Il est de votre responsabilité de vérifier que vous avez lu et accepté les dernières conditions sur le site Web.</p>
                    <p>8.10 Quelles lois s'appliquent aux présentes Conditions. Les présentes conditions sont régies par les lois de la République fédérale du Nigéria. Vous et nous convenons tous les deux que les tribunaux nigérians auront une compétence exclusive.</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-check"></i> J'accepte</button>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
<div class="modal fade show" id="termsModal2" tabindex="-1" role="dialog" aria-labelledby="myModal" style="padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModal">AVIS DE CONFIDENTIALITÉ</h4>
                </div>

                <div class="modal-body modal-short">
                    <p>Cet avis de confidentialité explique ce que nous faisons avec vos informations personnelles lorsque vous visitez le site www.pluggedin.ng («site Web»). Il décrit comment nous recueillons, utilisons et traitons vos informations personnelles et comment nous nous conformons à nos obligations juridiques envers vous. Votre vie privée est importante pour nous et nous nous engageons à protéger et à protéger vos droits.</p>
                    <p>Nous pouvons modifier cet avis de confidentialité de temps à autre. Veuillez visiter cette page régulièrement car nous publierons tous les changements ici. Le cas échéant, nous pourrons également vous informer des modifications par courrier électronique.</p>
                    <p>Si vous n'êtes pas satisfait de l'un des aspects de cet avis de confidentialité, vous pouvez bénéficier des droits légaux décrits ci-dessous, le cas échéant.</p>
                    <h4><b>1. INFORMATIONS QUE NOUS COLLECTONS SUR VOUS</b></h4>
                    <p>
                        Lorsque vous utilisez le site Web, nous collectons et utilisons des informations vous concernant dans le cadre de nos services et de l'assistance clientèle. Nous pouvons collecter tout ou partie des informations répertoriées ci-dessous pour nous aider à cet égard:
                    </p>
                    <p>
                        les informations que vous soumettez en ligne via le site Web, y compris votre nom, vos coordonnées, votre date de naissance et votre âge. Nous collectons ces informations de différentes manières, y compris les interactions hors ligne avec nous. et
                        <br>
                        des informations techniques sur votre visite, notamment les détails de vos visites sur le site Web et de votre navigation sur celui-ci, des données de trafic, des données de communication, des informations sur l'appareil que vous utilisez pour accéder au site Web, votre adresse IP (Internet Protocol) utilisée pour connecter votre d’ordinateur à Internet, vos informations de connexion, le type et la version du navigateur, le fuseau horaire, les types et versions de plug-in de navigateur, le système d’exploitation et la plateforme.
                    </p>
                    <p>
                        Nous recueillons également automatiquement des informations sur la manière dont les visiteurs utilisent notre site Web en utilisant des cookies et des technologies similaires. Il est possible de désactiver les cookies en définissant les préférences de votre navigateur. Pour en savoir plus sur la manière dont nous utilisons les cookies et comment les désactiver, veuillez consulter notre politique en matière de cookies.
                    </p>
                    <p>
                        Certaines des informations personnelles que nous recueillons auprès de vous sont nécessaires pour nous permettre de remplir nos obligations contractuelles envers vous ou envers les autres. D'autres éléments peuvent simplement être nécessaires pour assurer le bon fonctionnement de notre relation.
                    </p>
                    <h4><b>2. COMMENT NOUS UTILISONS VOS INFORMATIONS</b></h4>
                    <p>Nous utilisons vos informations des manières suivantes: <br>
                        veiller à ce que le contenu du site Web soit présenté le plus efficacement possible pour vous;
                        <br>pour nos besoins internes, tels que le contrôle de la qualité, la performance du site Web, l'administration du système et l'évaluation de l'utilisation du site Web, afin que nous puissions vous fournir des services améliorés;
                        <br>vous informer des modifications apportées à nos services;<br>pour vérifier votre âge;<br>vous authentifier lors de la connexion à ce site Web.
                        <br>dans les rares cas où nous cesserions de fournir le site Web, de déplacer et de combiner vos informations personnelles détenues dans nos bases de données relatives au site Web avec celles d'un autre service en ligne similaire ou associé (site Web ou application) que nous ou l'un de nos BAT les entités fonctionnent. Si nous le faisons, nous vous enverrons toujours un e-mail pour vous informer de ces modifications à l'avance.
                        <br>pour vous permettre de participer aux fonctionnalités du site Web, lorsque vous le souhaitez.
                    </p>
                    <p>b. Vérification de ton âge <br>Comme ce site Web concerne les produits du tabac, nous devons nous assurer que les visiteurs ont 18 ans ou plus. Faute de vérification de votre âge, vous ne pourrez pas accéder à ce site. Afin de nous permettre de le faire, nous aurons besoin de certaines informations vous concernant dans notre base de données.</p>
                    <p>Afin de vérifier votre identité, nous pouvons également vous demander de fournir des informations démographiques (telles que le sexe et la date de naissance) et d’autres informations personnelles pouvant être utilisées par nous-mêmes ainsi que par notre prestataire de services pour vérifier vos coordonnées et peuvent inclure votre numéro de passeport ou votre information de conduite. Numéro de licence. Ces informations sont vérifiées par rapport à des sources de données indépendantes sécurisées, afin de faciliter la vérification de votre identité.</p>
                    <p>c. Utilisation d'informations non personnelles <br>
                        Nous pouvons surveiller votre utilisation du site Web et enregistrer votre adresse de courrier électronique et / ou votre adresse IP, votre système d'exploitation et le type de navigateur que vous utilisez pour l'administration du système. Les informations que nous utilisons sont des informations statistiques sur les actions et les habitudes de navigation de nos utilisateurs, qui ne permettent d’identifier aucun individu.
                    </p>
                    <p>
                        Nous collectons des données statistiques agrégées non personnelles sur les visiteurs du site Web et les modèles de trafic. Ces informations n'identifient pas les utilisateurs à titre personnel et nous n'utilisons pas ces informations pour créer des profils d'utilisateurs individuels: elles ne contiennent que des informations générales sur les utilisateurs du site Web.
                    </p>
                    <h4><b>3. NOTRE BASE JURIDIQUE POUR L’UTILISATION DE VOS INFORMATIONS</b></h4>
                    <p>Nous utilisons différentes bases légales pour utiliser vos informations personnelles, à savoir:
                        <br>Consentement - nous comptons sur votre consentement, dans certains cas, pour vous envoyer des communications. Vous pouvez retirer votre consentement à tout moment.
                        <br>Obligation légale - comme expliqué ci-dessus, nous avons l'obligation légale de ne pas avoir de visiteurs sur le site Web âgés de moins de 18 ans. Nous devons effectuer des vérifications de l'âge afin de nous conformer à cette obligation. Nous pouvons également être amenés à utiliser vos informations pour nous conformer à d’autres obligations légales qui s’appliquent à nous.
                        <br>Intérêts légitimes - nous avons un intérêt légitime à utiliser vos informations de la même manière que dans cet avis de confidentialité, par exemple pour améliorer et personnaliser le site Web et pour mener certaines activités d'étude de marché.</p>
                    <h4><b>4. PARTAGE DE VOS INFORMATIONS AVEC DES TIERS</b></h4>
                    <p>Nous pouvons partager vos informations avec l'un des groupes suivants: <br>
                        l'une quelconque de nos entités BAT, lorsque cela est nécessaire et conformément aux lois sur les transferts de données;
                        <br>lorsque nous pensons que la loi ou une autre réglementation nous oblige à partager ces données (par exemple, lors d'un litige anticipé);
                        <br>des fournisseurs de services tiers qui exercent des fonctions pour notre compte; <br>des fournisseurs informatiques externalisés tiers pour lesquels nous disposons d'un accord de traitement de données approprié (ou de protections similaires);
                        <br>si une entité BAT fusionne ou est acquise par une autre entreprise ou société à l'avenir, nous pouvons partager vos informations personnelles avec les nouveaux propriétaires de l'entreprise ou de la société (et vous informer de cette information);
                        <br>si nous devons partager vos informations pour se conformer aux exigences légales ou réglementaires (par exemple, à des fins de vérification de l'âge).<br>
                        Nous pouvons partager les données statistiques agrégées non personnelles sur les visiteurs du site Web avec des tiers à des fins d'analyse et de statistique. br
                    </p>
                    <h4><b>5. O NOUS STOCKONS VOS INFORMATIONS</b></h4>
                    <p>Vos informations personnelles peuvent être transférées hors du Nigéria à des tiers.</p>
                    <p>Nous voulons nous assurer que vos informations personnelles sont stockées et transférées de manière sécurisée. Par conséquent, nous ne transférerons des données hors du Nigéria que si elles sont conformes à la législation sur la protection des données et si les moyens de transfert fournissent des garanties suffisantes en ce qui concerne vos données.</p>
                    <p>Lorsque nous transférons vos informations personnelles hors du Nigéria et que le pays ou le territoire en question ne maintient pas de normes de protection des données adéquates, nous prendrons toutes les mesures raisonnables pour que vos données soient traitées de manière sécurisée et conforme au présent avis de confidentialité. Vous pouvez demander à les voir en nous contactant aux coordonnées ci-dessous.</p>
                    <h4><b>6. COMMENT CONSERVER VOS INFORMATIONS?</b></h4>
                    <p>Nous nous soucions de la protection de vos informations. C'est pourquoi nous avons mis en place des mesures appropriées destinées à empêcher l'accès non autorisé à vos informations personnelles et leur utilisation abusive.</p>
                    <p>Nous nous engageons à prendre toutes les mesures raisonnables et appropriées pour protéger les informations personnelles que nous détenons contre toute utilisation abusive, perte ou accès non autorisé. Pour ce faire, nous mettons en place un éventail de mesures techniques et organisationnelles appropriées, notamment des mesures de cryptage et des plans de reprise après sinistre.</p>
                    <p>Si vous soupçonnez une utilisation abusive, la perte ou un accès non autorisé à vos informations personnelles, veuillez nous en informer immédiatement à propos des informations fournies à la fin du présent avis.
                        Malheureusement, la transmission d'informations via Internet n'est pas complètement sécurisée. Bien que nous appliquions nos procédures normales et respections les exigences légales en matière de protection de vos informations, nous ne pouvons pas garantir la sécurité
                    </p>
                    <h4><b>7. COMBIEN DE TEMPS NOUS GARDONS VOS INFORMATIONS</b></h4>
                    <p>Nous conserverons vos informations relatives aux commandes que vous avez passées auprès de nous conformément à la loi ou à un autre règlement (par exemple, à la suite d'une demande émanant d'une autorité fiscale ou liée à un éventuel litige).</p>

                    <p>Si vous avez ouvert un compte chez nous: nous conserverons vos informations personnelles aussi longtemps que votre compte sera ouvert. Si vous ne souhaitez plus détenir de compte chez nous, vous avez la possibilité de le supprimer. </p>

                    <p>Si vous vous êtes abonné pour recevoir une communication directe de notre part: nous enregistrerons vos informations personnelles sous forme d'abonnement jusqu'à votre désinscription ou indiquerons autrement votre intention d'être supprimées de notre base de données de contacts. </p>

                    <p>Si vous nous avez contacté avec une plainte ou une requête: nous conserverons vos informations personnelles aussi longtemps qu'il sera raisonnablement nécessaire pour résoudre votre plainte ou votre requête.
                        Les exceptions à ce qui précède sont où: <br>
                        nous avons soigneusement examiné la nécessité de conserver vos informations personnelles au-delà des délais décrits ci-dessus pour éventuellement établir, engager ou défendre des poursuites judiciaires ou pour nous conformer à une exigence légale ou réglementaire; <br>

                        nous intentons ou défendons une action en justice ou une autre procédure au cours de la période pendant laquelle nous conservons vos informations personnelles; dans ce cas, nous les conserverons jusqu'à ce que la procédure soit terminée et qu'aucun autre appel ne soit possible; <br>

                        vous exercez votre droit de nous demander de conserver vos informations personnelles pendant une période supérieure à notre période de conservation indiquée; <br>

                        vous exercez votre droit de faire effacer les informations (le cas échéant) et nous n'avons pas besoin de les conserver en relation avec les raisons permises ou requises par la loi; ou<br>

                        dans des cas limités, un tribunal ou un organisme de réglementation nous oblige à conserver vos informations personnelles pendant une période plus ou moins longue. </p>

                    <p>Lorsqu'il ne sera plus nécessaire de conserver vos données, nous supprimerons les informations personnelles que nous détenons sur vous de nos systèmes. Après cette période, nous pouvons regrouper les données (à partir desquelles vous ne pouvez pas être identifié) et les conserver à des fins d'analyse. </p>

                    <h4><b>8. VOS DROITS</b></h4>
                    <p>Vous disposez d'un certain nombre de droits de protection des données mondialement reconnus. </p>

                    <p>Droit d'objection <br>
                        Ce droit vous permet de refuser de traiter vos informations personnelles lorsque nous le faisons pour l'une des raisons suivantes: <br>
                        où nous comptons sur nos intérêts légitimes pour traiter vos informations; <br>
                        nous permettre d'accomplir une tâche dans l'intérêt public ou d'exercer une autorité officielle; <br>
                        dans certaines circonstances, vous envoyer des documents de marketing direct; ou<br>
                        à des fins scientifiques, historiques, de recherche ou statistiques.
                        Sauf pour les raisons pour lesquelles nous sommes certains de pouvoir continuer à traiter vos informations personnelles, nous allons temporairement arrêter de traiter vos informations personnelles conformément à votre objection jusqu'à ce que nous ayons enquêté sur la question. Si nous convenons que votre objection est justifiée conformément à vos droits en vertu des lois sur la protection des données, nous cesserons définitivement d'utiliser vos données à ces fins. Sinon, nous vous expliquerons pourquoi nous devons continuer à utiliser vos données. </p>

                    <p>b. Droit de retirer son consentement</p>
                    <p>Si nous avons obtenu votre consentement pour traiter vos informations personnelles dans le cadre de certaines activités, vous pouvez le retirer à tout moment et nous cesserons d'utiliser vos données à cette fin, à moins que nous estimions qu'il existe une base légale alternative pour justifier le traitement continu de vos données. vos données à cet effet, auquel cas nous vous informerons de cette condition. Si vous retirez votre consentement, notre utilisation de vos informations personnelles avant votre retrait est toujours légale. <p>

                    <p>c. Droit d'accès (demandes d'accès aux personnes concernées) </p>
                    <p>Vous pouvez nous demander à tout moment une copie des informations que nous détenons sur vous et nous demander de modifier, mettre à jour ou supprimer ces informations. Si nous vous fournissons un accès aux informations que nous détenons sur vous, nous ne vous facturerons pas cela sauf si la loi le permet. Si vous nous demandez des copies supplémentaires de ces informations, nous pouvons vous facturer des frais administratifs raisonnables. Si nous sommes légalement autorisés à le faire, nous pouvons refuser votre demande. Si nous refusons votre demande, nous vous en indiquerons toujours les raisons. </p>

                    <p>e. Droit de restreindre le traitement</p>
                    <p>Vous avez le droit de demander que nous limitions le traitement de vos informations personnelles dans certaines circonstances, par exemple si vous contestez l'exactitude des informations personnelles que nous détenons sur vous, vous vous opposez au traitement de vos informations personnelles par nos intérêts légitimes ou vous avez besoin de le garder dans le cadre d'une procédure judiciaire. Si nous avons partagé vos informations personnelles avec des tiers, nous les informerons du traitement limité, à moins que cela ne soit impossible ou implique un effort disproportionné. Bien entendu, nous vous informerons avant de lever toute restriction concernant le traitement de vos informations personnelles. </p>

                    <p>Nous ne pouvons traiter vos informations alors que leur traitement est restreint si nous avons votre consentement ou si nous sommes légalement autorisés à le faire, par exemple à des fins de conservation, pour protéger les droits d'un autre individu ou entreprise ou dans le cadre d'une procédure judiciaire. </p>

                    <p>F. Droit de rectification</p>
                    <p>Vous avez le droit de demander que nous rectifions toute information personnelle inexacte ou incomplète que nous détenons à votre sujet. </p>

                    <h4><b>9. MODIFICATIONS</b></h4>
                        <p>Nous pouvons apporter des modifications à cet avis de confidentialité à tout moment en publiant une copie de l'avis modifié sur le site Web ou, le cas échéant, en vous envoyant un courrier électronique avec cet avis. Toute modification prendra effet 7 jours après la date de notre courrier électronique ou la date à laquelle nous publions les conditions modifiées sur le site Web, selon la date la plus proche. <p>

                        <h4><b>10. COMMENT NOUS CONTACTER</b></h4>
                        <p>Si vous avez des questions au sujet de cet avis de confidentialité, y compris de vos droits en ce qui concerne vos informations personnelles, veuillez contacter le service des affaires générales par e-mail à l'adresse batna_feedback@bat.com ou par courrier à l'adresse suivante: <br>
                            British American Tobacco Marketing Nigéria Limité
                            2 route d'Olumegbon, Ikoyi, Lagos. </p>

                        <p>Lorsque vous nous contactez par courrier électronique ou postal, veuillez utiliser la rubrique "Requête relative à la protection des données" afin que nous puissions adresser votre requête au service concerné et la traiter rapidement. <p>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-check"></i> J'accepte</button>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>



    <div class="m-login__head">
        <h3 class="m-login__title">
            Se connecter
        </h3>
    </div>
    
    @if(Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error')}}
        </div>
    @endif

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success')}}
        </div>
    @endif
    
    <form class="m-login__form m-form" action="{{ route('loginme') }}" method="POST">
        @csrf
        <div class="row form-group m-form__group">
            <select class="col-md-3 form-control m-input" name="indice" id="indice" style="padding: 12px;height: 60px;">
                <option>Indicatif</option> 
                <option value="+225">+225</option>
                <option value="+234">+234</option>
            </select>
            <input class="col-md-8 form-control{{ $errors->has('email') ? ' is-invalid' : '' }} m-input" type="number" placeholder="Contact" name="email" value="{{ old('email') }}" required autofocus style="margin-left: 5px">
            <label class="col-md-12 labeLogin">Entrez votre contact</label>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>

        <!--<div class="form-group m-form__group">-->
        <!--    <input class="form-control m-input m-login__form-input--last" type="date" placeholder="Date de naissance" name="password">-->
        <!--    <label class="col-md-12 labeLogin">Entrez votre date de naissance</label>-->
        <!--</div>-->
        
        <div class="row form-group m-form__group">
            <input class="col-md-4 form-control m-input" type="number" placeholder="Jour" name="jour">
            <input class="col-md-3 form-control m-input" type="number" placeholder="Mois" name="mois">
            <input class="col-md-4 form-control m-input" type="number" placeholder="Annee" name="password">
            <label class="col-md-12 labeLogin">Entrez votre date de naissance</label>
        </div>

        {{--<div class="form-group m-form__group">
            <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} m-input m-login__form-input--last" type="password" placeholder="Mot de passe" name="password">
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>--}}

        <div class="row m-login__form-sub">
            <div class="col m--align-left m-login__form-left">
                <label class="m-checkbox  m-checkbox--light">
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    Se souvenir de moi
                    <span></span>
                </label>
            </div>
        </div>
        <div class="row m-login__form-sub">
            <div class="col m--align-left m-login__form-left">
                <label class="m-checkbox  m-checkbox--light">
                    <input type="checkbox" checked name="terme" id="terme" {{ old('terme') ? 'checked' : '' }} required>
                    J'accepte les <a href="#" data-toggle="modal" data-target="#termsModal">Termes et Conditions</a> et la
                    <a href="#" data-toggle="modal" data-target="#termsModal2">politique de confidentialité</a>
                    <span></span>
                </label>
            </div>
            {{--<div class="col-md-12">
                <label class="m-checkbox  m-checkbox--light">
                    <a href="#" data-toggle="modal" data-target="#termsModal">Termes et Conditions</a>
                </label>
                <label class="m-checkbox  m-checkbox--light">
                    <a href="#" data-toggle="modal" data-target="#termsModal2">Avis de confidentialité</a>
                </label>
            </div>--}}
        </div>
        <div class="m-login__form-action">
            <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
                Connexion
            </button>
        </div>
    </form>
@endsection
