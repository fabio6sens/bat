@extends('layouts.appfront')

@section('title')
    Mon compte
    @parent
@stop

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('bundles/dropify.css')}}">
@endsection

@section('content')

    <div id="compte" class="gallery layout layout__gallery-screen faq">
        <div class="container">
            <h2 class="layout__section-header layout__section-header--decorated">Mon compte</h2>

            @include('frontend/includes/errorsuccess')

            <div class="row">
                <div class="blog-main">
                    {{--<div class="col-md-3 col-sm-3 col-xs-12 page-sidebar">
                        <aside>
                            <div class="inner-box">
                                <div class="user-panel-sidebar">
                                    <div class="collapse-box">
                                        <div class="panel-collapse collapse show" id="MyClassified">
                                            <ul class="acc-list">
                                                <li><a class="active" href=""><i class="fa fa-home"></i> MON COMPTE </a></li>
                                            </ul>
                                        </div>
                                    </div>


                                    <div class="collapse-box">
                                        <h5 class="collapse-title"> Déconnexion <a href="#TerminateAccount" aria-expanded="true" data-toggle="collapse" class="pull-right"></a></h5>

                                        <div class="panel-collapse collapse show" id="TerminateAccount">
                                            <ul class="acc-list">
                                                <li><a href="http://localhost/ecowash/logout" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Me déconnecter </a></li>
                                                <form id="logout-form" action="" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- /.collapse-box  -->
                                </div>
                            </div>
                            <!-- /.inner-box  -->

                        </aside>
                    </div>--}}
                    <div class="col-md-12 col-sm-12 col-xs-12 page-content">
                        <div class="inner-box">
                            <div class="row">
                                <div class="col-md-5 col-xs-4 col-xs-12">
                                    <h3 class="no-padding text-center-480 useradmin">
                                        <a href="#">
                                            <img class="userImg" src="{{ Auth::user()->img  ? url('userAvatar/'.Auth::user()->img.'') : url('img/logo.png') }}" alt="user image" width="64" height="64">
                                            {{Auth::user()->name}}
                                        </a>
                                    </h3>
                                </div>
                            </div>
                        </div>

                        <div class="inner-box">
                            {{--<div class="welcome-msg">
                                <h3 class="page-sub-header2 clearfix no-padding"><a href="#">Bienvenue Test Test</a></h3>
                            </div>--}}

                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default active">
                                    <!-- Single Faq -->
                                    <div class="faq-heading" id="FaqTitle1">
                                        <h4 class="faq-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#faq1" aria-expanded="true" class=""> Modifier mon profil</a>
                                        </h4>
                                    </div>
                                    <div id="faq1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="FaqTitle1" aria-expanded="true" style="">
                                        <div class="faq-body">
                                            <form class="form-horizontal" role="form" method="POST" action="{{route('userUpdate')}}">
                                                @csrf
                                                <div class="form-group">
                                                    <label id="nom" class="col-sm-3 control-label">Nom&Prénom</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="nom" value="{{Auth::user()->name}}" readonly>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Sexe</label>
                                                    <div class="col-sm-9">
                                                        <select class="form-control" id="sexe" required="" readonly>
                                                            <option value="H" {{Auth::user()->genre=='H' ? 'selected' : ''}}>Homme</option>
                                                            <option value="F" {{Auth::user()->genre=='F' ? 'selected' : ''}}>Femme</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="contact" class="col-sm-3 control-label">Contact</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="contact" name="contact" value="{{Auth::user()->telephone}}">
                                                    </div>
                                                </div>
                                                {{--<div class="form-group">
                                                    <label for="adresse" class="col-sm-3 control-label">Adresse</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="adresse" name="adresse" value="01 bp 123 abidjan 01">
                                                    </div>
                                                </div>--}}

                                                <div class="form-group">
                                                    <div class="col-sm-offset-3 col-sm-9">
                                                        <button type="submit" class="btn btn-danger"> Mise à jour</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!--/ End Single Faq -->
                                </div>
                                <!--<div class="panel panel-default">-->
                                    <!-- Single Faq -->
                                <!--    <div class="faq-heading" id="FaqTitle2">-->
                                <!--        <h4 class="faq-title">-->
                                <!--            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#faq2" aria-expanded="false"> Changer de mot de passe </a>-->
                                <!--        </h4>-->
                                <!--    </div>-->
                                <!--    <div id="faq2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="FaqTitle2" aria-expanded="false" style="height: 0px;">-->
                                <!--        <div class="faq-body">-->
                                <!--            <form class="form-horizontal" action="{{route('userUpdatePass')}}" role="form" method="POST">-->
                                <!--                @csrf-->
                                <!--                <div class="form-group">-->
                                <!--                    <label class="col-sm-3 control-label">Mot de passe actuel</label>-->
                                <!--                    <div class="col-sm-9">-->
                                <!--                        <input class="form-control" name="motpass" required="required" placeholder="Mot de passe actuel" type="password">-->
                                <!--                    </div>-->
                                <!--                </div>-->
                                <!--                <div class="form-group">-->
                                <!--                    <label class="col-sm-3 control-label">Nouveau mot de passe</label>-->
                                <!--                    <div class="col-sm-9">-->
                                <!--                        <input class="form-control" name="newpass" required="required" placeholder="Nouveau mot de passe" type="password">-->
                                <!--                    </div>-->
                                <!--                </div>-->
                                <!--                <div class="form-group">-->
                                <!--                    <label class="col-sm-3 control-label">Confirmer mot de passe</label>-->
                                <!--                    <div class="col-sm-9">-->
                                <!--                        <input class="form-control" name="confirmpass" required="required" placeholder="Confirmer mot de passe" type="password">-->
                                <!--                    </div>-->
                                <!--                </div>-->
                                <!--                <div class="form-group">-->
                                <!--                    <div class="col-sm-offset-3 col-sm-9">-->
                                <!--                        <button type="submit" class="btn btn-danger"> Mise à jour</button>-->
                                <!--                    </div>-->
                                <!--                </div>-->
                                <!--            </form>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                    <!--/ End Single Faq -->
                                <!--</div>-->
                                <div class="panel panel-default">
                                    <!-- Single Faq -->
                                    <div class="faq-heading" id="FaqTitle3">
                                        <h4 class="faq-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#faq3" aria-expanded="false">Mon Avatar</a>
                                        </h4>
                                    </div>
                                    <div id="faq3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="FaqTitle3" aria-expanded="false" style="height: 0px;">
                                        <div class="faq-body">
                                            <form class="clearfix" action="{{route('userUpdateAvatar')}}" method="POST" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-4">
                                                            <div class="thumbnail">
                                                                <img id="output" class="img-responsive" src="{{ Auth::user()->img  ? url('userAvatar/'.Auth::user()->img.'') : url('img/logo.png') }}" alt="Image de profil" width="460" height="700" /> {{--{{ Auth::user()->img  ? url('images/userAvatar/'.Auth::user()->img.'') : url('images/user.jpg') }}--}}
                                                            </div>
                                                        </div>

                                                        <div class="col-md-9 col-sm-8">
                                                            <div class="sky-form nomargin">
                                                                {{--<label class="labelUpload">Selectionner une image</label>--}}
                                                                <label for="file" class="input input-file">
                                                                    <div class="button">
                                                                        Selectionnez une image<input type="file" accept='image/*' id="file" name="fileUser" onchange="loadFile(event)">
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="margiv-top10">
                                                    @csrf
                                                    <button type="submit" name="imgProfil" class="btn btn-danger"><i class="fa fa-check"></i> Enregistrer </button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                    <!--/ End Single Faq -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@section('footer_scripts')
    <script type="text/javascript">
        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };
    </script>
@endsection