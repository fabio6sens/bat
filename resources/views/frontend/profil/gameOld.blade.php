@extends('layouts.appfront')

@section('title')
    Game
    @parent
@stop

@section('header_styles')
    <link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="{{ asset('assetsfront/sweetalert/sweetalert.css')}}">
    {{--<link rel="stylesheet" href="{{ asset('game/css/style.css')}}">--}}
    <style>
        .toast{
            opacity: 0!important;
        }
    </style>
@endsection

@section('content')
    <div class="js-header-background" style="display:none"></div>
    <div id="game" class="gallery layout layout__gallery-screen">
        <div class="container">
            <h2 class="layout__section-header layout__section-header--decorated">Game</h2>
            @include('frontend/includes/errorsuccess')
            <div class="row">
                <div class="blog-main">

                    <div class="col-md-12 col-sm-12 col-xs-12 page-content">
                        <div class="inner-box">
                            <div id="slipgame" class="row">
                                    <button class="spinBtn">CLIQUER POUR FAIRE TOURNER !</button>
                                    <div class="wheelContainer">
                                        <svg class="wheelSVG" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" text-rendering="optimizeSpeed">
                                            <defs>
                                                <filter id="shadow" x="-100%" y="-100%" width="550%" height="550%">
                                                    <feOffset in="SourceAlpha" dx="0" dy="0" result="offsetOut"></feOffset>
                                                    <feGaussianBlur stdDeviation="9" in="offsetOut" result="drop" />
                                                    <feColorMatrix in="drop" result="color-out" type="matrix" values="0 0 0 0   0 0 0 0 0   0 0 0 0 0   0 0 0 0 .3 0" />
                                                    <feBlend in="SourceGraphic" in2="color-out" mode="normal" />
                                                </filter>
                                            </defs>
                                            <g class="mainContainer">
                                                <g class="wheel">
                                                    <!-- <image  xlink:href="http://example.com/images/wheel_graphic.png" x="0%" y="0%" height="100%" width="100%"></image> -->
                                                </g>
                                            </g>
                                            <g class="centerCircle" />
                                            <g class="wheelOutline" />
                                            <g class="pegContainer" opacity="1">
                                                <path class="peg" fill="#EEEEEE" d="M22.139,0C5.623,0-1.523,15.572,0.269,27.037c3.392,21.707,21.87,42.232,21.87,42.232 s18.478-20.525,21.87-42.232C45.801,15.572,38.623,0,22.139,0z" />
                                            </g>
                                            <g class="valueContainer" />
                                        </svg>
                                        <div class="toast">
                                            <p/>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@section('footer_scripts')

    <script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/utils/Draggable.min.js'></script>
    <script src='{{ asset('game/js/ThrowPropsPlugin.min.js')}}'></script>
    <script src='{{ asset('game/js/Spin2WinWheel.js')}}'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/TextPlugin.min.js'></script>
    <script src="{{ asset('game/js/index.js')}}"></script>
    <script src="{{ asset('assetsfront/sweetalert/sweetalert.min.js')}}"></script>
@endsection