@extends('layouts.appfront')

@section('title')
    Video
    @parent
@stop

@section('header_styles')
@endsection

@section('content')
    <div class="js-header-background" style="display:none"></div>
    <div id="gallery-screen" class="gallery layout layout__gallery-screen">
        <div class="container">
            <h2 class="layout__section-header layout__section-header--decorated">Video</h2>
            @if(isset($videos))
            @foreach($videos as $video)
                <iframe width="100%" height="600" style="margin-bottom: 25px;" src="https://www.youtube.com/embed/{{$video->url}}?autoplay=0&showinfo=0&controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                {{--<div id="video-screen" class="layout layout__video-screen">
                    <div class="video-block">
                        <figure class="video-block__video-wrapper">
                            <!-- poster="pics/video-placeholder-fest-dark.jpg" -->
                            <video class="video-block__video" id="video" preload="none"  width="100%" height="100%">
                                <source src="{{$video->url}}" type="video/mp4">
                            </video>
                        </figure>
                        <button class="video-block__play js-video__play"></button>
                        <div id="text-poster" class="video-block__poster js-video-block-poster  wow fadeIn">
                            <h2 class="video-block__heading">{{$video->title}}</h2>
                            --}}{{--<h3 class="video-block__band">unknow artist</h3>
                            <p class="video-block__subheading">(See the video fom last concert)</p>--}}{{--
                            --}}{{--<p class="video-block__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>--}}{{--
                        </div>
                    </div>
                </div>--}}
            <br>
            @endforeach
            @endif

        </div>
    </div>

@endsection



@section('footer_scripts')
@endsection