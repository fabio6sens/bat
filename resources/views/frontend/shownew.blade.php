@extends('layouts.appfront')

@section('title')
    {{$itemactu->title}}
    @parent
@stop

@section('header_styles')
    <style>
        p {
            margin: 0 0 10px!important;
        }
        fieldset{
            margin-top: 10px!important;
            margin-bottom: 30px!important;
        }
        form textarea{
            display: block;
            width: 100%;
            padding: .65rem 1.25rem;
            font-size: 1rem;
            line-height: 1.25;
            color: #495057;
            background-color: transparent;
            border: 1px solid #b7b7b7;
            background-image: none;
            background-clip: padding-box;
            border-radius: .25rem;
            font-size: 16px!important;
            color: inherit!important;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        form input{
            width: 100%!important;
        }
        
    </style>
@endsection

@section('content')

    <div id="schedule-screen" class="schedule layout__gallery-screen" >
        <div class="container">
            @include('frontend/includes/errorsuccess')
            
            <h2 class="layout__section-header layout__section-header--decorated">{{$itemactu->title}}</h2>
            <div class="row">
                <!-- image -->
                <div class="col-xs-12 col-md-12">
                    <figure class="schedule__image-block schedule__image-block--left wow fadeInLeft showactu-mob-me" style="margin-bottom: 50px;">
                        <img src="{{url('frontarticles/'.$itemactu->imgactu)}}" alt="image actualite" class="img-responsive">
                    </figure>
                </div>
                
                <div class="col-xs-12 col-md-12">
                    <div style="padding-left: 70px;margin-bottom: 25px;">
                        @if($likUser==0)
                            <a href="{{route('likenew',$itemactu->id)}}" class="button__standart">
                                <i class="fa fa-heart"></i>
                                <span>J'aime</span> {{$countliknew}}
                            </a>
                        @else
                            <a href="{{route('likenonew',$itemactu->id)}}" class="button__standart">
                                <i class="fa fa-heart"></i>
                                <span>Je n'aime plus</span>
                            </a>
                        @endif
                    </div>
                </div>

                <!-- text -->
                <div class="col-xs-12 col-md-12">
                    <div class="schedule__text-block schedule__text-block--right" style="padding-top: 0;">
                        <ul class="schedule__list">
                            <li class="schedule__event">
                                <!--<p class="schedule__event-heading"><span class="dateNews">{{ date("d",strtotime($itemactu->created_at))}}/{{ date("m",strtotime($itemactu->created_at))}}</span><span><time class="schedule__event-time">{{ date("h:i",strtotime($itemactu->created_at))}}</time></p>-->
                                <span class="schedule__event-text">{!! $itemactu->des !!}</span>
                            </li>
                        </ul>
                    </div>
                </div>
                
                
                {{--message for admin--}}
                <div class="schedule__text-block--right" id="btnMSG" >
                <a onclick="sendMSG()" class="button button__standart">ENVOYER UN COMMENTAIRE A L'ADMINISTRATION</a>
                </div>
                {{--formulaire de contact--}}
                <div class="schedule__text-block--right page-footer__form col-xs-12 col-md-8" id="formMSG">
                    <form action="{{route('messadmin')}}" method="POST">
                        @csrf
                        {{--<fieldset>
                            <label for="name">Nom</label>
                            <input type="text" id="name" placeholder="Entrez votre nom" value="" autocomplete="off" required="">
                        </fieldset>
                        <fieldset>
                            <label for="email">Type your email here</label>
                            <input type="text" id="email" placeholder="Entrez votre email" value="" autocomplete="off" required="">
                        </fieldset>--}}
                        <fieldset>
                            <label for="msg">Message</label>
                            <textarea name="msg" id="msg" cols="30" rows="10" placeholder="Message" required></textarea>
                        </fieldset>
                        <fieldset>
                            <input type="hidden" name="slug" value="{{$itemactu->id}}">
                            <input type="submit" value="ENVOYER" aria-label="Submit">
                        </fieldset>
                    </form>
                </div>
                
            </div>
        </div>
    </div>

@endsection



@section('footer_scripts')
    <script>
        (function($){
            $('#formMSG').hide();
        }(jQuery))

        function sendMSG(){
            //alert("texte");
            $('#formMSG').show(1000);
            $('#btnMSG').hide(1000);
        }

    </script>
@endsection