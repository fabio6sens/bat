<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>Creer votre mot de passe </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://cdn.bootcss.com/webfont/1.6.16/webfontloader.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <!--begin::Page Vendors -->
    <link href="{{asset('bundles/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors -->
    <link href="{{asset('bundles/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('bundles/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/mystyle.css') }}">
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{asset('img/logo.png')}}" />

</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url({{asset('bundles/app/media/img//bg/bg-2.jpg')}});">
        <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="{{route('frontend')}}">
                        <img src="{{asset('img/logo.png')}}" style="width:96px;height:65px;">
                    </a>
                </div>

                @include('frontend/includes/errorsuccess')

                <div class="m-login__signin">
                    <div class="m-login__head">
                        <h3 class="m-login__title"> Créer votre mot de passe </h3>
                    </div>
                    <form class="m-login__form m-form" method="POST" action="{{route('savecreatpass')}}">
                        @csrf

                        <div class="form-group m-form__group">
                            <input id="email" class="form-control m-input m-login__form-input--last" type="text" value="{{$userExit->email}}" readonly>
                        </div>

                        <div class="form-group m-form__group">
                            <input id="password" name="password" class="form-control m-input m-login__form-input--last" type="password" placeholder="Mot de passe" required>
                        </div>

                        <div class="form-group m-form__group">
                            <input id="password_confirmation" name="password_confirmation" class="form-control m-input m-login__form-input--last" type="password" placeholder="Confirmer le mot de passe" required>
                        </div>

                        <div class="m-login__form-action">
                            <input type="hidden" name="slug" value="{{$userExit->id}}">
                            <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
                                Enregistrer
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->

<script src="{{asset('bundles/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('bundles/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('bundles/snippets/pages/user/login.js')}}" type="text/javascript"></script>
</body>
</html>
