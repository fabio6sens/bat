@extends('layouts.appfront')

@section('title')
    Actualités
    @parent
@stop

@section('header_styles')
@endsection

@section('content')
    <div class="js-header-background" style="display:none"></div>
    <div id="gallery-screen" class="gallery   layout layout__gallery-screen">
        <div class="container">
            <h2 class="layout__section-header layout__section-header--decorated">Actualites</h2>
            <div class="row">
                @foreach($allactus as $allactu)
                <div class="col-md-6 col-xs-12">
                    <div class="row">
                        <!-- image -->
                        <div class="col-xs-12 col-md-12">
                            <figure class="schedule__image-block schedule__image-block--left wow fadeInLeft" style="margin-bottom: 40px!important;">
                                <a href="{{route('showNews',$allactu->slug)}}"><img src="{{url('frontarticles/'.$allactu->imgactu)}}" alt="image actualite" class="img-responsive"/></a>
                                <figcaption>{{ date("d",strtotime($allactu->created_at))}}/<span>{{ date("m",strtotime($allactu->created_at))}}</span></figcaption>
                            </figure>
                        </div>
                        <!-- text -->
                        <div class="col-xs-12 col-md-12">
                            <div class="schedule__text-block schedule__text-block--right" style="padding-top: 10px">
                                <ul class="schedule__list">
                                    <li class="schedule__event">
                                        <p class="schedule__event-heading"><!--<time class="schedule__event-time">{{ date("h:i",strtotime($allactu->created_at))}}</time>--><a class="titleActu" href="{{route('showNews',$allactu->slug)}}">{{$allactu->title}}</a></p>
                                        <p class="schedule__event-text">{{$allactu->minidesc}}</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection



@section('footer_scripts')
@endsection