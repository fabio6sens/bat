@extends('layouts.appfront')

@section('title')
    Galerie
    @parent
@stop

@section('header_styles')
@endsection

@section('content')
    <div class="js-header-background" style="display:none"></div>
    <div id="gallery-screen" class="gallery   layout layout__gallery-screen">
        <div class="container">
            <h2 class="layout__section-header layout__section-header--decorated">Gallerie</h2>
            <div id="lightgallery" class="gallery__container">
                @foreach($galeries as $galerie)
                    <a href="{{ url('frontgalerie/'.$galerie->imgs)}}" class="gallery__item gallery__item--big"><img src="{{ url('frontgalerie/'.$galerie->imgs)}}" alt="" class="gallery__image"></a>
                    {{--<a href="{{ asset('assetsfront/pics/fest-gallery-2.jpg')}}" class="gallery__item gallery__item--small"><img src="{{ asset('assetsfront/pics/fest-gallery-2.jpg')}}" alt="Gallery placeholder" class="gallery__image"></a>
                    <a href="{{ asset('assetsfront/pics/fest-gallery-3.jpg')}}" class="gallery__item gallery__item--small"><img src="{{ asset('assetsfront/pics/fest-gallery-3.jpg')}}" alt="Gallery placeholder" class="gallery__image"></a>
                    <a href="{{ asset('assetsfront/pics/fest-gallery-4.jpg')}}" class="gallery__item gallery__item--small"><img src="{{ asset('assetsfront/pics/fest-gallery-4.jpg')}}" alt="Gallery placeholder" class="gallery__image"></a>
                    <a href="{{ asset('assetsfront/pics/fest-gallery-5.jpg')}}" class="gallery__item gallery__item--small"><img src="{{ asset('assetsfront/pics/fest-gallery-5.jpg')}}" alt="Gallery placeholder" class="gallery__image"></a>
                    <a href="{{ asset('assetsfront/pics/fest-gallery-6.jpg')}}" class="gallery__item gallery__item--big"><img src="{{ asset('assetsfront/pics/fest-gallery-6.jpg')}}" alt="Gallery placeholder" class="gallery__image"></a>--}}
                @endforeach
            </div>
        </div>
    </div>

@endsection



@section('footer_scripts')
@endsection