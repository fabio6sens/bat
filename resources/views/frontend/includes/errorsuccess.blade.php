@if(Session::has('success'))

    <div class="alert-brand-me fade show" role="alert">
        {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>--}}
        {{Session::get('success')}}
    </div>
    <br>
@endif

@if(Session::has('error'))

    <div class="alert-brand-me fade show" role="alert">
        {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>--}}
        {{Session::get('error')}}
    </div>
    <br>
@endif

@if($errors->any())
    <div class="alert-brand-me fade show" role="alert">
        @foreach($errors->all() as $errorr)
            <p>{{ $errorr }}</p>
        @endforeach
    </div>
    <br>
@endif