@extends('layouts.appfront')

@section('title')
    Bienvenue
    @parent
@stop

@section('header_styles')
@endsection

@section('content')
    <div id="about-screen" class="about   layout layout__about-screen">
        <div class="container">
            <h2 class="layout__section-header layout__section-header--decorated">À LA UNE</h2>
            <div class="about   row">
                <div class="col-xs-12 col-md-6  col-lg-5  wow fadeInLeft">
                    {{--<div class="about__timer-wrap">
                        <p class="about__timer-header">Coming very soon!</p>
                        <div id="countdown" class="timer">
                        </div>
                    </div>--}}
                    <figure class="schedule__image-block schedule__image-block--left wow fadeInLeft">
                        <a href="{{route('showNews',$allactus[0]->slug)}}"><img src="{{url('frontarticles/'.$allactus[0]->imgactu)}}" alt="image actualite" class="img-responsive" style=""></a>
                        <figcaption style="color: #fff;">{{ date("d",strtotime($allactus[0]->created_at))}}/<span>{{ date("m",strtotime($allactus[0]->created_at))}}</span></figcaption>
                    </figure>

                    {{--<img src="{{url('frontarticles/'.$allactus[0]->imgactu)}}" alt="image actualite" class="img-responsive">--}}
                </div>
                <div class="col-xs-12 col-md-6 col-lg-7 wow fadeInRight">
                    {{--<time class="schedule__event-time">{{ date("h:i",strtotime($allactus[0]->created_at))}}</time><a href="{{route('showNews',$allactus[0]->slug)}}"><h2 class="schedule__event-heading wow fadeIn" style="margin-left: 42px">{{$allactus[0]->title}}</h2></a>--}}
                    <p class="schedule__event-heading">{{--<time class="schedule__event-time" style="color:#337ab7;">{{ date("h:i",strtotime($allactus[0]->created_at))}}</time>--}}<a href="{{route('showNews',$allactus[0]->slug)}}" style="color: #337ab7">{{$allactus[0]->title}}</a></p>
                    <div class="about__text-wrapper">
                        {{--<p class="about__text--highlighted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>--}}
                        <p class="about__text">{{$allactus[0]->minidesc}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(isset($allactus[1]))
    <div id="schedule-screen" class="schedule   layout layout__schedule-screen">
        <div class="container">
            <h2 class="layout__section-header layout__section-header--decorated">Actualites</h2>
            @if(isset($allactus[1]))
            <div class="row">
                <!-- image -->
                <div class="col-xs-12 col-md-6">
                    <figure class="schedule__image-block schedule__image-block--left wow fadeInLeft">
                        <a href="{{route('showNews',$allactus[1]->slug)}}"><img src="{{url('frontarticles/'.$allactus[1]->imgactu)}}" alt="image actualite" class="img-responsive"></a>
                        <figcaption>{{ date("d",strtotime($allactus[1]->created_at))}}/<span>{{ date("m",strtotime($allactus[1]->created_at))}}</span></figcaption>
                    </figure>
                </div>
                <!-- text -->
                <div class="col-xs-12 col-md-6">
                    <div class="schedule__text-block schedule__text-block--right">
                        <ul class="schedule__list">
                            <li class="schedule__event">
                                <p class="schedule__event-heading">{{--<time class="schedule__event-time">{{ date("h:i",strtotime($allactus[1]->created_at))}}</time>--}}<a href="{{route('showNews',$allactus[1]->slug)}}">{{$allactus[1]->title}}</a></p>

                                <p class="schedule__event-text">{{$allactus[1]->minidesc}}</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            @endif
            @if(isset($allactus[2]))
            <div class="row">
                <!-- image -->
                <div class="col-xs-12 col-md-6 col-md-push-6">
                    <figure class="schedule__image-block schedule__image-block--right wow fadeInRight">
                        <a href="{{route('showNews',$allactus[2]->slug)}}"><img src="{{url('frontarticles/'.$allactus[2]->imgactu)}}" alt="image actualite" class="img-responsive"></a>
                        <figcaption>{{ date("d",strtotime($allactus[2]->created_at))}}/<span>{{ date("m",strtotime($allactus[2]->created_at))}}</span></figcaption>
                    </figure>
                </div>
                <!-- text -->
                <div class="col-xs-12 col-md-6 col-md-pull-6">
                    <div class="schedule__text-block schedule__text-block--left">
                        <ul class="schedule__list">
                            <li class="schedule__event">
                                <p class="schedule__event-heading">{{--<time class="schedule__event-time">{{ date("h:i",strtotime($allactus[2]->created_at))}}</time>--}}<a href="{{route('showNews',$allactus[2]->slug)}}">{{$allactus[2]->title}}</a></p>

                                <p class="schedule__event-text">{{$allactus[2]->minidesc}}</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            @endif
            <div class="gallery__btn"  style="margin-top: 22px;">
                <a href="{{route('frontNews')}}" class="button button__standart">Voir plus</a>
            </div>
        </div>
    </div>
    @endif

    @if(isset($lastVideo))
    <div id="video-screen" class="layout layout__video-screen">
        <iframe width="100%" height="600" style="margin-bottom: 25px;" src="https://www.youtube.com/embed/{{$lastVideo->url}}?autoplay=0&showinfo=0&controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        {{--<div class="video-block">
            <figure class="video-block__video-wrapper">
                <!-- poster="pics/video-placeholder-fest-dark.jpg" -->
                <video class="video-block__video" id="video" preload="none"  width="100%" height="100%">
                    <source src="{{$lastVideo->url}}" type="video/mp4">
                </video>
            </figure>
            <button class="video-block__play js-video__play"></button>
            <div id="text-poster" class="video-block__poster js-video-block-poster  wow fadeIn">
                <h2 class="video-block__heading">{{$lastVideo->title}}</h2>
                --}}{{--<h3 class="video-block__band">unknow artist</h3>
                <p class="video-block__subheading">(See the video fom last concert)</p>--}}{{--
                --}}{{--<p class="video-block__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>--}}{{--
            </div>
        </div>--}}
    </div>
   @endif 
    <!-- <div id="pricing-screen" class="price   layout layout__price-screen">
      <div class="container">
        <h2 class="layout__section-header layout__section-header--decorated">How much it costs?</h2>
        <div class="price__flex   row">
          <div class="price__flex-item   col-xs-12 col-sm-12 col-md-4">
            <p class="price__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
          </div>
          <div class="price__flex-item   col-xs-12 col-sm-6 col-md-4">
            <div class="price__card wow fadeInUp">
              <div class="price__card-inner">
                <div class="price__card-top">
                  <p class="price__card-heading">fan zone</p>
                  <p class="price__card-text">Funny party aroud the main scene with drinks and games</p>
                </div>
                <div class="price__card-bottom">
                  <p class="price__card-price">it’s free</p>
                  <a href="#" class="price__card-btn button button__standart">Register</a>
                </div>
              </div>
            </div>
          </div>
          <div class="price__flex-item   col-xs-12 col-sm-6 col-md-4">
            <div class="price__card wow fadeInDown">
              <div class="price__card-inner">
                <div class="price__card-top">
                  <p class="price__card-heading">exclusive</p>
                  <p class="price__card-text">Funny party on the main scene with drinks and games. Free food and bus transfer  are inclusive!</p>
                </div>
                <div class="price__card-bottom">
                  <p class="price__card-price">100$</p>
                  <a href="#" class="price__card-btn button button__standart">Register</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <div id="gallery-screen" class="gallery   layout layout__gallery-screen">
        <div class="container">
            <h2 class="layout__section-header layout__section-header--decorated">Gallerie</h2>
            <div id="lightgallery" class="gallery__container">
                @foreach($galeries as $galerie)
                <a href="{{ url('frontgalerie/'.$galerie->imgs)}}" class="gallery__item gallery__item--big"><img src="{{ url('frontgalerie/'.$galerie->imgs)}}" alt="" class="gallery__image"></a>
                {{--<a href="{{ asset('assetsfront/pics/fest-gallery-2.jpg')}}" class="gallery__item gallery__item--small"><img src="{{ asset('assetsfront/pics/fest-gallery-2.jpg')}}" alt="Gallery placeholder" class="gallery__image"></a>
                <a href="{{ asset('assetsfront/pics/fest-gallery-3.jpg')}}" class="gallery__item gallery__item--small"><img src="{{ asset('assetsfront/pics/fest-gallery-3.jpg')}}" alt="Gallery placeholder" class="gallery__image"></a>
                <a href="{{ asset('assetsfront/pics/fest-gallery-4.jpg')}}" class="gallery__item gallery__item--small"><img src="{{ asset('assetsfront/pics/fest-gallery-4.jpg')}}" alt="Gallery placeholder" class="gallery__image"></a>
                <a href="{{ asset('assetsfront/pics/fest-gallery-5.jpg')}}" class="gallery__item gallery__item--small"><img src="{{ asset('assetsfront/pics/fest-gallery-5.jpg')}}" alt="Gallery placeholder" class="gallery__image"></a>
                <a href="{{ asset('assetsfront/pics/fest-gallery-6.jpg')}}" class="gallery__item gallery__item--big"><img src="{{ asset('assetsfront/pics/fest-gallery-6.jpg')}}" alt="Gallery placeholder" class="gallery__image"></a>--}}
                @endforeach
            </div>
            <div class="gallery__btn">
                <a href="{{route('frontGalerie')}}" class="button button__standart">Voir plus</a>
            </div>
        </div>
    </div>

    <!-- <div id="contacts-screen" class="layout layout__contacts-screen">
  <div class="container">
    <h2 class="layout__section-header layout__section-header--decorated">Contact us</h2>
  </div>
  <div class="contacts" style="background-image: url(pics/bg-fest-contacts-dark.jpg)">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-6 col-md-push-6">
          <div class="contacts__wrapper wow slideInRight">
            <div class="contacts__item">
              <h4 class="contacts__header">where are we?</h4>
              <p class="contacts__text">Serpukhovskaya ul., 26
              Sankt-Peterburg</p>
              <i class="fa fa-map-signs"></i>
            </div>
            <div class="contacts__item">
              <h4 class="contacts__header">send us a letter</h4>
              <a class="contacts__text" href="mailto:crafty@superevent.co">crafty@superevent.co</a>
              <i class="fa fa-envelope"></i>
            </div>
            <div class="contacts__item">
              <h4 class="contacts__header">Call us</h4>
              <a class="contacts__text" href="tel:+22354675579">+(22) 35467 5579</a>
              <a class="contacts__text" href="tel:+22354675579">+(22) 35467 5579</a>
              <i class="fa fa-phone-square"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="layout layout__map-screen">
  <div id="footer-map"></div>
</div>-->
@endsection



@section('footer_scripts')
@endsection