<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113215970-17"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-113215970-17');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@section('title') | Un Autre Niveau @show </title>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}


    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Oswald:300,400,700" rel="stylesheet">
    <!-- Styles -->
    {{--  <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('assetsfront/components/lightgallery.min.css')}}">
    <!-- endinject -->
    <link rel="stylesheet" href="{{ asset('assetsfront/css/fest-dark.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mystyle.css') }}">
    {{--<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">--}}
    <link rel="shortcut icon" href="{{asset('img/logo.png')}}" />

    @yield('header_styles')
    
    <!-- Start Bloquer click droit HEAD section -->
    <script type="text/javascript">
        function clickExplorer() {
            if( document.all ) {
                //alert('© Copyright 2013');
            }
            return false;
        }
        function clickOther(e) {
            if( document.layers || ( document.getElementById && !document.all ) ) {
                if ( e.which == 2 || e.which == 3 ) {
                    //alert('© Copyright 2013');
                    return false;
                }
            }
        }
        if( document.layers ) {
            document.captureEvents( Event.MOUSEDOWN );
            document.onmousedown=clickOther;
        }
        else {
            document.onmouseup = clickOther;
            document.oncontextmenu = clickExplorer;
        }
    </script>
    <!-- End Bloquer click droit HEAD section -->


</head>
<body>
    @if(!Route::is('game'))
    <header class="page-header js-page-header" >
        @if(Route::is('frontend'))
        <div class="page-header__background js-header-background" data-small="{{ asset('assetsfront/img/bg.jpg') }}" data-large="{{ asset('assetsfront/img/bg.jpg') }}"></div>
        @endif
            <div class="container">
            <div class="row">
                <nav class="navbar">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed page-header__btn js-mobile-menu-btn" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="page-header__brand link-logoh-me" href="{{route('frontend')}}"><img src="{{asset('img/logo.png')}}" class="img-responsive logoh-me" alt="un autre niveau"></a>
                        </div>
    
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse   page-header__nav" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                            @guest
                                <li><a href="{{route('frontNews')}}" class="page-header__link">Actualites</a></li>
                                <li><a href="{{route('frontVideo')}}" class="page-header__link">Video</a></li>
                                <li><a href="{{route('frontGalerie')}}" class="page-header__link">Galerie</a></li>
                                <li><a href="{{route('login')}}" class="page-header__link">Login</a></li>
                                {{--<li><a href="{{route('game')}}" class="page-header__link">Game</a></li>--}}
    
                            @else
                                <li><a href="{{route('frontNews')}}" class="page-header__link">Actualites</a></li>
                                <li><a href="{{route('frontVideo')}}" class="page-header__link">Video</a></li>
                                <li><a href="{{route('frontGalerie')}}" class="page-header__link">Galerie</a></li>
                                @if(Auth::user()->role_id == 1 or Auth::user()->role_id == 2 or Auth::user()->role_id == '')
                                <li><a href="{{route('admin.dashboard')}}" class="page-header__link">Admin</a></li>
                                @else
                                <li><a href="{{route('indexUser')}}" class="page-header__link">Mon compte</a></li>
                                @endif
    
                                <li><a href="{{ route('logout') }}" class="page-header__link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Déconnexion') }}</a></li>
                                <li style="margin-top: -11px;"><a href="https://www.instagram.com/unautreniveau/" target="_blank" class="page-header__link socials__link" style="font-size: 22px;"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                {{--@if(!Request::is('indexUser'))
                                    <li><a href="{{route('frontNews')}}" class="page-header__link">Actualites</a></li>
                                    <li><a href="{{route('frontVideo')}}" class="page-header__link">Video</a></li>
                                    <li><a href="{{route('frontGalerie')}}" class="page-header__link">Galerie</a></li>
                                    <li><a href="{{route('indexUser')}}" class="page-header__link">Mon compte</a></li>
                                @elseif(Request::is('indexUser'))
                                        --}}{{--or Request::is('frontNews')or Request::is('frontVideo')or Request::is('frontGalerie') or Request::is('showNews')--}}{{--
                                        <li><a href="{{route('indexUser')}}" class="page-header__link">Mon compte</a></li>
                                    <li><a href="{{route('frontend')}}" class="page-header__link">Aller au site</a></li>
    
                                    <li><a href="{{ route('logout') }}" class="page-header__link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Déconnexion') }}</a></li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                @endif--}}
                            @endguest
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </div>
        </div>
        @if(Route::is('frontend'))
        <div class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 wow fadeIn">
                        <h1 class="hero__header">un Autre</br>niveau</h1>
                        <p class="hero__subheader">#unautreniveau</p>
                        {{--<a class="button button__hero" href="#">Buy the ticket <!--<i class="fa fa-arrow-circle-o-right"></i>--></a>--}}
                    </div>
                </div>
            </div>
            <div class="hero__decoration-slice"></div>
        </div>
        @endif
    </header>
    @endif
    
    
    @yield('content')
    
    @if(!Route::is('game'))
    <footer class="page-footer">
        <div class="container">
            <!--<div class="row">-->
            <!--    <address class="page-footer__contacts   col-xs-6 col-sm-3">-->
            <!--        <h5 class="page-footer__header">Contactez-nous</h5>-->
            <!--        <p class="page-footer__text">Via Lusitania, 26-34</br>Roma</p>-->
            <!--        <a href="tel:+22354675579">+(22) 35467 5579</a>-->
            <!--        <a href="mailto:event@studio.com">event@studio.com</a>-->
            <!--    </address>-->
            <!--    <div class="page-footer__socials   col-xs-6 col-sm-4 col-sm-push-5">-->
            <!--        <h5 class="page-footer__header">Rejoignez-nous</h5>-->
            <!--        <p class="page-footer__text">Nous sommes heureux de vous voir dans nos communautés! Vous êtes les bienvenus!</p>-->
            <!--        <ul class="socials">-->
            <!--            <li class="socials__item"><a href="#" class="socials__link"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
            <!--        </ul>-->
            <!--    </div>-->
            <!--    <div class="page-footer__form   col-xs-12 col-sm-5 col-sm-pull-4">-->
            <!--        {{--<h5 class="page-footer__header">Abonnez-vous pour de super nouvelles</h5>-->
            <!--        <p class="page-footer__text">Seules des informations fraîches sur notre vie et des articles utiles sur les dernières tendances en matière de décoration d'intérieur.</p>-->
            <!--        <form action="" method="">-->
            <!--            <fieldset>-->
            <!--                <label for="email">Votre email</label>-->
            <!--                <input type="text" id="email" placeholder="Saisir votre email" autocomplete="off" required>-->
            <!--                <input type="submit" value="Envoyer" aria-label="Submit"></input>-->
            <!--            </fieldset>-->
            <!--        </form>--}}-->
            <!--    </div>-->
            <!--</div>-->
            
            
            <div class="row">
                <address class="page-footer__contacts   col-xs-6 col-sm-3 col-md-offset-3">
                    <h5 class="page-footer__header">Contactez-nous</h5>
                    <p class="page-footer__text">Abidjan <br>Côte d'Ivoire</p>
                    <a href="tel:+22566984541">+22566984541</a>
                    <a href="mailto:info@unautreniveau.com">info@unautreniveau.com</a>
                </address>
                <div class="page-footer__socials   col-xs-6 col-sm-4">
                    <h5 class="page-footer__header">Rejoignez-nous</h5>
                    <p class="page-footer__text">Nous sommes heureux de vous voir dans nos communautés! Vous êtes les bienvenus!</p>
                    <ul class="socials">
                        <li class="socials__item"><a href="https://www.instagram.com/unautreniveau/" target="_blank" class="socials__link"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                
            </div>
            <div class="row">
                <div class="page-footer__copyright   col-xs-12">
                    <p>© un autre niveau. Tous droit Réserver</p>
                </div>
            </div>
        </div>
    </footer>
    @endif
{{--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>--}}
<script src="{{ asset('assetsfront/js/audio.js')}}"></script>
<!-- inject:js -->
<script src="{{ asset('assetsfront/components/jquery.min.js')}}"></script>
<script src="{{ asset('assetsfront/components/bootstrap.min.js')}}"></script>
<script src="{{ asset('assetsfront/components/jquery-ui.min.js')}}"></script>
<script src="{{ asset('assetsfront/components/jquery.countdown.js')}}"></script>
<script src="{{ asset('assetsfront/components/lightgallery.min.js')}}"></script>
<script src="{{ asset('assetsfront/components/validator.js')}}"></script>
<script src="{{ asset('assetsfront/js/audio.js')}}"></script>
<script src="{{ asset('assetsfront/js/scripts.js')}}"></script>
<!-- <script src="{{ asset('assetsfront/js/switcher.js')}}"></script> -->
<script src="{{ asset('assetsfront/js/wow.min.js')}}"></script>
<!-- endinject -->

@yield('footer_scripts')
</body>
</html>
