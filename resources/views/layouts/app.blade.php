<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        Dashboard BAT | Login
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <!-- <script src="https://cdn.bootcss.com/webfont/1.6.16/webfontloader.js"></script> -->
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{asset('bundles/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('bundles/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .modal-short {
            max-height: 400px;
            overflow: auto;
        }
        .labeLogin{
            display: none;
        }
        .txtfoot{
            margin-left: 21%;
        }
        .imgbtn{
                width: 5%;
            }
        .pfoot2{
            color: #fff;text-align: center;font-size: 20px;margin-top: 45px;text-transform: uppercase; font-weight: bold;
        }
            
        @media  screen and (max-width: 767px) {
            .labeLogin{
                display: block!important;font-size: 17px!important; color: white!important; opacity: 0.7;
            }
            .txtfoot{
                margin-left: inherit;
                margin-top : 10px;
            }
            .imgbtn{
                width: 8%;
            }
            .pfoot2{
                color: #fff;text-align: center;font-size: 12px;
            }
        }
    </style>
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{asset('img/logo.png')}}" />
    <!-- Start Bloquer click droit HEAD section -->
    <script type="text/javascript">
        function clickExplorer() {
            if( document.all ) {
                //alert('© Copyright 2013');
            }
            return false;
        }
        function clickOther(e) {
            if( document.layers || ( document.getElementById && !document.all ) ) {
                if ( e.which == 2 || e.which == 3 ) {
                    //alert('© Copyright 2013');
                    return false;
                }
            }
        }
        if( document.layers ) {
            document.captureEvents( Event.MOUSEDOWN );
            document.onmousedown=clickOther;
        }
        else {
            document.onmouseup = clickOther;
            document.oncontextmenu = clickExplorer;
        }
    </script>
    <!-- End Bloquer click droit HEAD section -->
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url({{asset('bundles/app/media/img//bg/bg-2.jpg')}});">
        <div class="m-grid__item m-grid__item--fluid    m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <!--<a href="{{route('frontend')}}">-->
                    <!--    <img src="{{asset('img/logo.png')}}" style="width:96px;height:65px;">-->
                    <!--</a>-->
                </div>
                <div class="m-login__signin">
                    @yield('content')
                </div>
            </div>
          
            <div class="col-md-7 col-xs-12 txtfoot">
               <p style="color: #fff;text-align: center;font-size: 12px;">En accédant à ce site, vous avez confirmé que vous avez au moins 18 ans et que vous avez consenti à recevoir des communications relatives au tabac. Vous acceptez également les conditions d'utilisation et la politique de confidentialité de ce site. Par la présente, British American Tobacco est exclue de toute responsabilité s'il est constaté que l’une des informations fournies par vous est erronée et si un recours en justice est proposé à l’égard des risques ou de la responsabilité engendrés par de telles informations erronées.</p>
               <p class="pfoot2">
                	<img class="imgbtn" src="{{asset('assetsfront/img/foot.png')}}"> Abus dangereux pour la santé 
                </p>
            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->
<!--begin::Base Scripts -->
<script src="{{asset('bundles/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('bundles/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
<!--end::Base Scripts -->
<!--begin::Page Snippets -->
<!-- <script src="{{asset('bundles/snippets/pages/user/login.js')}}" type="text/javascript"></script> -->
<!--end::Page Snippets -->
</body>
<!-- end::Body -->
</html>
