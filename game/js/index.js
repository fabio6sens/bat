//Usage

//load your JSON (you could jQuery if you prefer)
function loadJSON(callback) {

  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', './game/wheel_data.json', true);
  xobj.onreadystatechange = function() {
    if (xobj.readyState == 4 && xobj.status == "200") {
      //Call the anonymous function (callback) passing in the response
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

//your own function to capture the spin results
function myResult(e) {
  //e is the result object
    console.log('Spin Count: ' + e.spinCount + ' - ' + 'Win: ' + e.win + ' - ' + 'Message: ' +  e.msg);

    // if you have defined a userData object...
    if(e.userData){
      if(e.userData.score== 1000000){

        //$('#slipgame').hide();
          $.ajax
          ({
              type: "get",
              url: "savewin",
              timeout: 10000,
              data: "status=win",
              success: function (response) {
                  var rep = JSON.parse(response);
                  //var nbdata = rep.data.length;
                  if(rep.type == 1){
                      swal("Félicitation ", "vous avez gagnez", "success")
                      //alert('Félicitation vous avez gagnez');
                      $('#slipgame').hide();
                  }
              },

          });

        //var nameclass = document.getElementById('slipgame').eq(0);
        //nameclass.hide();
      }else{
          console.log("perdu");
          if(e.spinCount == 3){
              swal("Oup !! ", 'vous avez perdu', "error")
          }
          var nbtour = 3 - (e.spinCount+1);
          swal("Oup !! ", 'vous avez perdu, il vous reste '+nbtour+' tours', "error")
      }
      console.log('User defined score: ' + e.userData.score)

    }

  if(e.spinCount == 3){
    //show the game progress when the spinCount is 3
    console.log(e.target.getGameProgress());
    //restart it if you like
    //e.target.restart();
  }

}

//your own function to capture any errors
function myError(e) {
  //e is error object
  console.log('Spin Count: ' + e.spinCount + ' - ' + 'Message: ' +  e.msg);

}

function myGameEnd(e) {
  
  //e is gameResultsArray
  console.log(e);
  //alert(e);
  TweenMax.delayedCall(5, function(){
    
    Spin2WinWheel.reset();

  })


}

function init() {
  loadJSON(function(response) {
    // Parse JSON string to an object
    var jsonData = JSON.parse(response);
    //if you want to spin it using your own button, then create a reference and pass it in as spinTrigger
    var mySpinBtn = document.querySelector('.spinBtn');
    //create a new instance of Spin2Win Wheel and pass in the vars object
    var myWheel = new Spin2WinWheel();
    
    //WITH your own button
    myWheel.init({data:jsonData, onResult:myResult, onGameEnd:myGameEnd, onError:myError, spinTrigger:mySpinBtn});
    
    //WITHOUT your own button
    //myWheel.init({data:jsonData, onResult:myResult, onGameEnd:myGameEnd, onError:myError});
  });
}



//And finally call it
init();
