<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('mobile/initDevice', 'MobileController@initDevice')->middleware('cors');
Route::post('mobile/saveUser', 'MobileController@saveUser')->middleware('cors');
Route::post('mobile/saveUsers', 'MobileController@saveUsers')->middleware('cors');
Route::get('mobile/getUsers/{device}/{page?}', 'MobileController@getUsers')->middleware('cors');
Route::get('mobile/deleteUser/{id}', 'MobileController@deleteUser')->middleware('cors');
