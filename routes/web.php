<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Frontend\FrontendController@index')->name('frontend');
Route::get('/galerie', 'Frontend\FrontendController@indexGalerie')->name('frontGalerie');
Route::get('/actualites', 'Frontend\FrontendController@indexNews')->name('frontNews');
Route::get('/actualites/{slug}', 'Frontend\FrontendController@showNews')->name('showNews');
Route::get('/videos', 'Frontend\FrontendController@indexVideo')->name('frontVideo');

Route::get('/created-pass/{token}', 'HomeController@creatpass')->name('creatpass');
Route::post('/save-pass/', 'HomeController@savecreatpass')->name('savecreatpass');
//Route::get('/get-game/{token}', 'Frontend\FrontendController@getgame')->name('getgame');
Route::get('/getgame/{id}', 'MobileController@getgame')->name('getgame');

Route::get('/mon-compte', 'Frontend\FrontendUsersController@index')->name('indexUser');
Route::post('/mon-compte', 'Frontend\FrontendUsersController@update')->name('userUpdate');
Route::post('/mon-compte/pass', 'Frontend\FrontendUsersController@editpass')->name('userUpdatePass');
Route::post('/mon-compte/avatar', 'Frontend\FrontendUsersController@editavatar')->name('userUpdateAvatar');

Route::get('/usergame', 'Frontend\FrontendUsersController@game')->name('game');
Route::get('/savewin', 'Frontend\FrontendUsersController@savewin')->name('savewin');


Route::post('/l-ogin', 'Auth\LoginController@loginme')->name('loginme');
Route::get('/login/{token}', 'Auth\LoginController@getconfirm')->name('getconfirm');
Route::post('/l-oginadmin', 'Auth\LoginController@loginadminme')->name('loginadminme');

Route::post('/messadmin', 'Frontend\FrontendUsersController@messadmin')->name('messadmin');
Route::get('/likenew/{id}', 'Frontend\FrontendUsersController@likenew')->name('likenew');
Route::get('/likenewno/{id}', 'Frontend\FrontendUsersController@likenonew')->name('likenonew');
/* ================== Admin Routes  ================== */
require __DIR__.'/routing/admin_routes.php';


Auth::routes();

/* ================== Frontend Routes  ================== */
require __DIR__.'/routing/admin_frontend_routes.php';

Route::get('/home', 'HomeController@index')->name('home');
