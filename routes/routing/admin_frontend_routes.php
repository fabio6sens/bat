<?php

Route::get('/gallery', 'Frontend\AdminController@gallerie')->name('gallery');
Route::post('/gallery', 'Frontend\AdminController@store')->name('addgallery');
Route::get('/gallery/{id}/delimg', 'Frontend\AdminController@delImg')->name('delimggallery');

Route::get('/video', 'Frontend\AdminController@video')->name('video');
Route::post('/video', 'Frontend\AdminController@storeVideo')->name('addvideo');
Route::get('/video/{id}/delvideo', 'Frontend\AdminController@delvideo')->name('delvideo');

Route::get('/news', 'Frontend\AdminController@news')->name('new');
Route::get('/news/add', 'Frontend\AdminController@addnews')->name('addnews');
Route::post('/news/store', 'Frontend\AdminController@newsstore')->name('newsstore');
Route::get('/newsedit/{id}/edit', 'Frontend\AdminController@newsedit')->name('newsedit');
Route::get('/newsshow/{id}/show', 'Frontend\AdminController@newsshow')->name('newsshow');
Route::post('/news/update', 'Frontend\AdminController@newsupdate')->name('newsupdate');
Route::post('/news/{id}/destroy', 'Frontend\AdminController@newsdestroy')->name('newsdestroy');

Route::post('/collecte/import', 'Frontend\AdminController@importExcel')->name('addExcelContact');
