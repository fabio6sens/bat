<?php 

Route::group(['middleware' => ['auth','utilisateur']], function () {
	Route::get('admin', 'Admin\AppController@index')->name('admin.dashboard');

	#Routes for users
	Route::prefix('users')->group(function () {
		//Roles
		Route::get('roles', 'Admin\UsersController@roles');
		Route::post('postRoles', 'Admin\UsersController@postroles');
		Route::get('editRole/{id}', 'Admin\UsersController@editroles')->name('editroles');
		Route::post('updateRoles', 'Admin\UsersController@updateroles')->name('updateroles');

		//Utilisateurs
		Route::get('index', 'Admin\UsersController@index');
		Route::get('create', 'Admin\UsersController@create');
		Route::post('store', 'Admin\UsersController@store');
		Route::get('edit/{id}', 'Admin\UsersController@edit')->name('edituser');
        Route::post('update', 'Admin\UsersController@update')->name('updateuser');
        
        //Importer
        Route::get('import', 'Admin\UsersController@import');

        //Rapport
        Route::get('collecte/rapport', 'Admin\UsersController@rapport');
        Route::post('collecte/rapport', 'Admin\UsersController@rapportshow');

		//login
		Route::post('login', 'Admin\UsersController@login');

		//Collecte
		Route::get('collecte', 'Admin\UsersController@collecte');
		Route::get('collecte/instagram', 'Admin\CollecteController@instagram');
		Route::get('collecte/{id}', 'Admin\UsersController@collecteshow')->name('showcollecte');
			//Sub routing export collecte
			Route::post('export', 'Admin\UsersController@export');
			Route::get('exporturl', 'Admin\UsersController@exporturl');
        Route::get('exportphase/{id}', 'Admin\UsersController@exportphase');
			Route::get('export/{slug}', 'Admin\CollecteController@export');

        //Collecte new pdt
        Route::get('dunhill/collecte', 'Admin\UsersController@collectePdt1');
        Route::post('dunhill/collecte/export', 'Admin\UsersController@collectePdt1export');
        Route::get('cravenarouge/collecte', 'Admin\UsersController@collectePdt2');
        Route::post('cravenarouge/collecte/export', 'Admin\UsersController@collectePdt2export');
        Route::get('duncra/{id}', 'Admin\UsersController@duncrashow')->name('duncrashow');

        //Rapport
        Route::get('dunhill/rapport', 'Admin\UsersController@rapportDunhill');
        Route::post('dunhill/rapport', 'Admin\UsersController@rapportDunhillShow');
        Route::get('cravenarouge/rapport', 'Admin\UsersController@rapportCraven');
        Route::post('cravenarouge/rapport', 'Admin\UsersController@rapportCravenShow');

        Route::get('rovingteam', 'Admin\UsersController@roving');
        Route::get('rovingteam/create', 'Admin\UsersController@roving_create');
        Route::get('rovingteam/{id}/edit', 'Admin\UsersController@roving_edit')->name('roving_edit');
        Route::get('rovingteam/{id}/delete', 'Admin\UsersController@roving_delete')->name('roving_delete');
        Route::post('rovingteam/store', 'Admin\UsersController@roving_store');
        Route::post('rovingteam/update', 'Admin\UsersController@roving_update');

		//delete & update
		Route::delete('{id}', 'Admin\UsersController@destroy');
		Route::delete('roles/{id}', 'Admin\UsersController@destroyRule');
		
		//profile
        Route::get('myprofil', 'Admin\UsersController@indexprofil')->name('myprofil');
        Route::post('/myprofil/save', 'Admin\UsersController@profilupdate')->name('profilUpdate');
        Route::post('/myprofil/pass', 'Admin\UsersController@profileditpass')->name('profilUpdatePass');
        Route::post('/myprofil/avatar', 'Admin\UsersController@profileditavatar')->name('profilUpdateAvatar');

	});

	#Routes for sms
	Route::prefix('sms')->group(function () {
		Route::get('index', 'Admin\SmsController@index');
		Route::get('create', 'Admin\SmsController@create');
		Route::post('store', 'Admin\SmsController@store');
		Route::get('details/{id}', 'Admin\SmsController@details');
		Route::delete('{id}', 'Admin\SmsController@destroy');
	});
	
	#Routes for sms
    Route::prefix('message')->group(function () {
        Route::get('index', 'Admin\UsersController@indexmessage')->name('message');
        Route::get('index/{id}', 'Admin\UsersController@indexmessageshow')->name('showmessage');
    });

});